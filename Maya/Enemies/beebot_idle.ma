//Maya ASCII 2019 scene
//Name: beebot_idle.ma
//Last modified: Sat, Jan 11, 2020 08:05:18 PM
//Codeset: 1252
file -rdi 1 -ns "beebotrig_final" -rfn "beebotrig_finalRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Sonja/Hagenberg/pro3/spaceblaze-assets/Maya/beebotrig_final.ma";
file -r -ns "beebotrig_final" -dr 1 -rfn "beebotrig_finalRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Sonja/Hagenberg/pro3/spaceblaze-assets/Maya/beebotrig_final.ma";
requires maya "2019";
requires -nodeType "gameFbxExporter" "gameFbxExporter" "1.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiStandardSurface"
		 "mtoa" "3.1.2";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "201812112215-434d8d9c04";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18362)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "5DE2115A-49BB-4AC3-A588-E6A60F3B8EED";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.34160826807351685 0.50225387586262982 5.3480885668878821 ;
	setAttr ".r" -type "double3" 4.4616472713074389 -721.79999999988956 -1.2430175265603059e-17 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E72ED1D2-4BDA-3A06-84F8-C0919EAA935F";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 5.7168263134948374;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "909E62DF-4846-38A7-EB80-CD9DFB74355D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "A4539B08-47D6-03B1-6B4F-A688618CD22C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "30F2E34E-49A5-9115-DB0B-65B45AB2D22D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2421F50F-4784-F8C7-3CB5-FCA3871A5784";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "B9698B13-4047-AC17-715A-AEA3190E3BC7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AB2697CB-4686-2D92-C6DC-3D96BCE2091D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A8F274C3-4262-EE2A-ADF5-7EB9919B2036";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "C17A7688-4F61-74A3-F380-BF96D4F4C373";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "EEE5869A-49C4-949D-ADE5-8CBCBD77F0AA";
createNode displayLayerManager -n "layerManager";
	rename -uid "912069AC-483E-B38D-7135-4EA84793DF4B";
	setAttr -s 3 ".dli[1:3]"  7 6 8;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "9E0E80FA-4AB1-079C-5788-338A3CF0B90A";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "59A70D26-439D-F1CD-B026-4FB05F282B3A";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "28694BEE-4EC0-EF8B-7B2A-EA9A6509A3E7";
	setAttr ".g" yes;
createNode gameFbxExporter -n "gameExporterPreset1";
	rename -uid "77F7D619-4394-8C43-EF08-F096676B12BE";
	setAttr ".pn" -type "string" "Model Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".ebm" yes;
	setAttr ".inc" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "gameExporterPreset2";
	rename -uid "A2935AB0-4B02-BD66-EAFF-33AC3BC762C7";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 2;
	setAttr ".spt" 2;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "gameExporterPreset3";
	rename -uid "B48E2184-4B50-9022-8181-BF8C837E1022";
	setAttr ".pn" -type "string" "TE Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 3;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode reference -n "beebotrig_finalRN";
	rename -uid "7A359DBC-4816-2F8A-EB90-0CAFD71B3CCE";
	setAttr -s 80 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"beebotrig_finalRN"
		"beebotrig_finalRN" 0
		"beebotrig_finalRN" 105
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translateZ" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face" 
		"rotateX" " -av 12.683"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top" 
		"rotate" " -type \"double3\" -12.02791599002650536 0 -10"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top" 
		"rotateZ" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm" 
		"rotate" " -type \"double3\" 1.53812363703637178 0 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L" 
		"rotate" " -type \"double3\" 0 7.77777777777786827 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L" 
		"rotateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L" 
		"rotateZ" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R" 
		"rotate" " -type \"double3\" 0 -7.77777777777786827 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R" 
		"rotateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R" 
		"rotateZ" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translate" " -type \"double3\" 0 -0.1258020551286991 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translateZ" " -av"
		2 "beebotrig_final:mesh" "displayType" " 2"
		2 "beebotrig_final:skeleton" "visibility" " 1"
		2 "beebotrig_final:ctrlsLayer" "displayType" " 0"
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateX" 
		"beebotrig_finalRN.placeHolderList[1]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateY" 
		"beebotrig_finalRN.placeHolderList[2]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateZ" 
		"beebotrig_finalRN.placeHolderList[3]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[4]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[5]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[6]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[7]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateX" 
		"beebotrig_finalRN.placeHolderList[8]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateY" 
		"beebotrig_finalRN.placeHolderList[9]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateZ" 
		"beebotrig_finalRN.placeHolderList[10]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[11]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[12]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[13]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[14]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.rotateX" 
		"beebotrig_finalRN.placeHolderList[15]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.visibility" 
		"beebotrig_finalRN.placeHolderList[16]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleX" 
		"beebotrig_finalRN.placeHolderList[17]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleY" 
		"beebotrig_finalRN.placeHolderList[18]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleZ" 
		"beebotrig_finalRN.placeHolderList[19]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateX" 
		"beebotrig_finalRN.placeHolderList[20]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateY" 
		"beebotrig_finalRN.placeHolderList[21]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateZ" 
		"beebotrig_finalRN.placeHolderList[22]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.visibility" 
		"beebotrig_finalRN.placeHolderList[23]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateX" 
		"beebotrig_finalRN.placeHolderList[24]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateY" 
		"beebotrig_finalRN.placeHolderList[25]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateZ" 
		"beebotrig_finalRN.placeHolderList[26]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleX" 
		"beebotrig_finalRN.placeHolderList[27]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleY" 
		"beebotrig_finalRN.placeHolderList[28]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleZ" 
		"beebotrig_finalRN.placeHolderList[29]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateX" 
		"beebotrig_finalRN.placeHolderList[30]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateY" 
		"beebotrig_finalRN.placeHolderList[31]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateZ" 
		"beebotrig_finalRN.placeHolderList[32]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.visibility" 
		"beebotrig_finalRN.placeHolderList[33]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateX" 
		"beebotrig_finalRN.placeHolderList[34]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateY" 
		"beebotrig_finalRN.placeHolderList[35]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateZ" 
		"beebotrig_finalRN.placeHolderList[36]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleX" 
		"beebotrig_finalRN.placeHolderList[37]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleY" 
		"beebotrig_finalRN.placeHolderList[38]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleZ" 
		"beebotrig_finalRN.placeHolderList[39]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateY" 
		"beebotrig_finalRN.placeHolderList[40]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateX" 
		"beebotrig_finalRN.placeHolderList[41]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateZ" 
		"beebotrig_finalRN.placeHolderList[42]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.visibility" 
		"beebotrig_finalRN.placeHolderList[43]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateX" 
		"beebotrig_finalRN.placeHolderList[44]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateY" 
		"beebotrig_finalRN.placeHolderList[45]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateZ" 
		"beebotrig_finalRN.placeHolderList[46]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleX" 
		"beebotrig_finalRN.placeHolderList[47]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleY" 
		"beebotrig_finalRN.placeHolderList[48]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleZ" 
		"beebotrig_finalRN.placeHolderList[49]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateY" 
		"beebotrig_finalRN.placeHolderList[50]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateX" 
		"beebotrig_finalRN.placeHolderList[51]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateZ" 
		"beebotrig_finalRN.placeHolderList[52]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.visibility" 
		"beebotrig_finalRN.placeHolderList[53]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateX" 
		"beebotrig_finalRN.placeHolderList[54]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateY" 
		"beebotrig_finalRN.placeHolderList[55]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateZ" 
		"beebotrig_finalRN.placeHolderList[56]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleX" 
		"beebotrig_finalRN.placeHolderList[57]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleY" 
		"beebotrig_finalRN.placeHolderList[58]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleZ" 
		"beebotrig_finalRN.placeHolderList[59]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateX" 
		"beebotrig_finalRN.placeHolderList[60]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateY" 
		"beebotrig_finalRN.placeHolderList[61]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[62]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateX" 
		"beebotrig_finalRN.placeHolderList[63]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateY" 
		"beebotrig_finalRN.placeHolderList[64]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateZ" 
		"beebotrig_finalRN.placeHolderList[65]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[66]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[67]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[68]" ""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion1.message" "beebotrig_finalRN.placeHolderList[69]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion1.message" "beebotrig_finalRN.placeHolderList[70]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion2.message" "beebotrig_finalRN.placeHolderList[71]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion2.message" "beebotrig_finalRN.placeHolderList[72]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion3.message" "beebotrig_finalRN.placeHolderList[73]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion3.message" "beebotrig_finalRN.placeHolderList[74]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion4.message" "beebotrig_finalRN.placeHolderList[75]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion4.message" "beebotrig_finalRN.placeHolderList[76]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion5.message" "beebotrig_finalRN.placeHolderList[77]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion5.message" "beebotrig_finalRN.placeHolderList[78]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion6.message" "beebotrig_finalRN.placeHolderList[79]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion6.message" "beebotrig_finalRN.placeHolderList[80]" 
		"";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "632CBDD4-4754-0DCA-CFFD-4BB4DA3F6ED5";
	setAttr ".version" -type "string" "3.1.2";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "8B00F361-48B8-0071-F4EC-38BB8CAFE73D";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "0609C1A1-45CB-E55D-1B3B-41A72EB67227";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "5F384D3D-4B91-1EED-0D76-EE968DFE7F85";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode animCurveTU -n "body_ctrl_visibility";
	rename -uid "561CCF71-414C-C24A-8A0C-8AB9707D8BD5";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "body_ctrl_translateX";
	rename -uid "7568C406-409E-5E9E-CEB2-E982C74D6F60";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 120 0;
createNode animCurveTL -n "body_ctrl_translateY";
	rename -uid "338DC6A7-4BC4-080F-DF7D-D39A8EB79D33";
	setAttr ".tan" 3;
	setAttr -s 7 ".ktv[0:6]"  0 0 20 0.18820121279226887 40 0 60 0.18820121279226887
		 80 0 100 0.18820121279226887 120 0;
createNode animCurveTL -n "body_ctrl_translateZ";
	rename -uid "F26FB683-4B62-D802-8133-308666950715";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 120 0;
createNode animCurveTU -n "body_ctrl_scaleX";
	rename -uid "913332C1-4FB1-EDE4-2C20-A4B4743EA92A";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
createNode animCurveTU -n "body_ctrl_scaleY";
	rename -uid "5B2B4043-4A61-039B-B3E3-50BA9CC194B8";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
createNode animCurveTU -n "body_ctrl_scaleZ";
	rename -uid "3E5234A1-4A02-9658-4037-DC9F688014C6";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
createNode animCurveTU -n "face_visibility";
	rename -uid "2C845856-4DFA-09F0-2B9A-5EA86D2B3257";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  5;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  5;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "face_rotateX";
	rename -uid "DBD1D850-4A8F-9986-5CA6-8A97D2EAB54D";
	setAttr ".tan" 3;
	setAttr -s 8 ".ktv[0:7]"  0 12.683 5 19.521684704648393 25 -3.5033991214157973
		 45 18.755526150426075 65 -3.5699248120300737 85 20 105 -3.5699248120300737 120 12.683;
	setAttr -s 8 ".kit[0:7]"  2 3 3 3 3 3 3 2;
	setAttr -s 8 ".kot[0:7]"  2 3 3 3 3 3 3 2;
createNode animCurveTU -n "face_scaleX";
	rename -uid "4AB9D705-4798-16B3-CD63-38B40F3590DE";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  5;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  5;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTU -n "face_scaleY";
	rename -uid "8B079DD4-4EA7-35C4-3AB3-2DBFAB1B8BF6";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  5;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  5;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTU -n "face_scaleZ";
	rename -uid "65C0CC4E-41FE-83B7-99FB-ADBE3C69ADA4";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  5;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  5;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTU -n "neck_rotate_ctrl_visibility";
	rename -uid "36E1F9FA-4001-1915-3FF7-58A1B6F87200";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "neck_rotate_ctrl_rotateX";
	rename -uid "75C42D16-40DE-5E91-8211-D28731C047C2";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "neck_rotate_ctrl_rotateY";
	rename -uid "5D49F5D7-4631-E6E1-024E-6BBEA7BA7C73";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "neck_rotate_ctrl_rotateZ";
	rename -uid "7871DBF1-4EAB-658A-678D-06B85EA66ED0";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "neck_rotate_ctrl_scaleX";
	rename -uid "EEC1F55D-4E7C-A800-BA69-BBAC55110484";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "neck_rotate_ctrl_scaleY";
	rename -uid "D3278D0F-4765-083E-61C1-1684AE1E72C3";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "neck_rotate_ctrl_scaleZ";
	rename -uid "5EF60A1E-4C14-AAB7-6F68-00BEE3647253";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "tail_ctry_btm_visibility";
	rename -uid "07A7F863-421D-015A-DEB1-5F8EC1D6ACD7";
	setAttr ".tan" 5;
	setAttr -s 6 ".ktv[0:5]"  10 1 30 1 50 1 70 1 90 1 110 1;
	setAttr -s 6 ".kit[0:5]"  1 9 9 9 1 9;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "tail_ctry_btm_translateX";
	rename -uid "7805ACEF-4A5D-0896-D5B8-D2BDAF28DBB1";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 0 30 0 50 0 70 0 90 0 110 0;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "tail_ctry_btm_translateY";
	rename -uid "11AE18F9-47DA-4D6F-9D33-24B1DDE5741F";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 0 30 0 50 0 70 0 90 0 110 0;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "tail_ctry_btm_translateZ";
	rename -uid "7C44B515-4F19-A54B-FEE8-5CB158EF1EA4";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 0 30 0 50 0 70 0 90 0 110 0;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "tail_ctry_btm_rotateX";
	rename -uid "6610629A-4B99-F974-C969-B4AC1EF898A2";
	setAttr ".tan" 3;
	setAttr -s 12 ".ktv[0:11]"  0 1.5381236370363718 15 -14.399000000000001
		 15.000000170068027 -14.398853629213724 35 1.538 35.000000170068027 1.5381236370363718
		 55 -14.398853629213724 55.000000170068027 -14.398853629213724 75 1.5381236370363718
		 75.00000017006802 1.5381236370363718 99.99999982993198 -14.398853629213724 100 -14.398853629213724
		 120 1.5381236370363718;
	setAttr -s 12 ".kit[11]"  2;
	setAttr -s 12 ".kot[11]"  2;
createNode animCurveTA -n "tail_ctry_btm_rotateY";
	rename -uid "C14C8FE2-4C52-ACEA-A4A3-BFA3DC548A06";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 0 30 0 50 0 70 0 90 0 110 0;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "tail_ctry_btm_rotateZ";
	rename -uid "E8E45910-4ACB-281A-921D-84BC60A7354A";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 0 30 0 50 0 70 0 90 0 110 0;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctry_btm_scaleX";
	rename -uid "4B47021E-4411-A811-6F4A-FFA569623036";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 1 30 1 50 1 70 1 90 1 110 1;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctry_btm_scaleY";
	rename -uid "4992CE6A-4923-985E-A617-0788A3456054";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 1 30 1 50 1 70 1 90 1 110 1;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctry_btm_scaleZ";
	rename -uid "41A0A418-43AE-41FC-BFA5-9B8DBA401445";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  10 1 30 1 50 1 70 1 90 1 110 1;
	setAttr -s 6 ".kit[0:5]"  1 2 2 2 1 2;
	setAttr -s 6 ".kot[0:5]"  1 2 2 1 2 1;
	setAttr -s 6 ".kix[0:5]"  0.83333333333333348 0.83333333333333326 
		0.83333333333333348 0.83333333333333304 0.83333333333333348 0.83333333333333304;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  0.83333333333333304 0.83333333333333348 
		0.83333333333333304 0.83333333333333348 0.83333333333333304 0.83333333333333348;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctrl_top_visibility";
	rename -uid "F5BD0B85-41C0-1552-10CA-3297C82BF3E3";
	setAttr ".tan" 5;
	setAttr -s 7 ".ktv[0:6]"  0 1 20 1 40 1 60 1 80 1 100 1 120 1;
	setAttr -s 7 ".kit[0:6]"  1 9 9 9 1 9 1;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "tail_ctrl_top_translateX";
	rename -uid "CFDD1E01-460C-A434-0280-B292EA7B674F";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 0 20 0 40 0 60 0 80 0 100 0 120 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "tail_ctrl_top_translateY";
	rename -uid "A8529817-47DC-E0AA-DD2B-0A80D319E5A7";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 0 20 0 40 0 60 0 80 0 100 0 120 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "tail_ctrl_top_translateZ";
	rename -uid "CDB3DC66-4ABA-6358-769E-0785C2CD9575";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 0 20 0 40 0 60 0 80 0 100 0 120 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "tail_ctrl_top_rotateX";
	rename -uid "D6B54B75-4160-6EC0-D442-61ADC91176F1";
	setAttr ".tan" 3;
	setAttr -s 8 ".ktv[0:7]"  0 -12.027915990026505 19.999999829931973 6.846
		 20 6.8464373621223977 40 -12.027915990026505 60 7.787 80 -12.027915990026505 100 5.9247985686304254
		 120 -12.027915990026505;
createNode animCurveTA -n "tail_ctrl_top_rotateY";
	rename -uid "5400C589-40B5-5FE4-1BF3-F3B89D788E8E";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 0 20 0 40 0 60 0 80 0 100 0 120 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctrl_top_scaleX";
	rename -uid "40548731-4F50-F96C-C3CC-11BCC78F0F1E";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 1 20 1 40 1 60 1 80 1 100 1 120 1;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctrl_top_scaleY";
	rename -uid "9E2A3574-4994-FE44-C3CD-1F9635A3CD05";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 1 20 1 40 1 60 1 80 1 100 1 120 1;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "tail_ctrl_top_scaleZ";
	rename -uid "36325F3C-4742-6ABE-A15C-A2BD035BE01A";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 1 20 1 40 1 60 1 80 1 100 1 120 1;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 1 2 1;
	setAttr -s 7 ".kot[0:6]"  1 2 2 1 2 1 2;
	setAttr -s 7 ".kix[0:6]"  0.83333333333333337 0.83333333333333337 
		0.83333333333333337 0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.83333333333333326 0.83333333333333337 
		0.83333333333333326 0.83333333333333337 0.83333333333333348 0.83333333333333337 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "wing_rotate_ctrl_L_rotateX";
	rename -uid "E99DB199-435E-07B7-C9FC-0F9A7C776AE9";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTA -n "wing_rotate_ctrl_L_rotateY";
	rename -uid "F61310E6-44FD-E303-5644-7987D1ABCBAD";
	setAttr ".tan" 3;
	setAttr -s 41 ".ktv[0:40]"  0 7.7777777777778683 3 29.999999999999996
		 6 0 9 29.999999999999996 12 0 15 29.999999999999996 18 0 21 29.999999999999996 24 0
		 27 29.999999999999996 30 0 33 29.999999999999996 36 0 39 29.999999999999996 42 0
		 45 29.999999999999996 48 0 51 29.999999999999996 54 0 57 29.999999999999996 60 0
		 63 29.999999999999996 66 0 69 29.999999999999996 72 0 75 29.999999999999996 78 0
		 81 29.999999999999996 84 0 87 29.999999999999996 90 0 93 29.999999999999996 96 0
		 99 29.999999999999996 102 0 105 29.999999999999996 108 0 111 29.999999999999996 114 0
		 117 29.999999999999996 120 0;
createNode animCurveTA -n "wing_rotate_ctrl_L_rotateZ";
	rename -uid "BCC96F11-46C0-4C91-9AAE-A4AC0C51A8C4";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTU -n "wing_rotate_ctrl_L_visibility";
	rename -uid "7D63E859-4E68-3E74-4324-17A30EFDCD9A";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTL -n "wing_rotate_ctrl_L_translateX";
	rename -uid "8DF971F3-4696-00AF-D54F-CF8B86740013";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTL -n "wing_rotate_ctrl_L_translateY";
	rename -uid "AD41045D-4EC0-A91D-0926-5A837D24996F";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTL -n "wing_rotate_ctrl_L_translateZ";
	rename -uid "3C41D51A-492D-26E1-FF96-CFB218FFF9A5";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTU -n "wing_rotate_ctrl_L_scaleX";
	rename -uid "040963D2-470D-258A-8AA9-2F85D5E8DDDE";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTU -n "wing_rotate_ctrl_L_scaleY";
	rename -uid "75FB62B7-4E75-6D59-5780-5789622AC76B";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTU -n "wing_rotate_ctrl_L_scaleZ";
	rename -uid "872B21F2-4F7D-F692-4446-87AEAADBD53E";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTA -n "wing_rotate_ctrl_R_rotateX";
	rename -uid "8B1ACB98-4FFC-A41A-A8D0-91B065870CA7";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "wing_rotate_ctrl_R_rotateY";
	rename -uid "535A6F13-487E-4DFE-4098-E8B57479D2F7";
	setAttr ".tan" 3;
	setAttr -s 41 ".ktv[0:40]"  0 -7.7777777777778683 3 -29.999999999999996
		 6 0 9 -29.999999999999996 12 0 15 -29.999999999999996 18 0 21 -29.999999999999996
		 24 0 27 -29.999999999999996 30 0 33 -29.999999999999996 36 0 39 -29.999999999999996
		 42 0 45 -29.999999999999996 48 0 51 -29.999999999999996 54 0 57 -29.999999999999996
		 60 0 63 -29.999999999999996 66 0 69 -29.999999999999996 72 0 75 -29.999999999999996
		 78 0 81 -29.999999999999996 84 0 87 -29.999999999999996 90 0 93 -29.999999999999996
		 96 0 99 -29.999999999999996 102 0 105 -29.999999999999996 108 0 111 -29.999999999999996
		 114 0 117 -29.999999999999996 120 0;
createNode animCurveTA -n "wing_rotate_ctrl_R_rotateZ";
	rename -uid "8645E6A2-4925-172A-E7A0-4B9746C0F192";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "wing_rotate_ctrl_R_visibility";
	rename -uid "30318264-4D22-B44E-A291-4C885CB3E88F";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTL -n "wing_rotate_ctrl_R_translateX";
	rename -uid "8DB24761-45B0-497E-C8D7-E5AAC7028DBA";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "wing_rotate_ctrl_R_translateY";
	rename -uid "741AEBAE-4D92-E7A2-0FA2-3EBCA5545AB9";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "wing_rotate_ctrl_R_translateZ";
	rename -uid "71374760-4762-C7DB-F51C-E3A6CBEFA085";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "wing_rotate_ctrl_R_scaleX";
	rename -uid "51AA843F-4071-2D12-9499-75AAE754E858";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "wing_rotate_ctrl_R_scaleY";
	rename -uid "A05A1A2F-4949-9E15-320C-7B82DCC30C2E";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "wing_rotate_ctrl_R_scaleZ";
	rename -uid "87B85E25-4E48-3A3C-D0D7-88B06B8DF5B7";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "7EEEB5DD-43C2-9541-846F-7181F40A45D4";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1282\n            -height 456\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n"
		+ "            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n"
		+ "            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n"
		+ "\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n"
		+ "                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n"
		+ "            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1282\\n    -height 456\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1282\\n    -height 456\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "CC3F2C80-4B9B-93BB-F52D-659ADE78A705";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast -20 -aet 200 ";
	setAttr ".st" 6;
createNode shapeEditorManager -n "beebotrig_final:shapeEditorManager1";
	rename -uid "5001CA25-4ECD-F6E2-56EF-F8B41F6B10D2";
createNode poseInterpolatorManager -n "beebotrig_final:poseInterpolatorManager1";
	rename -uid "BD8B53FF-4245-6D5D-0689-1F95DABA0E64";
createNode renderLayerManager -n "beebotrig_final:renderLayerManager1";
	rename -uid "D2DBE031-463D-4972-ADD7-F0998A7B7E6E";
createNode renderLayer -n "beebotrig_final:defaultRenderLayer1";
	rename -uid "161122D9-422B-7819-F08D-3F877CBEDE54";
	setAttr ".g" yes;
createNode aiStandardSurface -n "beebotrig_final:aiStandardSurface3";
	rename -uid "4BE4C55B-4475-9798-8E66-D1A8FB70114E";
	setAttr ".base_color" -type "float3" 0.0060000001 0.0060000001 0.0060000001 ;
createNode shadingEngine -n "beebotrig_final:aiStandardSurface1SG1";
	rename -uid "7C60186D-4155-5E5B-8DB2-2189241D446B";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "beebotrig_final:materialInfo3";
	rename -uid "99A78441-49F5-947A-49CE-6C930732CC34";
createNode aiStandardSurface -n "beebotrig_final:aiStandardSurface4";
	rename -uid "F810B9FA-4A89-6203-97BC-FC80DA2A3C88";
	setAttr ".base_color" -type "float3" 0.91329998 0.0108 0 ;
createNode shadingEngine -n "beebotrig_final:aiStandardSurface2SG1";
	rename -uid "56CD36EC-48D6-B73C-D45F-71B906EC46C2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "beebotrig_final:materialInfo4";
	rename -uid "A94D8CEB-4362-1D33-EB09-99BFCE090D72";
createNode polyExtrudeEdge -n "beebotrig_final:polyExtrudeEdge78";
	rename -uid "4057A7F7-4431-C77C-55A0-7687FF4B9502";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[96:103]";
	setAttr ".ix" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -2.8330772466548515 0.78051001456659364 6.326182673000738 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -2.8330772466548515 0.78051001456659364 6.326182673000738 1;
	setAttr ".pvt" -type "float3" -2.8330772 0.74201363 6.4427419 ;
	setAttr ".rs" 48944;
	setAttr ".lt" -type "double3" -1.2663481374630692e-16 0.035300693834484184 -7.6588041464376033e-16 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.3290893901147411 0.27102443147984168 6.2871882396615355 ;
	setAttr ".cbx" -type "double3" -2.3370648636673446 1.2130026738667765 6.5982964926052308 ;
createNode polyExtrudeFace -n "beebotrig_final:polyExtrudeFace116";
	rename -uid "BF52CBC9-4239-2B99-ADF5-F2AA7339DC56";
	setAttr ".ics" -type "componentList" 2 "f[31]" "f[39]";
	setAttr ".ix" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -1.237370651658251 0.78059761600866207 6.326182673000738 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -1.237370651658251 0.78059761600866207 6.326182673000738 1;
	setAttr ".pvt" -type "float3" -1.6419077 1.0097425 6.2752514 ;
	setAttr ".rs" 46726;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.7027399970770074 0.93565412048320462 6.1197758549170373 ;
	setAttr ".cbx" -type "double3" -1.5810753870035659 1.0912825516822895 6.4307272433252622 ;
createNode polyExtrudeFace -n "beebotrig_final:polyExtrudeFace117";
	rename -uid "6936CB3D-4C95-7043-9A12-89A17D66AC55";
	setAttr ".ics" -type "componentList" 2 "f[22]" "f[25]";
	setAttr ".ix" -type "matrix" 0.46349038772053064 0 0 0 0 0.46349038772053064 0 0
		 0 0 0.46349038772053064 0 -7.9509430838345159 1.0263017414812494 5.3607038008016374 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.46349038772053064 0 0 0 0 0.46349038772053064 0 0
		 0 0 0.46349038772053064 0 -7.9509430838345159 1.0263017414812494 5.3607038008016374 1;
	setAttr ".pvt" -type "float3" -7.950943 0.85730112 5.2243176 ;
	setAttr ".rs" 46044;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.0281914771835741 0.79455654762098415 5.0879309671467983 ;
	setAttr ".cbx" -type "double3" -7.8736946766723674 0.92004567274274174 5.3607038008016374 ;
createNode polyExtrudeFace -n "beebotrig_final:polyExtrudeFace118";
	rename -uid "0B4873D4-4D5B-A660-885F-94BC73EF92B2";
	setAttr ".ics" -type "componentList" 4 "f[6:7]" "f[18:19]" "f[28:29]" "f[38:39]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pvt" -type "float3" -7.4275131 0.33314458 2.5058122 ;
	setAttr ".rs" 52264;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.1254854202270508 0.33314454555511475 1.8212032318115234 ;
	setAttr ".cbx" -type "double3" -6.7295408248901367 0.33314460515975952 3.1904211044311523 ;
createNode displayLayer -n "beebotrig_final:mesh1";
	rename -uid "C9E48559-4224-30E4-0188-BE99F45D9DA0";
	setAttr ".c" 28;
	setAttr ".do" 1;
createNode displayLayer -n "beebotrig_final:skeleton1";
	rename -uid "8A68D42B-4DD4-13D0-E9DC-C9A8CC8A9A85";
	setAttr ".dt" 2;
	setAttr ".v" no;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.40400004 0.088 ;
	setAttr ".do" 2;
createNode displayLayer -n "beebotrig_final:ctrlsLayer1";
	rename -uid "5E2975B4-4663-3552-A9D6-F08A23DA32BA";
	setAttr ".dt" 2;
	setAttr ".c" 15;
	setAttr ".do" 3;
createNode renderLayerManager -n "pasted__renderLayerManager";
	rename -uid "CEBDAC58-4685-830B-629B-6788F95F6C03";
createNode renderLayer -n "pasted__defaultRenderLayer";
	rename -uid "7F398A33-4CCA-C27A-24BA-7098AB5E4A62";
	setAttr ".g" yes;
createNode gameFbxExporter -n "pasted__gameExporterPreset1";
	rename -uid "3E01AD8E-4943-1414-C749-CD89D5F65677";
	setAttr ".pn" -type "string" "Model Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".ebm" yes;
	setAttr ".inc" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "pasted__gameExporterPreset2";
	rename -uid "B598E932-4BA1-8C42-E722-16A8678B5047";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 2;
	setAttr ".spt" 2;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "pasted__gameExporterPreset3";
	rename -uid "E99D5CE7-4FEC-E4B7-675B-79B56423F806";
	setAttr ".pn" -type "string" "TE Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 3;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode reference -n "pasted__beebotrig_finalRN";
	rename -uid "657F6231-4ECE-399C-E2C6-B0B80A165172";
	setAttr -s 69 ".phl";
	setAttr ".ed" -type "dataReferenceEdits" 
		"pasted__beebotrig_finalRN"
		"beebotrig_finalRN" 69
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateX" 
		"pasted__beebotrig_finalRN.placeHolderList[1]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateY" 
		"pasted__beebotrig_finalRN.placeHolderList[2]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[3]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[4]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[5]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[6]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[7]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[8]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateY" 
		"pasted__beebotrig_finalRN.placeHolderList[9]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[10]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[11]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[12]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[13]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[14]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[15]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[16]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[17]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[18]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[19]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[20]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateY" 
		"pasted__beebotrig_finalRN.placeHolderList[21]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[22]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[23]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateX" 
		"pasted__beebotrig_finalRN.placeHolderList[24]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateY" 
		"pasted__beebotrig_finalRN.placeHolderList[25]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[26]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[27]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[28]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[29]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[30]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateY" 
		"pasted__beebotrig_finalRN.placeHolderList[31]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[32]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[33]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateX" 
		"pasted__beebotrig_finalRN.placeHolderList[34]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateY" 
		"pasted__beebotrig_finalRN.placeHolderList[35]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[36]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[37]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[38]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[39]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateY" 
		"pasted__beebotrig_finalRN.placeHolderList[40]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[41]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[42]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[43]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateX" 
		"pasted__beebotrig_finalRN.placeHolderList[44]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateY" 
		"pasted__beebotrig_finalRN.placeHolderList[45]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[46]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[47]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[48]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[49]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateY" 
		"pasted__beebotrig_finalRN.placeHolderList[50]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[51]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[52]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[53]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateX" 
		"pasted__beebotrig_finalRN.placeHolderList[54]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateY" 
		"pasted__beebotrig_finalRN.placeHolderList[55]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[56]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[57]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[58]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[59]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateX" 
		"pasted__beebotrig_finalRN.placeHolderList[60]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateY" 
		"pasted__beebotrig_finalRN.placeHolderList[61]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[62]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.visibility" 
		"pasted__beebotrig_finalRN.placeHolderList[63]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateX" 
		"pasted__beebotrig_finalRN.placeHolderList[64]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateY" 
		"pasted__beebotrig_finalRN.placeHolderList[65]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateZ" 
		"pasted__beebotrig_finalRN.placeHolderList[66]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleX" 
		"pasted__beebotrig_finalRN.placeHolderList[67]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleY" 
		"pasted__beebotrig_finalRN.placeHolderList[68]" ""
		5 4 "pasted__beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleZ" 
		"pasted__beebotrig_finalRN.placeHolderList[69]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode animCurveTU -n "pasted__body_ctrl_visibility";
	rename -uid "6E587A64-45DB-B41D-2086-A3A47449550E";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "pasted__body_ctrl_translateX";
	rename -uid "E5F57512-439B-EC1A-C806-A8985F2238D0";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 120 0;
createNode animCurveTL -n "pasted__body_ctrl_translateY";
	rename -uid "B3E5B93E-478A-752D-DC1C-1CBCEF9D5E73";
	setAttr ".tan" 2;
	setAttr -s 8 ".ktv[0:7]"  0 0 20 0.18820121279226887 40 0 60 0.18820121279226887
		 80 0 100 0.18820121279226887 120 0 126 0;
	setAttr -s 8 ".kit[4:7]"  1 2 1 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 1 2;
	setAttr -s 8 ".kix[4:7]"  0.875 0.83333333333333348 1 0.875;
	setAttr -s 8 ".kiy[4:7]"  -0.18820121279226887 0.18820121279226887 
		0 -0.18820121279226887;
	setAttr -s 8 ".kox[3:7]"  0.875 0.83333333333333348 0.875 0.79166666666666674 
		1;
	setAttr -s 8 ".koy[3:7]"  -0.18820121279226887 0.18820121279226887 
		-0.18820121279226887 0.18820121279226887 0;
createNode animCurveTL -n "pasted__body_ctrl_translateZ";
	rename -uid "EFCC2749-4D1E-332E-1F7E-F687EA809C44";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 120 0;
createNode animCurveTU -n "pasted__body_ctrl_scaleX";
	rename -uid "4E9F19EE-4E2E-1EA8-77F5-41B1081F90BC";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
createNode animCurveTU -n "pasted__body_ctrl_scaleY";
	rename -uid "F83FB18F-4E55-A063-D252-1D943B08FF1F";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
createNode animCurveTU -n "pasted__body_ctrl_scaleZ";
	rename -uid "7846A9D0-4BF3-4293-4BC9-049B2F91F168";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 120 1;
createNode animCurveTU -n "pasted__tailtip_ctrl_visibility";
	rename -uid "4FC7E03D-4E81-E72C-CC7E-248582A9398B";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pasted__tailtip_ctrl_translateX";
	rename -uid "212E72AD-43D9-CAE9-953A-658879442FFC";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 -0.00035913783849084169;
createNode animCurveTL -n "pasted__tailtip_ctrl_translateY";
	rename -uid "AE4227A1-4E42-1915-42CE-89B77CA5F389";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 0 20 0.18820121279226887 40 0 60 0.16071856583977939
		 80 0 100 0.19276444089016687 120 0;
createNode animCurveTL -n "pasted__tailtip_ctrl_translateZ";
	rename -uid "86AAE6C3-4583-1873-4425-A0836B0C9470";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "pasted__tailtip_ctrl_rotateX";
	rename -uid "63E9824A-4395-7A48-4293-C286416CABA0";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "pasted__tailtip_ctrl_rotateY";
	rename -uid "E5BEBFC8-4F09-1324-F1FF-0E87312BCA83";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "pasted__tailtip_ctrl_rotateZ";
	rename -uid "E38C9F27-4079-9083-94CE-10AB960704BE";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "pasted__tailtip_ctrl_scaleX";
	rename -uid "24D3C9BB-477B-BDF5-60C8-338287D961EA";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "pasted__tailtip_ctrl_scaleY";
	rename -uid "72E69A49-4D0D-F168-B7E9-2296BED6C904";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "pasted__tailtip_ctrl_scaleZ";
	rename -uid "DA987660-46A6-D12A-AE16-34869D166268";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "pasted__face_visibility";
	rename -uid "2EB192EC-4465-9229-F745-FF8AD028FBF4";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pasted__face_rotateX";
	rename -uid "5389B777-47F8-2BE5-19E0-709A05FA23E7";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pasted__face_scaleX";
	rename -uid "051DD7A5-4207-FA26-AE9F-39B94D464FB3";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__face_scaleY";
	rename -uid "38B4E72D-484B-A6D4-B7CF-A2B952C146E2";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__face_scaleZ";
	rename -uid "C3271F1B-4831-9B1F-D390-018E0428FA11";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__neck_rotate_ctrl_visibility";
	rename -uid "BBE15C23-4D12-2775-DD5F-589B4E00C9CE";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pasted__neck_rotate_ctrl_rotateX";
	rename -uid "C7B9E722-4833-B00F-B4F7-F8AF4A9EA49C";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__neck_rotate_ctrl_rotateY";
	rename -uid "42A93202-4248-3FEF-9F47-81B770B329A4";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__neck_rotate_ctrl_rotateZ";
	rename -uid "18C620A8-4E7E-AE4C-CA4C-28AD7C069CDE";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pasted__neck_rotate_ctrl_scaleX";
	rename -uid "8E37F010-45BF-A14F-68E9-3AB3D661D462";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__neck_rotate_ctrl_scaleY";
	rename -uid "20811B8A-4413-186F-8F6B-0E819FC1ED6B";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__neck_rotate_ctrl_scaleZ";
	rename -uid "D58BC58A-42D9-B5CF-03DC-70B4F1776BF7";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__tail_ctry_btm_visibility";
	rename -uid "5F40ABBB-4FAF-AB90-B30F-2898A9937391";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pasted__tail_ctry_btm_translateX";
	rename -uid "41AC66F7-41CA-30E4-2D1A-EBAF4CA52DE0";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pasted__tail_ctry_btm_translateY";
	rename -uid "655210B3-4ED6-426C-0E2B-E78FE062F76C";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pasted__tail_ctry_btm_translateZ";
	rename -uid "76407233-4A05-B7ED-98FF-61B435A2A861";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__tail_ctry_btm_rotateX";
	rename -uid "F1DC4E17-4CD6-778D-CFA0-4EBC0F179065";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__tail_ctry_btm_rotateY";
	rename -uid "0D3DC6ED-4B0E-BEC8-354A-EE9BDE8E2C17";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__tail_ctry_btm_rotateZ";
	rename -uid "EF4F4818-4EBF-94E1-D772-0EBF92D35C57";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pasted__tail_ctry_btm_scaleX";
	rename -uid "99919066-4745-4556-E125-BAB0F4E801D9";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__tail_ctry_btm_scaleY";
	rename -uid "13E04AC8-4923-0AB1-5CA8-6DA822DC2630";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__tail_ctry_btm_scaleZ";
	rename -uid "CA058C97-46E2-D702-94B4-4FBC773FC211";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__tail_ctrl_top_visibility";
	rename -uid "7CF251EB-4D69-88D8-1C12-FAA5992302F5";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pasted__tail_ctrl_top_translateX";
	rename -uid "AE33D358-4354-E6AD-F180-499C2A42E620";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pasted__tail_ctrl_top_translateY";
	rename -uid "F4BCA7BD-42BD-1DB3-1C41-BBB27509F2D6";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pasted__tail_ctrl_top_translateZ";
	rename -uid "79176068-4401-063D-4F7F-BEADF4030C1A";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__tail_ctrl_top_rotateX";
	rename -uid "390FBA64-406E-8DF3-3BA7-0DA9681D583B";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__tail_ctrl_top_rotateY";
	rename -uid "9971044C-466A-0A4E-C06D-73A11D726E7E";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pasted__tail_ctrl_top_rotateZ";
	rename -uid "9B462B1A-48E0-C948-1D5D-96A9A7001988";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pasted__tail_ctrl_top_scaleX";
	rename -uid "3192160C-44F3-AA44-369A-4F80F2452CC9";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__tail_ctrl_top_scaleY";
	rename -uid "01138F87-441F-9849-9EBE-23AA51A513FE";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pasted__tail_ctrl_top_scaleZ";
	rename -uid "76C102C0-4ABC-7D90-C187-849943E888E6";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTA -n "pasted__wing_rotate_ctrl_L_rotateX";
	rename -uid "6557CBB3-45F1-3C53-239A-4CA5EC965E1E";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTA -n "pasted__wing_rotate_ctrl_L_rotateY";
	rename -uid "20A23B6E-479A-68B4-7749-269587B1AF93";
	setAttr ".tan" 3;
	setAttr -s 41 ".ktv[0:40]"  0 0 3 29.999999999999996 6 0 9 29.999999999999996
		 12 0 15 29.999999999999996 18 0 21 29.999999999999996 24 0 27 29.999999999999996
		 30 0 33 29.999999999999996 36 0 39 29.999999999999996 42 0 45 29.999999999999996
		 48 0 51 29.999999999999996 54 0 57 29.999999999999996 60 0 63 29.999999999999996
		 66 0 69 29.999999999999996 72 0 75 29.999999999999996 78 0 81 29.999999999999996
		 84 0 87 29.999999999999996 90 0 93 29.999999999999996 96 0 99 29.999999999999996
		 102 0 105 29.999999999999996 108 0 111 29.999999999999996 114 0 117 29.999999999999996
		 120 0;
createNode animCurveTA -n "pasted__wing_rotate_ctrl_L_rotateZ";
	rename -uid "94A1A461-4A1D-FB64-D853-78925BE39F2B";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_L_visibility";
	rename -uid "F0C898F8-4C25-497D-F9E2-3F820B3023D7";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTL -n "pasted__wing_rotate_ctrl_L_translateX";
	rename -uid "7A5F4218-46BD-EBAA-5EA8-ACAC6D83D52F";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTL -n "pasted__wing_rotate_ctrl_L_translateY";
	rename -uid "1C06078D-418E-42DB-857B-8BAFAD577D69";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTL -n "pasted__wing_rotate_ctrl_L_translateZ";
	rename -uid "A0FA447A-477B-F656-68EF-7CA05B8F9C5B";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 0 6 0 12 0 18 0 24 0 30 0 36 0 42 0 48 0
		 54 0 60 0 66 0 72 0 78 0 84 0 90 0 96 0 102 0 108 0 114 0 120 0;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_L_scaleX";
	rename -uid "FCF34937-4AA7-8C3F-AFAF-9B9CF1270F7D";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_L_scaleY";
	rename -uid "BDFA6CF6-4909-0276-E199-25915A6B8E81";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_L_scaleZ";
	rename -uid "8DB3D504-4FD0-792C-4FD0-209F3F93DA81";
	setAttr ".tan" 3;
	setAttr -s 21 ".ktv[0:20]"  0 1 6 1 12 1 18 1 24 1 30 1 36 1 42 1 48 1
		 54 1 60 1 66 1 72 1 78 1 84 1 90 1 96 1 102 1 108 1 114 1 120 1;
createNode animCurveTA -n "pasted__wing_rotate_ctrl_R_rotateX";
	rename -uid "72868971-4F34-23EB-2E1B-83A367D8B9A2";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "pasted__wing_rotate_ctrl_R_rotateY";
	rename -uid "24DA7ABC-4ED1-BAC4-E42B-0FB0480089F2";
	setAttr ".tan" 3;
	setAttr -s 41 ".ktv[0:40]"  0 0 3 -29.999999999999996 6 0 9 -29.999999999999996
		 12 0 15 -29.999999999999996 18 0 21 -29.999999999999996 24 0 27 -29.999999999999996
		 30 0 33 -29.999999999999996 36 0 39 -29.999999999999996 42 0 45 -29.999999999999996
		 48 0 51 -29.999999999999996 54 0 57 -29.999999999999996 60 0 63 -29.999999999999996
		 66 0 69 -29.999999999999996 72 0 75 -29.999999999999996 78 0 81 -29.999999999999996
		 84 0 87 -29.999999999999996 90 0 93 -29.999999999999996 96 0 99 -29.999999999999996
		 102 0 105 -29.999999999999996 108 0 111 -29.999999999999996 114 0 117 -29.999999999999996
		 120 0;
createNode animCurveTA -n "pasted__wing_rotate_ctrl_R_rotateZ";
	rename -uid "D236689E-4520-841C-FD46-E289EFA87273";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_R_visibility";
	rename -uid "18D306DB-4F50-D29D-E5CF-FA9E40504BA9";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTL -n "pasted__wing_rotate_ctrl_R_translateX";
	rename -uid "44D93D54-4351-79A3-7F46-12ADB9A4A506";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "pasted__wing_rotate_ctrl_R_translateY";
	rename -uid "03E04D55-4630-C05E-BFC7-F6A8D87A5CD1";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "pasted__wing_rotate_ctrl_R_translateZ";
	rename -uid "06C4ACBC-4DEE-ACCA-0A6E-829FBBB1FD33";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_R_scaleX";
	rename -uid "90EBDE7E-4D9C-A61F-B783-88904D9F8EC1";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_R_scaleY";
	rename -uid "EACABBB6-4B84-C4D9-3DC8-5DBBD50285FF";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "pasted__wing_rotate_ctrl_R_scaleZ";
	rename -uid "A776495D-419A-D236-827E-96803C1E13D3";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  0 1;
createNode nodeGraphEditorInfo -n "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "83A2CBDB-4468-C57E-0662-4AA9E5E2DBF2";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -355.95236680810473 -269.04760835662768 ;
	setAttr ".tgi[0].vh" -type "double2" 357.14284295127567 267.85713221345674 ;
	setAttr -s 6 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -91.428573608398438;
	setAttr ".tgi[0].ni[0].y" -240;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -91.428573608398438;
	setAttr ".tgi[0].ni[1].y" -37.142856597900391;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" -91.428573608398438;
	setAttr ".tgi[0].ni[2].y" 165.71427917480469;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" -91.428573608398438;
	setAttr ".tgi[0].ni[3].y" -442.85714721679688;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" -91.428573608398438;
	setAttr ".tgi[0].ni[4].y" 580;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" -91.428573608398438;
	setAttr ".tgi[0].ni[5].y" 368.57144165039063;
	setAttr ".tgi[0].ni[5].nvs" 1923;
createNode animCurveTL -n "tailtip_ctrl_translateY";
	rename -uid "27353D93-4FDF-BAF9-3CD3-00838DF0F273";
	setAttr ".tan" 3;
	setAttr -s 7 ".ktv[0:6]"  0 -0.1258020551286991 20 0.043244456450490132
		 40 -0.1258020551286991 60 0.043244456450490132 80 -0.1258020551286991 100 0.043244456450490132
		 120 -0.1258020551286991;
createNode animCurveTL -n "tailtip_ctrl_translateX";
	rename -uid "2FBA64E8-4C5A-88B9-3FFC-739AE3DD673C";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 0 10 0 20 0 30 0 40 0 50 0 60 0 70 0 80 0
		 90 0 100 0 110 0 120 0;
	setAttr -s 13 ".kit[0:12]"  3 2 2 2 2 2 2 2 
		2 2 2 2 3;
	setAttr -s 13 ".kot[0:12]"  3 2 2 2 2 2 2 2 
		2 2 2 2 3;
createNode animCurveTU -n "tailtip_ctrl_visibility";
	rename -uid "DCD915FF-4049-9140-3E43-2989CC457BF4";
	setAttr ".tan" 9;
	setAttr -s 13 ".ktv[0:12]"  0 1 10 1 20 1 30 1 40 1 50 1 60 1 70 1 80 1
		 90 1 100 1 110 1 120 1;
	setAttr -s 13 ".kot[0:12]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5;
createNode animCurveTA -n "tailtip_ctrl_rotateX";
	rename -uid "11492315-4869-3FDE-D334-0ABB9D6BC915";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 0 10 0 20 0 30 0 40 0 50 0 60 0 70 0 80 0
		 90 0 100 0 110 0 120 0;
createNode animCurveTA -n "tailtip_ctrl_rotateY";
	rename -uid "18E0D725-4D82-4277-D065-6993808D1E8D";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 0 10 0 20 0 30 0 40 0 50 0 60 0 70 0 80 0
		 90 0 100 0 110 0 120 0;
createNode animCurveTA -n "tailtip_ctrl_rotateZ";
	rename -uid "099619FF-4327-2D6E-8F27-F6A43EE6CA49";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 0 10 0 20 0 30 0 40 0 50 0 60 0 70 0 80 0
		 90 0 100 0 110 0 120 0;
createNode animCurveTU -n "tailtip_ctrl_scaleX";
	rename -uid "AEF40BD9-4A43-0C52-204E-CC80C4F69F2C";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 1 10 1 20 1 30 1 40 1 50 1 60 1 70 1 80 1
		 90 1 100 1 110 1 120 1;
createNode animCurveTU -n "tailtip_ctrl_scaleY";
	rename -uid "16E8A909-4ACA-3DCB-0CDE-FBB3FB2C6203";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 1 10 1 20 1 30 1 40 1 50 1 60 1 70 1 80 1
		 90 1 100 1 110 1 120 1;
createNode animCurveTU -n "tailtip_ctrl_scaleZ";
	rename -uid "0450B6BC-447D-34E6-2F01-9DBA92C44074";
	setAttr ".tan" 2;
	setAttr -s 13 ".ktv[0:12]"  0 1 10 1 20 1 30 1 40 1 50 1 60 1 70 1 80 1
		 90 1 100 1 110 1 120 1;
createNode animCurveTA -n "tail_ctrl_top_rotateZ";
	rename -uid "E5379037-4702-864D-29D8-CCB67B4A829E";
	setAttr ".tan" 3;
	setAttr -s 5 ".ktv[0:4]"  0 -10 30 10 60 -10 90 10 120 -10;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "49F974CE-4FE4-FF75-8A1A-908D3CE1E9F3";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -355.95236680810473 -269.04760835662768 ;
	setAttr ".tgi[0].vh" -type "double2" 357.14284295127567 267.85713221345674 ;
	setAttr -s 6 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -91.428573608398438;
	setAttr ".tgi[0].ni[0].y" -37.142856597900391;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -91.428573608398438;
	setAttr ".tgi[0].ni[1].y" -240;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" -91.428573608398438;
	setAttr ".tgi[0].ni[2].y" 165.71427917480469;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" -91.428573608398438;
	setAttr ".tgi[0].ni[3].y" 368.57144165039063;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" -91.428573608398438;
	setAttr ".tgi[0].ni[4].y" 580;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" -91.428573608398438;
	setAttr ".tgi[0].ni[5].y" -442.85714721679688;
	setAttr ".tgi[0].ni[5].nvs" 1923;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -s 4 ".r";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 38 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off ".eeaa";
	setAttr -k off ".engm";
	setAttr -k off ".mes";
	setAttr -k off ".emb";
	setAttr -av -k off ".mbbf";
	setAttr -k off ".mbs";
	setAttr -k off ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off ".twa";
	setAttr -k off ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
select -ne :ikSystem;
	setAttr -s 2 ".sol";
connectAttr "body_ctrl_translateX.o" "beebotrig_finalRN.phl[1]";
connectAttr "body_ctrl_translateY.o" "beebotrig_finalRN.phl[2]";
connectAttr "body_ctrl_translateZ.o" "beebotrig_finalRN.phl[3]";
connectAttr "body_ctrl_scaleX.o" "beebotrig_finalRN.phl[4]";
connectAttr "body_ctrl_scaleY.o" "beebotrig_finalRN.phl[5]";
connectAttr "body_ctrl_scaleZ.o" "beebotrig_finalRN.phl[6]";
connectAttr "body_ctrl_visibility.o" "beebotrig_finalRN.phl[7]";
connectAttr "neck_rotate_ctrl_rotateX.o" "beebotrig_finalRN.phl[8]";
connectAttr "neck_rotate_ctrl_rotateY.o" "beebotrig_finalRN.phl[9]";
connectAttr "neck_rotate_ctrl_rotateZ.o" "beebotrig_finalRN.phl[10]";
connectAttr "neck_rotate_ctrl_visibility.o" "beebotrig_finalRN.phl[11]";
connectAttr "neck_rotate_ctrl_scaleX.o" "beebotrig_finalRN.phl[12]";
connectAttr "neck_rotate_ctrl_scaleY.o" "beebotrig_finalRN.phl[13]";
connectAttr "neck_rotate_ctrl_scaleZ.o" "beebotrig_finalRN.phl[14]";
connectAttr "face_rotateX.o" "beebotrig_finalRN.phl[15]";
connectAttr "face_visibility.o" "beebotrig_finalRN.phl[16]";
connectAttr "face_scaleX.o" "beebotrig_finalRN.phl[17]";
connectAttr "face_scaleY.o" "beebotrig_finalRN.phl[18]";
connectAttr "face_scaleZ.o" "beebotrig_finalRN.phl[19]";
connectAttr "tail_ctrl_top_rotateX.o" "beebotrig_finalRN.phl[20]";
connectAttr "tail_ctrl_top_rotateY.o" "beebotrig_finalRN.phl[21]";
connectAttr "tail_ctrl_top_rotateZ.o" "beebotrig_finalRN.phl[22]";
connectAttr "tail_ctrl_top_visibility.o" "beebotrig_finalRN.phl[23]";
connectAttr "tail_ctrl_top_translateX.o" "beebotrig_finalRN.phl[24]";
connectAttr "tail_ctrl_top_translateY.o" "beebotrig_finalRN.phl[25]";
connectAttr "tail_ctrl_top_translateZ.o" "beebotrig_finalRN.phl[26]";
connectAttr "tail_ctrl_top_scaleX.o" "beebotrig_finalRN.phl[27]";
connectAttr "tail_ctrl_top_scaleY.o" "beebotrig_finalRN.phl[28]";
connectAttr "tail_ctrl_top_scaleZ.o" "beebotrig_finalRN.phl[29]";
connectAttr "tail_ctry_btm_rotateX.o" "beebotrig_finalRN.phl[30]";
connectAttr "tail_ctry_btm_rotateY.o" "beebotrig_finalRN.phl[31]";
connectAttr "tail_ctry_btm_rotateZ.o" "beebotrig_finalRN.phl[32]";
connectAttr "tail_ctry_btm_visibility.o" "beebotrig_finalRN.phl[33]";
connectAttr "tail_ctry_btm_translateX.o" "beebotrig_finalRN.phl[34]";
connectAttr "tail_ctry_btm_translateY.o" "beebotrig_finalRN.phl[35]";
connectAttr "tail_ctry_btm_translateZ.o" "beebotrig_finalRN.phl[36]";
connectAttr "tail_ctry_btm_scaleX.o" "beebotrig_finalRN.phl[37]";
connectAttr "tail_ctry_btm_scaleY.o" "beebotrig_finalRN.phl[38]";
connectAttr "tail_ctry_btm_scaleZ.o" "beebotrig_finalRN.phl[39]";
connectAttr "wing_rotate_ctrl_L_rotateY.o" "beebotrig_finalRN.phl[40]";
connectAttr "wing_rotate_ctrl_L_rotateX.o" "beebotrig_finalRN.phl[41]";
connectAttr "wing_rotate_ctrl_L_rotateZ.o" "beebotrig_finalRN.phl[42]";
connectAttr "wing_rotate_ctrl_L_visibility.o" "beebotrig_finalRN.phl[43]";
connectAttr "wing_rotate_ctrl_L_translateX.o" "beebotrig_finalRN.phl[44]";
connectAttr "wing_rotate_ctrl_L_translateY.o" "beebotrig_finalRN.phl[45]";
connectAttr "wing_rotate_ctrl_L_translateZ.o" "beebotrig_finalRN.phl[46]";
connectAttr "wing_rotate_ctrl_L_scaleX.o" "beebotrig_finalRN.phl[47]";
connectAttr "wing_rotate_ctrl_L_scaleY.o" "beebotrig_finalRN.phl[48]";
connectAttr "wing_rotate_ctrl_L_scaleZ.o" "beebotrig_finalRN.phl[49]";
connectAttr "wing_rotate_ctrl_R_rotateY.o" "beebotrig_finalRN.phl[50]";
connectAttr "wing_rotate_ctrl_R_rotateX.o" "beebotrig_finalRN.phl[51]";
connectAttr "wing_rotate_ctrl_R_rotateZ.o" "beebotrig_finalRN.phl[52]";
connectAttr "wing_rotate_ctrl_R_visibility.o" "beebotrig_finalRN.phl[53]";
connectAttr "wing_rotate_ctrl_R_translateX.o" "beebotrig_finalRN.phl[54]";
connectAttr "wing_rotate_ctrl_R_translateY.o" "beebotrig_finalRN.phl[55]";
connectAttr "wing_rotate_ctrl_R_translateZ.o" "beebotrig_finalRN.phl[56]";
connectAttr "wing_rotate_ctrl_R_scaleX.o" "beebotrig_finalRN.phl[57]";
connectAttr "wing_rotate_ctrl_R_scaleY.o" "beebotrig_finalRN.phl[58]";
connectAttr "wing_rotate_ctrl_R_scaleZ.o" "beebotrig_finalRN.phl[59]";
connectAttr "tailtip_ctrl_translateX.o" "beebotrig_finalRN.phl[60]";
connectAttr "tailtip_ctrl_translateY.o" "beebotrig_finalRN.phl[61]";
connectAttr "tailtip_ctrl_visibility.o" "beebotrig_finalRN.phl[62]";
connectAttr "tailtip_ctrl_rotateX.o" "beebotrig_finalRN.phl[63]";
connectAttr "tailtip_ctrl_rotateY.o" "beebotrig_finalRN.phl[64]";
connectAttr "tailtip_ctrl_rotateZ.o" "beebotrig_finalRN.phl[65]";
connectAttr "tailtip_ctrl_scaleX.o" "beebotrig_finalRN.phl[66]";
connectAttr "tailtip_ctrl_scaleY.o" "beebotrig_finalRN.phl[67]";
connectAttr "tailtip_ctrl_scaleZ.o" "beebotrig_finalRN.phl[68]";
connectAttr "beebotrig_finalRN.phl[69]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "beebotrig_finalRN.phl[70]" "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "beebotrig_finalRN.phl[71]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "beebotrig_finalRN.phl[72]" "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "beebotrig_finalRN.phl[73]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "beebotrig_finalRN.phl[74]" "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "beebotrig_finalRN.phl[75]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "beebotrig_finalRN.phl[76]" "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "beebotrig_finalRN.phl[77]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "beebotrig_finalRN.phl[78]" "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "beebotrig_finalRN.phl[79]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "beebotrig_finalRN.phl[80]" "pasted__hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "beebotrig_final:renderLayerManager1.rlmi[0]" "beebotrig_final:defaultRenderLayer1.rlid"
		;
connectAttr "beebotrig_final:aiStandardSurface3.out" "beebotrig_final:aiStandardSurface1SG1.ss"
		;
connectAttr "beebotrig_final:aiStandardSurface1SG1.msg" "beebotrig_final:materialInfo3.sg"
		;
connectAttr "beebotrig_final:aiStandardSurface3.msg" "beebotrig_final:materialInfo3.m"
		;
connectAttr "beebotrig_final:aiStandardSurface3.msg" "beebotrig_final:materialInfo3.t"
		 -na;
connectAttr "beebotrig_final:aiStandardSurface4.out" "beebotrig_final:aiStandardSurface2SG1.ss"
		;
connectAttr "beebotrig_final:aiStandardSurface2SG1.msg" "beebotrig_final:materialInfo4.sg"
		;
connectAttr "beebotrig_final:aiStandardSurface4.msg" "beebotrig_final:materialInfo4.m"
		;
connectAttr "beebotrig_final:aiStandardSurface4.msg" "beebotrig_final:materialInfo4.t"
		 -na;
connectAttr "layerManager.dli[1]" "beebotrig_final:mesh1.id";
connectAttr "layerManager.dli[2]" "beebotrig_final:skeleton1.id";
connectAttr "layerManager.dli[3]" "beebotrig_final:ctrlsLayer1.id";
connectAttr "pasted__renderLayerManager.rlmi[0]" "pasted__defaultRenderLayer.rlid"
		;
connectAttr "pasted__body_ctrl_translateX.o" "pasted__beebotrig_finalRN.phl[1]";
connectAttr "pasted__body_ctrl_translateY.o" "pasted__beebotrig_finalRN.phl[2]";
connectAttr "pasted__body_ctrl_translateZ.o" "pasted__beebotrig_finalRN.phl[3]";
connectAttr "pasted__body_ctrl_scaleX.o" "pasted__beebotrig_finalRN.phl[4]";
connectAttr "pasted__body_ctrl_scaleY.o" "pasted__beebotrig_finalRN.phl[5]";
connectAttr "pasted__body_ctrl_scaleZ.o" "pasted__beebotrig_finalRN.phl[6]";
connectAttr "pasted__body_ctrl_visibility.o" "pasted__beebotrig_finalRN.phl[7]";
connectAttr "pasted__neck_rotate_ctrl_rotateX.o" "pasted__beebotrig_finalRN.phl[8]"
		;
connectAttr "pasted__neck_rotate_ctrl_rotateY.o" "pasted__beebotrig_finalRN.phl[9]"
		;
connectAttr "pasted__neck_rotate_ctrl_rotateZ.o" "pasted__beebotrig_finalRN.phl[10]"
		;
connectAttr "pasted__neck_rotate_ctrl_visibility.o" "pasted__beebotrig_finalRN.phl[11]"
		;
connectAttr "pasted__neck_rotate_ctrl_scaleX.o" "pasted__beebotrig_finalRN.phl[12]"
		;
connectAttr "pasted__neck_rotate_ctrl_scaleY.o" "pasted__beebotrig_finalRN.phl[13]"
		;
connectAttr "pasted__neck_rotate_ctrl_scaleZ.o" "pasted__beebotrig_finalRN.phl[14]"
		;
connectAttr "pasted__face_rotateX.o" "pasted__beebotrig_finalRN.phl[15]";
connectAttr "pasted__face_visibility.o" "pasted__beebotrig_finalRN.phl[16]";
connectAttr "pasted__face_scaleX.o" "pasted__beebotrig_finalRN.phl[17]";
connectAttr "pasted__face_scaleY.o" "pasted__beebotrig_finalRN.phl[18]";
connectAttr "pasted__face_scaleZ.o" "pasted__beebotrig_finalRN.phl[19]";
connectAttr "pasted__tail_ctrl_top_rotateX.o" "pasted__beebotrig_finalRN.phl[20]"
		;
connectAttr "pasted__tail_ctrl_top_rotateY.o" "pasted__beebotrig_finalRN.phl[21]"
		;
connectAttr "pasted__tail_ctrl_top_rotateZ.o" "pasted__beebotrig_finalRN.phl[22]"
		;
connectAttr "pasted__tail_ctrl_top_visibility.o" "pasted__beebotrig_finalRN.phl[23]"
		;
connectAttr "pasted__tail_ctrl_top_translateX.o" "pasted__beebotrig_finalRN.phl[24]"
		;
connectAttr "pasted__tail_ctrl_top_translateY.o" "pasted__beebotrig_finalRN.phl[25]"
		;
connectAttr "pasted__tail_ctrl_top_translateZ.o" "pasted__beebotrig_finalRN.phl[26]"
		;
connectAttr "pasted__tail_ctrl_top_scaleX.o" "pasted__beebotrig_finalRN.phl[27]"
		;
connectAttr "pasted__tail_ctrl_top_scaleY.o" "pasted__beebotrig_finalRN.phl[28]"
		;
connectAttr "pasted__tail_ctrl_top_scaleZ.o" "pasted__beebotrig_finalRN.phl[29]"
		;
connectAttr "pasted__tail_ctry_btm_rotateX.o" "pasted__beebotrig_finalRN.phl[30]"
		;
connectAttr "pasted__tail_ctry_btm_rotateY.o" "pasted__beebotrig_finalRN.phl[31]"
		;
connectAttr "pasted__tail_ctry_btm_rotateZ.o" "pasted__beebotrig_finalRN.phl[32]"
		;
connectAttr "pasted__tail_ctry_btm_visibility.o" "pasted__beebotrig_finalRN.phl[33]"
		;
connectAttr "pasted__tail_ctry_btm_translateX.o" "pasted__beebotrig_finalRN.phl[34]"
		;
connectAttr "pasted__tail_ctry_btm_translateY.o" "pasted__beebotrig_finalRN.phl[35]"
		;
connectAttr "pasted__tail_ctry_btm_translateZ.o" "pasted__beebotrig_finalRN.phl[36]"
		;
connectAttr "pasted__tail_ctry_btm_scaleX.o" "pasted__beebotrig_finalRN.phl[37]"
		;
connectAttr "pasted__tail_ctry_btm_scaleY.o" "pasted__beebotrig_finalRN.phl[38]"
		;
connectAttr "pasted__tail_ctry_btm_scaleZ.o" "pasted__beebotrig_finalRN.phl[39]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_rotateY.o" "pasted__beebotrig_finalRN.phl[40]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_rotateX.o" "pasted__beebotrig_finalRN.phl[41]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_rotateZ.o" "pasted__beebotrig_finalRN.phl[42]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_visibility.o" "pasted__beebotrig_finalRN.phl[43]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_translateX.o" "pasted__beebotrig_finalRN.phl[44]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_translateY.o" "pasted__beebotrig_finalRN.phl[45]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_translateZ.o" "pasted__beebotrig_finalRN.phl[46]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_scaleX.o" "pasted__beebotrig_finalRN.phl[47]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_scaleY.o" "pasted__beebotrig_finalRN.phl[48]"
		;
connectAttr "pasted__wing_rotate_ctrl_L_scaleZ.o" "pasted__beebotrig_finalRN.phl[49]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_rotateY.o" "pasted__beebotrig_finalRN.phl[50]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_rotateX.o" "pasted__beebotrig_finalRN.phl[51]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_rotateZ.o" "pasted__beebotrig_finalRN.phl[52]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_visibility.o" "pasted__beebotrig_finalRN.phl[53]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_translateX.o" "pasted__beebotrig_finalRN.phl[54]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_translateY.o" "pasted__beebotrig_finalRN.phl[55]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_translateZ.o" "pasted__beebotrig_finalRN.phl[56]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_scaleX.o" "pasted__beebotrig_finalRN.phl[57]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_scaleY.o" "pasted__beebotrig_finalRN.phl[58]"
		;
connectAttr "pasted__wing_rotate_ctrl_R_scaleZ.o" "pasted__beebotrig_finalRN.phl[59]"
		;
connectAttr "pasted__tailtip_ctrl_translateX.o" "pasted__beebotrig_finalRN.phl[60]"
		;
connectAttr "pasted__tailtip_ctrl_translateY.o" "pasted__beebotrig_finalRN.phl[61]"
		;
connectAttr "pasted__tailtip_ctrl_translateZ.o" "pasted__beebotrig_finalRN.phl[62]"
		;
connectAttr "pasted__tailtip_ctrl_visibility.o" "pasted__beebotrig_finalRN.phl[63]"
		;
connectAttr "pasted__tailtip_ctrl_rotateX.o" "pasted__beebotrig_finalRN.phl[64]"
		;
connectAttr "pasted__tailtip_ctrl_rotateY.o" "pasted__beebotrig_finalRN.phl[65]"
		;
connectAttr "pasted__tailtip_ctrl_rotateZ.o" "pasted__beebotrig_finalRN.phl[66]"
		;
connectAttr "pasted__tailtip_ctrl_scaleX.o" "pasted__beebotrig_finalRN.phl[67]";
connectAttr "pasted__tailtip_ctrl_scaleY.o" "pasted__beebotrig_finalRN.phl[68]";
connectAttr "pasted__tailtip_ctrl_scaleZ.o" "pasted__beebotrig_finalRN.phl[69]";
connectAttr "beebotrig_final:aiStandardSurface1SG1.pa" ":renderPartition.st" -na
		;
connectAttr "beebotrig_final:aiStandardSurface2SG1.pa" ":renderPartition.st" -na
		;
connectAttr "beebotrig_final:aiStandardSurface3.msg" ":defaultShaderList1.s" -na
		;
connectAttr "beebotrig_final:aiStandardSurface4.msg" ":defaultShaderList1.s" -na
		;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pasted__defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "beebotrig_final:defaultRenderLayer1.msg" ":defaultRenderingList1.r"
		 -na;
// End of beebot_idle.ma
