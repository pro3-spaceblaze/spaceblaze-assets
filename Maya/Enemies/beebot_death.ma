//Maya ASCII 2019 scene
//Name: beebot_death.ma
//Last modified: Sun, Jan 12, 2020 01:25:55 AM
//Codeset: 1252
file -rdi 1 -ns "beebotrig_final" -rfn "beebotrig_finalRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Sonja/Hagenberg/pro3/spaceblaze-assets/Maya/Enemies/beebotrig_final.ma";
file -r -ns "beebotrig_final" -dr 1 -rfn "beebotrig_finalRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Sonja/Hagenberg/pro3/spaceblaze-assets/Maya/Enemies/beebotrig_final.ma";
requires maya "2019";
requires -nodeType "gameFbxExporter" "gameFbxExporter" "1.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "3.1.2";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "201812112215-434d8d9c04";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18362)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "695F165B-4A3E-3DA5-6D8D-78862FF7BBD5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -9.8910672990223194 0.78481516189014167 1.5914368048472813 ;
	setAttr ".r" -type "double3" 0.26164727033527324 -442.99999999992707 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "19F70AA1-4FFF-828F-FCFA-A7BD4CD085D1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 10.024541896224864;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "CA2428C5-4CAB-A8A3-444E-4D8DA25868C7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2BEB9F58-4B42-E04D-EA74-589179B9E5BF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "16DF8B4C-4D9F-D4D7-F5C0-67A3C5015B31";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "6A9F941E-442E-2C9F-0F5C-FFA12AF819E5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "BFAD0E84-4D03-020B-F206-83A29D7005EE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "83AEC57C-46F1-97A5-4A2C-798C8B9B7FA8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "EE04B0FF-4419-2A25-3E5B-09B1B22637EF";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "E117A87E-4406-2CE0-4C21-91BC7D9CA869";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "76E70FE1-4B3E-6550-DCF8-76A5D613B097";
createNode displayLayerManager -n "layerManager";
	rename -uid "73FA3976-4F1E-FF7F-3DF0-1DAA0676570F";
createNode displayLayer -n "defaultLayer";
	rename -uid "6CA8F339-4415-1D4D-EB61-5B9ABB7A46CC";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "DA40409D-4478-9236-D108-3CA4F5A85F92";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "15FA5543-4012-2163-60B7-AAA872A5E5C3";
	setAttr ".g" yes;
createNode gameFbxExporter -n "gameExporterPreset1";
	rename -uid "EEA5B48A-454B-2A96-9A40-489DA0934861";
	setAttr ".pn" -type "string" "Model Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".ebm" yes;
	setAttr ".inc" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "gameExporterPreset2";
	rename -uid "4A2837B6-4504-00B2-E990-8E85B3B6EB4F";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 2;
	setAttr ".spt" 2;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "gameExporterPreset3";
	rename -uid "6C5C4AA0-4212-25F4-6537-9B9CDF16BEEA";
	setAttr ".pn" -type "string" "TE Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 3;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode reference -n "beebotrig_finalRN";
	rename -uid "69CD6C7B-4833-81A5-FF83-1BBF720D48D2";
	setAttr -s 88 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"beebotrig_finalRN"
		"beebotrig_finalRN" 0
		"beebotrig_finalRN" 109
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"translateZ" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"rotate" " -type \"double3\" 34.86250045440758072 0 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face" 
		"rotateX" " -av 43.83997385052893492"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top" 
		"rotate" " -type \"double3\" -44.39236434803871845 0 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm" 
		"rotate" " -type \"double3\" -22.85253801026904341 0 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm" 
		"rotateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L" 
		"rotate" " -type \"double3\" 0 14.99999999999999822 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L" 
		"rotateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R" 
		"rotate" " -type \"double3\" 0 -20 0"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R" 
		"rotateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translate" " -type \"double3\" -0.00081057561088160313 -0.11933616085990034 0.020156551480217388"
		
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translateX" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translateY" " -av"
		2 "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl" 
		"translateZ" " -av"
		2 "beebotrig_final:mesh" "displayType" " 2"
		2 "beebotrig_final:ctrlsLayer" "displayType" " 0"
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[1]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.translateX" 
		"beebotrig_finalRN.placeHolderList[2]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.translateY" 
		"beebotrig_finalRN.placeHolderList[3]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.translateZ" 
		"beebotrig_finalRN.placeHolderList[4]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.rotateX" 
		"beebotrig_finalRN.placeHolderList[5]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.rotateY" 
		"beebotrig_finalRN.placeHolderList[6]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.rotateZ" 
		"beebotrig_finalRN.placeHolderList[7]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[8]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[9]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[10]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateX" 
		"beebotrig_finalRN.placeHolderList[11]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateY" 
		"beebotrig_finalRN.placeHolderList[12]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.translateZ" 
		"beebotrig_finalRN.placeHolderList[13]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.rotateX" 
		"beebotrig_finalRN.placeHolderList[14]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.rotateY" 
		"beebotrig_finalRN.placeHolderList[15]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.rotateZ" 
		"beebotrig_finalRN.placeHolderList[16]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[17]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[18]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[19]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[20]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateX" 
		"beebotrig_finalRN.placeHolderList[21]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateY" 
		"beebotrig_finalRN.placeHolderList[22]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.rotateZ" 
		"beebotrig_finalRN.placeHolderList[23]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[24]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[25]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[26]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[27]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.rotateX" 
		"beebotrig_finalRN.placeHolderList[28]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.visibility" 
		"beebotrig_finalRN.placeHolderList[29]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleX" 
		"beebotrig_finalRN.placeHolderList[30]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleY" 
		"beebotrig_finalRN.placeHolderList[31]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:neck_rotate_ctrl|beebotrig_final:face.scaleZ" 
		"beebotrig_finalRN.placeHolderList[32]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateX" 
		"beebotrig_finalRN.placeHolderList[33]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateY" 
		"beebotrig_finalRN.placeHolderList[34]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.rotateZ" 
		"beebotrig_finalRN.placeHolderList[35]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.visibility" 
		"beebotrig_finalRN.placeHolderList[36]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateX" 
		"beebotrig_finalRN.placeHolderList[37]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateY" 
		"beebotrig_finalRN.placeHolderList[38]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.translateZ" 
		"beebotrig_finalRN.placeHolderList[39]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleX" 
		"beebotrig_finalRN.placeHolderList[40]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleY" 
		"beebotrig_finalRN.placeHolderList[41]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top.scaleZ" 
		"beebotrig_finalRN.placeHolderList[42]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateX" 
		"beebotrig_finalRN.placeHolderList[43]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateY" 
		"beebotrig_finalRN.placeHolderList[44]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.rotateZ" 
		"beebotrig_finalRN.placeHolderList[45]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.visibility" 
		"beebotrig_finalRN.placeHolderList[46]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateX" 
		"beebotrig_finalRN.placeHolderList[47]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateY" 
		"beebotrig_finalRN.placeHolderList[48]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.translateZ" 
		"beebotrig_finalRN.placeHolderList[49]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleX" 
		"beebotrig_finalRN.placeHolderList[50]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleY" 
		"beebotrig_finalRN.placeHolderList[51]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:tail_ctrl_top|beebotrig_final:tail_ctry_btm.scaleZ" 
		"beebotrig_finalRN.placeHolderList[52]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateY" 
		"beebotrig_finalRN.placeHolderList[53]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateX" 
		"beebotrig_finalRN.placeHolderList[54]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.rotateZ" 
		"beebotrig_finalRN.placeHolderList[55]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.visibility" 
		"beebotrig_finalRN.placeHolderList[56]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateX" 
		"beebotrig_finalRN.placeHolderList[57]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateY" 
		"beebotrig_finalRN.placeHolderList[58]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.translateZ" 
		"beebotrig_finalRN.placeHolderList[59]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleX" 
		"beebotrig_finalRN.placeHolderList[60]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleY" 
		"beebotrig_finalRN.placeHolderList[61]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_L.scaleZ" 
		"beebotrig_finalRN.placeHolderList[62]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateY" 
		"beebotrig_finalRN.placeHolderList[63]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateX" 
		"beebotrig_finalRN.placeHolderList[64]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.rotateZ" 
		"beebotrig_finalRN.placeHolderList[65]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.visibility" 
		"beebotrig_finalRN.placeHolderList[66]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateX" 
		"beebotrig_finalRN.placeHolderList[67]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateY" 
		"beebotrig_finalRN.placeHolderList[68]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.translateZ" 
		"beebotrig_finalRN.placeHolderList[69]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleX" 
		"beebotrig_finalRN.placeHolderList[70]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleY" 
		"beebotrig_finalRN.placeHolderList[71]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:body_ctrl|beebotrig_final:wing_rotate_ctrl_R.scaleZ" 
		"beebotrig_finalRN.placeHolderList[72]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateX" 
		"beebotrig_finalRN.placeHolderList[73]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateY" 
		"beebotrig_finalRN.placeHolderList[74]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.translateZ" 
		"beebotrig_finalRN.placeHolderList[75]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.visibility" 
		"beebotrig_finalRN.placeHolderList[76]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateX" 
		"beebotrig_finalRN.placeHolderList[77]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateY" 
		"beebotrig_finalRN.placeHolderList[78]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.rotateZ" 
		"beebotrig_finalRN.placeHolderList[79]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleX" 
		"beebotrig_finalRN.placeHolderList[80]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleY" 
		"beebotrig_finalRN.placeHolderList[81]" ""
		5 4 "beebotrig_finalRN" "|beebotrig_final:ctrls|beebotrig_final:Main_ctrl|beebotrig_final:tailtip_ctrl.scaleZ" 
		"beebotrig_finalRN.placeHolderList[82]" ""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion1.message" "beebotrig_finalRN.placeHolderList[83]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion2.message" "beebotrig_finalRN.placeHolderList[84]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion3.message" "beebotrig_finalRN.placeHolderList[85]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion4.message" "beebotrig_finalRN.placeHolderList[86]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion5.message" "beebotrig_finalRN.placeHolderList[87]" 
		""
		5 3 "beebotrig_finalRN" "beebotrig_final:unitConversion6.message" "beebotrig_finalRN.placeHolderList[88]" 
		"";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "632CBDD4-4754-0DCA-CFFD-4BB4DA3F6ED5";
	setAttr ".version" -type "string" "3.1.2";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "8B00F361-48B8-0071-F4EC-38BB8CAFE73D";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "0609C1A1-45CB-E55D-1B3B-41A72EB67227";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "5F384D3D-4B91-1EED-0D76-EE968DFE7F85";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C4CD2A6C-4441-45DF-5149-6E96B226DA65";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1282\n            -height 457\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n"
		+ "            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n"
		+ "            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n"
		+ "\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n"
		+ "                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n"
		+ "            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1282\\n    -height 457\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1282\\n    -height 457\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "185E135F-49C7-5773-AAFB-658C18F92426";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast 0 -aet 200 ";
	setAttr ".st" 6;
createNode animCurveTU -n "tail_ctrl_top_visibility";
	rename -uid "065695E8-4FA7-60D1-5B17-49B3C203795B";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "tail_ctrl_top_translateX";
	rename -uid "7D7D7B91-4149-3857-398A-1492388A10BB";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
createNode animCurveTL -n "tail_ctrl_top_translateY";
	rename -uid "9155925B-4519-2F2D-40E9-71A89A85C89B";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
createNode animCurveTL -n "tail_ctrl_top_translateZ";
	rename -uid "0C729AC1-4841-75C2-7BE7-9FA242F6ABA0";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
createNode animCurveTA -n "tail_ctrl_top_rotateX";
	rename -uid "DE15E9AC-4957-347F-97E6-00825EA81F96";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 20 -44.392364348038718 30 -44.392364348038718;
createNode animCurveTA -n "tail_ctrl_top_rotateY";
	rename -uid "2A6F714B-4813-EC38-0971-9F9A369F9787";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
createNode animCurveTA -n "tail_ctrl_top_rotateZ";
	rename -uid "2271A15F-4FFC-964A-2FAA-5E8300BCCCD2";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
createNode animCurveTU -n "tail_ctrl_top_scaleX";
	rename -uid "F8964CCD-4ECF-F45E-C658-DEB1D5D4E864";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
createNode animCurveTU -n "tail_ctrl_top_scaleY";
	rename -uid "4063979D-4FEB-FBC1-E35B-DEAF8C1B9C9F";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
createNode animCurveTU -n "tail_ctrl_top_scaleZ";
	rename -uid "105C5F18-4860-ADA7-A8C8-468E982F19DF";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
createNode animCurveTU -n "tail_ctry_btm_visibility";
	rename -uid "EC249DB2-4869-6C2E-3308-CCA84EF476AD";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "tail_ctry_btm_translateX";
	rename -uid "EF7C9C7F-4E9C-6CCD-A6FB-ED8EF67104AB";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "tail_ctry_btm_translateY";
	rename -uid "D5F0D19D-44D5-9C92-2F66-DFA55A6F2DA4";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "tail_ctry_btm_translateZ";
	rename -uid "3AE0BE8A-4778-A350-3313-EBB7B910F908";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "tail_ctry_btm_rotateX";
	rename -uid "FEAA7D91-42D5-DAAC-59AE-359F8A9B26D8";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 -22.852538010269043;
createNode animCurveTA -n "tail_ctry_btm_rotateY";
	rename -uid "76EA593A-4888-5069-79B7-4CA775224D7C";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "tail_ctry_btm_rotateZ";
	rename -uid "C33DF889-4A09-E97C-DCD5-D8B3F2ECE5FE";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "tail_ctry_btm_scaleX";
	rename -uid "39331238-4971-F9FE-CDF0-82BD368ED8F8";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "tail_ctry_btm_scaleY";
	rename -uid "38DE5390-4DF8-0A1B-C59C-CDBD9C015AF1";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "tail_ctry_btm_scaleZ";
	rename -uid "D7F0C620-4DF6-F21B-A010-6482A396F6B8";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "wing_rotate_ctrl_R_visibility";
	rename -uid "EBE259FE-4641-E071-1F0C-60A36D7CEC36";
	setAttr ".tan" 9;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "wing_rotate_ctrl_R_translateX";
	rename -uid "5E0565EF-4ADD-D39A-A2B7-14A4DBA0F1AC";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTL -n "wing_rotate_ctrl_R_translateY";
	rename -uid "A2AC6AA7-4CAE-272D-171A-37A36D052EE0";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTL -n "wing_rotate_ctrl_R_translateZ";
	rename -uid "A9ECD1F2-4D4C-57B6-691B-BCAD7A4757E4";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTA -n "wing_rotate_ctrl_R_rotateX";
	rename -uid "7F7EAC77-40E1-C19B-7367-C8BB1C431187";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTA -n "wing_rotate_ctrl_R_rotateY";
	rename -uid "0761A692-4FE4-6BD1-4244-468238372045";
	setAttr ".tan" 2;
	setAttr -s 17 ".ktv[0:16]"  0 0 3 -29.999999999999996 6 0 9 -29.999999999999996
		 12 0 15 -29.999999999999996 18 0 21 -29.999999999999996 24 0 27 -29.999999999999996
		 30 0 33 -29.999999999999996 35 -14.999999999999998 38 -25 41 -20 43 -23 45 -20;
createNode animCurveTA -n "wing_rotate_ctrl_R_rotateZ";
	rename -uid "5AD482C2-486B-4C2A-128F-578B70A82B25";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTU -n "wing_rotate_ctrl_R_scaleX";
	rename -uid "CB00A275-459E-93BE-A95C-0AA7F380A9E9";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
createNode animCurveTU -n "wing_rotate_ctrl_R_scaleY";
	rename -uid "A217C52C-450F-DB53-DC36-7F9C77D82AEE";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
createNode animCurveTU -n "wing_rotate_ctrl_R_scaleZ";
	rename -uid "CFE38899-4B1A-C42D-138F-378EB7A98B17";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
createNode animCurveTU -n "wing_rotate_ctrl_L_visibility";
	rename -uid "4BBB949C-4153-DF93-FCC5-FBB0EB38F1E4";
	setAttr ".tan" 9;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "wing_rotate_ctrl_L_translateX";
	rename -uid "AC58F8AA-4278-0A6C-1D9F-AEA325DB1295";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTL -n "wing_rotate_ctrl_L_translateY";
	rename -uid "34C55581-45B8-CEFB-7889-03971748E85D";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTL -n "wing_rotate_ctrl_L_translateZ";
	rename -uid "7713F6AF-47B1-9779-0DFC-1BB037745A22";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTA -n "wing_rotate_ctrl_L_rotateX";
	rename -uid "82A94304-4AA9-C8A1-A08B-4B966CD8D182";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTA -n "wing_rotate_ctrl_L_rotateY";
	rename -uid "2CF9E13A-4183-DA7C-5CA1-4BAA32FF067F";
	setAttr ".tan" 2;
	setAttr -s 18 ".ktv[0:17]"  0 0 3 29.999999999999996 6 0 9 29.999999999999996
		 12 0 15 29.999999999999996 18 0 21 29.999999999999996 24 0 27 29.999999999999996
		 30 0 35 29.999999999999996 38 10 40 14.999999999999998 42 10 44 14.999999999999998
		 48 10 53 14.999999999999998;
	setAttr -s 18 ".kit[15:17]"  1 2 1;
	setAttr -s 18 ".kot[14:17]"  1 2 1 2;
	setAttr -s 18 ".kix[15:17]"  0.083333333333333481 0.16666666666666674 
		0.083333333333333481;
	setAttr -s 18 ".kiy[15:17]"  0.26179938779914941 -0.08726646259971646 
		0.26179938779914941;
	setAttr -s 18 ".kox[14:17]"  0.083333333333333481 0.16666666666666674 
		0.083333333333333481 1;
	setAttr -s 18 ".koy[14:17]"  0.26179938779914941 -0.08726646259971646 
		0.26179938779914941 0;
createNode animCurveTA -n "wing_rotate_ctrl_L_rotateZ";
	rename -uid "03E48BCB-4568-5E21-F1A1-0B81784F2911";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 24 0;
createNode animCurveTU -n "wing_rotate_ctrl_L_scaleX";
	rename -uid "7D0CA7A8-4EDA-F327-613B-75888A332648";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
createNode animCurveTU -n "wing_rotate_ctrl_L_scaleY";
	rename -uid "9D2E7CFF-48F5-2AD2-4196-8BA1B0B8161C";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
createNode animCurveTU -n "wing_rotate_ctrl_L_scaleZ";
	rename -uid "9DF3F4A8-48EA-F1FF-4E43-78B48449E1FC";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 24 1;
createNode animCurveTU -n "tailtip_ctrl_visibility";
	rename -uid "AF7EC3F8-4F29-0BB8-6A6A-2982D7A6D19E";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "tailtip_ctrl_translateX";
	rename -uid "D0CD9B8B-4451-4745-A887-D3B4FA5DB30D";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 -0.00035913783849084169 20 -0.00081057561088160313;
createNode animCurveTL -n "tailtip_ctrl_translateY";
	rename -uid "2CBD197A-4834-ED15-AC0C-DAA0927FB44F";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 -0.11933616085990034;
createNode animCurveTL -n "tailtip_ctrl_translateZ";
	rename -uid "40DD637C-45C4-9502-2F4D-DE8BFF70C255";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 0.020156551480217388;
createNode animCurveTA -n "tailtip_ctrl_rotateX";
	rename -uid "06F0EB2A-4D6B-D8CF-9054-C7A2A5EC5FD4";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "tailtip_ctrl_rotateY";
	rename -uid "A4824C6A-40FB-96F1-98CD-2FBF1C20DE44";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "tailtip_ctrl_rotateZ";
	rename -uid "88873D49-4FB2-6EEE-8AAB-8F992FA6429B";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "tailtip_ctrl_scaleX";
	rename -uid "76E2251F-4C38-29B8-7719-B5BB14894731";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "tailtip_ctrl_scaleY";
	rename -uid "F56EDDF5-4AF5-0F05-7097-4B979E9B1391";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "tailtip_ctrl_scaleZ";
	rename -uid "DBB94161-432A-F662-9920-6C8D01192A15";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "face_visibility";
	rename -uid "EFF63041-4A88-7D5B-20CD-B3923FF71B43";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "face_rotateX";
	rename -uid "57D91FBC-49CC-98A8-4A28-999263835C95";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0 20 23.28589941573183 30 43.839973850528935;
createNode animCurveTU -n "face_scaleX";
	rename -uid "2120139E-44AD-3C65-7530-309FA2801880";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "face_scaleY";
	rename -uid "B7D37510-45F2-3DF8-80A4-99B1124B20F5";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "face_scaleZ";
	rename -uid "85A51BB5-4872-BAF7-B96D-0DBADF789E62";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "body_ctrl_visibility";
	rename -uid "1B6D9C5C-4CC8-0A88-DE7F-4A8DAD019D9D";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[1]"  5;
createNode animCurveTL -n "body_ctrl_translateX";
	rename -uid "A51C111C-4EC2-7AF2-8B06-45A5EB610B5B";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTL -n "body_ctrl_translateY";
	rename -uid "5E550C08-4880-79BD-C020-0EA9C572BC87";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTL -n "body_ctrl_translateZ";
	rename -uid "5CABA070-440B-5498-BE19-91BCF8A9E3C8";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTU -n "body_ctrl_scaleX";
	rename -uid "0D57017D-47C6-2580-7A2C-DFB4789E4BF6";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTU -n "body_ctrl_scaleY";
	rename -uid "C6D7CB20-47C6-8174-46F0-349C40895E40";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTU -n "body_ctrl_scaleZ";
	rename -uid "5A0A894E-4E46-ACF6-E33C-418F61FF1E35";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 1 30 1;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTU -n "Main_ctrl_visibility";
	rename -uid "A4CB0724-43A4-C841-1444-51AC8B9A3284";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Main_ctrl_translateX";
	rename -uid "66682EB9-4D67-1830-17FE-F494C92970CE";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Main_ctrl_translateY";
	rename -uid "00CDCBC3-45DF-419B-14EB-7580B2BDB735";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0.23959629237651825;
createNode animCurveTL -n "Main_ctrl_translateZ";
	rename -uid "A27BC21E-421A-E879-B405-9586C73E2618";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "Main_ctrl_rotateX";
	rename -uid "54C916E2-49C2-71A9-F766-F9809B4F0420";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "Main_ctrl_rotateY";
	rename -uid "2B0660C4-4F30-3FC9-D196-D78D1E24771C";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "Main_ctrl_rotateZ";
	rename -uid "D3AD731C-46CA-1246-CE55-94B0A817D58D";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "Main_ctrl_scaleX";
	rename -uid "6121675E-458A-1507-AFC0-10A12DC407CD";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Main_ctrl_scaleY";
	rename -uid "CD235176-4F7A-0300-4DB2-DDB57AEA09FF";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Main_ctrl_scaleZ";
	rename -uid "28390E24-479E-87F0-D17D-A1AFB6A81651";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "neck_rotate_ctrl_visibility";
	rename -uid "133BA829-46F6-1191-4A35-83B169DCF6A0";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "neck_rotate_ctrl_rotateX";
	rename -uid "BE2443A5-436A-8FC0-01DE-5DBB18B310C1";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "neck_rotate_ctrl_rotateY";
	rename -uid "1520F28E-42CE-D374-3519-E09EDA89D1FE";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "neck_rotate_ctrl_rotateZ";
	rename -uid "7601E73A-448F-3236-7FF3-A3A54CA5D8BF";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "neck_rotate_ctrl_scaleX";
	rename -uid "A045CBC9-494F-133E-8738-FAA1794711BA";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "neck_rotate_ctrl_scaleY";
	rename -uid "D719AF8D-439B-9F30-1DCF-2D91C02ACDB3";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "neck_rotate_ctrl_scaleZ";
	rename -uid "2D4D0F1D-4BB0-143F-EAB4-4DA4747DF6CD";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "body_ctrl_rotateX";
	rename -uid "5CCA10EC-4060-FAE2-C8EF-FE92F03279DD";
	setAttr ".tan" 3;
	setAttr -s 3 ".ktv[0:2]"  0 0 20 34.862500454407581 30 34.862500454407581;
	setAttr -s 3 ".kit[2]"  2;
	setAttr -s 3 ".kot[2]"  2;
createNode animCurveTA -n "body_ctrl_rotateY";
	rename -uid "5A671800-44BF-4ED7-70FE-2ABBF64B5C9E";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode animCurveTA -n "body_ctrl_rotateZ";
	rename -uid "DCE05DF7-4E55-3C9E-568E-94BB3C436065";
	setAttr ".tan" 3;
	setAttr -s 2 ".ktv[0:1]"  0 0 30 0;
	setAttr -s 2 ".kit[1]"  2;
	setAttr -s 2 ".kot[1]"  2;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "311923D5-407C-63FE-F0CE-C8829C4B2DAB";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -355.95236680810473 -269.04760835662768 ;
	setAttr ".tgi[0].vh" -type "double2" 357.14284295127567 267.85713221345674 ;
	setAttr -s 6 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -91.428573608398438;
	setAttr ".tgi[0].ni[0].y" -45.714286804199219;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -91.428573608398438;
	setAttr ".tgi[0].ni[1].y" 377.14285278320313;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" -91.428573608398438;
	setAttr ".tgi[0].ni[2].y" -451.42855834960938;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" -91.428573608398438;
	setAttr ".tgi[0].ni[3].y" 165.71427917480469;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" -91.428573608398438;
	setAttr ".tgi[0].ni[4].y" 588.5714111328125;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" -91.428573608398438;
	setAttr ".tgi[0].ni[5].y" -248.57142639160156;
	setAttr ".tgi[0].ni[5].nvs" 1923;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".o" 67;
	setAttr ".unw" 67;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 38 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off ".eeaa";
	setAttr -k off ".engm";
	setAttr -k off ".mes";
	setAttr -k off ".emb";
	setAttr -av -k off ".mbbf";
	setAttr -k off ".mbs";
	setAttr -k off ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off ".twa";
	setAttr -k off ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
select -ne :ikSystem;
connectAttr "Main_ctrl_visibility.o" "beebotrig_finalRN.phl[1]";
connectAttr "Main_ctrl_translateX.o" "beebotrig_finalRN.phl[2]";
connectAttr "Main_ctrl_translateY.o" "beebotrig_finalRN.phl[3]";
connectAttr "Main_ctrl_translateZ.o" "beebotrig_finalRN.phl[4]";
connectAttr "Main_ctrl_rotateX.o" "beebotrig_finalRN.phl[5]";
connectAttr "Main_ctrl_rotateY.o" "beebotrig_finalRN.phl[6]";
connectAttr "Main_ctrl_rotateZ.o" "beebotrig_finalRN.phl[7]";
connectAttr "Main_ctrl_scaleX.o" "beebotrig_finalRN.phl[8]";
connectAttr "Main_ctrl_scaleY.o" "beebotrig_finalRN.phl[9]";
connectAttr "Main_ctrl_scaleZ.o" "beebotrig_finalRN.phl[10]";
connectAttr "body_ctrl_translateX.o" "beebotrig_finalRN.phl[11]";
connectAttr "body_ctrl_translateY.o" "beebotrig_finalRN.phl[12]";
connectAttr "body_ctrl_translateZ.o" "beebotrig_finalRN.phl[13]";
connectAttr "body_ctrl_rotateX.o" "beebotrig_finalRN.phl[14]";
connectAttr "body_ctrl_rotateY.o" "beebotrig_finalRN.phl[15]";
connectAttr "body_ctrl_rotateZ.o" "beebotrig_finalRN.phl[16]";
connectAttr "body_ctrl_scaleX.o" "beebotrig_finalRN.phl[17]";
connectAttr "body_ctrl_scaleY.o" "beebotrig_finalRN.phl[18]";
connectAttr "body_ctrl_scaleZ.o" "beebotrig_finalRN.phl[19]";
connectAttr "body_ctrl_visibility.o" "beebotrig_finalRN.phl[20]";
connectAttr "neck_rotate_ctrl_rotateX.o" "beebotrig_finalRN.phl[21]";
connectAttr "neck_rotate_ctrl_rotateY.o" "beebotrig_finalRN.phl[22]";
connectAttr "neck_rotate_ctrl_rotateZ.o" "beebotrig_finalRN.phl[23]";
connectAttr "neck_rotate_ctrl_visibility.o" "beebotrig_finalRN.phl[24]";
connectAttr "neck_rotate_ctrl_scaleX.o" "beebotrig_finalRN.phl[25]";
connectAttr "neck_rotate_ctrl_scaleY.o" "beebotrig_finalRN.phl[26]";
connectAttr "neck_rotate_ctrl_scaleZ.o" "beebotrig_finalRN.phl[27]";
connectAttr "face_rotateX.o" "beebotrig_finalRN.phl[28]";
connectAttr "face_visibility.o" "beebotrig_finalRN.phl[29]";
connectAttr "face_scaleX.o" "beebotrig_finalRN.phl[30]";
connectAttr "face_scaleY.o" "beebotrig_finalRN.phl[31]";
connectAttr "face_scaleZ.o" "beebotrig_finalRN.phl[32]";
connectAttr "tail_ctrl_top_rotateX.o" "beebotrig_finalRN.phl[33]";
connectAttr "tail_ctrl_top_rotateY.o" "beebotrig_finalRN.phl[34]";
connectAttr "tail_ctrl_top_rotateZ.o" "beebotrig_finalRN.phl[35]";
connectAttr "tail_ctrl_top_visibility.o" "beebotrig_finalRN.phl[36]";
connectAttr "tail_ctrl_top_translateX.o" "beebotrig_finalRN.phl[37]";
connectAttr "tail_ctrl_top_translateY.o" "beebotrig_finalRN.phl[38]";
connectAttr "tail_ctrl_top_translateZ.o" "beebotrig_finalRN.phl[39]";
connectAttr "tail_ctrl_top_scaleX.o" "beebotrig_finalRN.phl[40]";
connectAttr "tail_ctrl_top_scaleY.o" "beebotrig_finalRN.phl[41]";
connectAttr "tail_ctrl_top_scaleZ.o" "beebotrig_finalRN.phl[42]";
connectAttr "tail_ctry_btm_rotateX.o" "beebotrig_finalRN.phl[43]";
connectAttr "tail_ctry_btm_rotateY.o" "beebotrig_finalRN.phl[44]";
connectAttr "tail_ctry_btm_rotateZ.o" "beebotrig_finalRN.phl[45]";
connectAttr "tail_ctry_btm_visibility.o" "beebotrig_finalRN.phl[46]";
connectAttr "tail_ctry_btm_translateX.o" "beebotrig_finalRN.phl[47]";
connectAttr "tail_ctry_btm_translateY.o" "beebotrig_finalRN.phl[48]";
connectAttr "tail_ctry_btm_translateZ.o" "beebotrig_finalRN.phl[49]";
connectAttr "tail_ctry_btm_scaleX.o" "beebotrig_finalRN.phl[50]";
connectAttr "tail_ctry_btm_scaleY.o" "beebotrig_finalRN.phl[51]";
connectAttr "tail_ctry_btm_scaleZ.o" "beebotrig_finalRN.phl[52]";
connectAttr "wing_rotate_ctrl_L_rotateY.o" "beebotrig_finalRN.phl[53]";
connectAttr "wing_rotate_ctrl_L_rotateX.o" "beebotrig_finalRN.phl[54]";
connectAttr "wing_rotate_ctrl_L_rotateZ.o" "beebotrig_finalRN.phl[55]";
connectAttr "wing_rotate_ctrl_L_visibility.o" "beebotrig_finalRN.phl[56]";
connectAttr "wing_rotate_ctrl_L_translateX.o" "beebotrig_finalRN.phl[57]";
connectAttr "wing_rotate_ctrl_L_translateY.o" "beebotrig_finalRN.phl[58]";
connectAttr "wing_rotate_ctrl_L_translateZ.o" "beebotrig_finalRN.phl[59]";
connectAttr "wing_rotate_ctrl_L_scaleX.o" "beebotrig_finalRN.phl[60]";
connectAttr "wing_rotate_ctrl_L_scaleY.o" "beebotrig_finalRN.phl[61]";
connectAttr "wing_rotate_ctrl_L_scaleZ.o" "beebotrig_finalRN.phl[62]";
connectAttr "wing_rotate_ctrl_R_rotateY.o" "beebotrig_finalRN.phl[63]";
connectAttr "wing_rotate_ctrl_R_rotateX.o" "beebotrig_finalRN.phl[64]";
connectAttr "wing_rotate_ctrl_R_rotateZ.o" "beebotrig_finalRN.phl[65]";
connectAttr "wing_rotate_ctrl_R_visibility.o" "beebotrig_finalRN.phl[66]";
connectAttr "wing_rotate_ctrl_R_translateX.o" "beebotrig_finalRN.phl[67]";
connectAttr "wing_rotate_ctrl_R_translateY.o" "beebotrig_finalRN.phl[68]";
connectAttr "wing_rotate_ctrl_R_translateZ.o" "beebotrig_finalRN.phl[69]";
connectAttr "wing_rotate_ctrl_R_scaleX.o" "beebotrig_finalRN.phl[70]";
connectAttr "wing_rotate_ctrl_R_scaleY.o" "beebotrig_finalRN.phl[71]";
connectAttr "wing_rotate_ctrl_R_scaleZ.o" "beebotrig_finalRN.phl[72]";
connectAttr "tailtip_ctrl_translateX.o" "beebotrig_finalRN.phl[73]";
connectAttr "tailtip_ctrl_translateY.o" "beebotrig_finalRN.phl[74]";
connectAttr "tailtip_ctrl_translateZ.o" "beebotrig_finalRN.phl[75]";
connectAttr "tailtip_ctrl_visibility.o" "beebotrig_finalRN.phl[76]";
connectAttr "tailtip_ctrl_rotateX.o" "beebotrig_finalRN.phl[77]";
connectAttr "tailtip_ctrl_rotateY.o" "beebotrig_finalRN.phl[78]";
connectAttr "tailtip_ctrl_rotateZ.o" "beebotrig_finalRN.phl[79]";
connectAttr "tailtip_ctrl_scaleX.o" "beebotrig_finalRN.phl[80]";
connectAttr "tailtip_ctrl_scaleY.o" "beebotrig_finalRN.phl[81]";
connectAttr "tailtip_ctrl_scaleZ.o" "beebotrig_finalRN.phl[82]";
connectAttr "beebotrig_finalRN.phl[83]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "beebotrig_finalRN.phl[84]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "beebotrig_finalRN.phl[85]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "beebotrig_finalRN.phl[86]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "beebotrig_finalRN.phl[87]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "beebotrig_finalRN.phl[88]" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of beebot_death.ma
