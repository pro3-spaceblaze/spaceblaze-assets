//Maya ASCII 2019 scene
//Name: egg_walk_final.ma
//Last modified: Sat, Jan 11, 2020 02:13:58 PM
//Codeset: 1252
file -rdi 1 -ns "eggrig_final" -rfn "eggrig_finalRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Sonja/Hagenberg/robots/eggrig_final.ma";
file -r -ns "eggrig_final" -dr 1 -rfn "eggrig_finalRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Sonja/Hagenberg/robots/eggrig_final.ma";
requires maya "2019";
requires -nodeType "gameFbxExporter" "gameFbxExporter" "1.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiStandardSurface"
		 "mtoa" "3.1.2";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "201812112215-434d8d9c04";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18362)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "A4235EC9-4EAF-8A68-8A76-179C8DE2BFE8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.4283754092922878 2.0094409209499391 -6.8684209133870917 ;
	setAttr ".r" -type "double3" 17627.661645687138 -4127.3999999923917 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "AAAA1A4B-4C77-10C8-0E17-148C872C6CC0";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 6.8219422171178152;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.33201749956824966 -0.23878619729330319 -0.3512973471150308 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "9B1DF931-414D-01A9-B62B-178AEE020ED6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.13636935129872851 1000.494150784524 -0.020596506796591134 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "B5089D66-4D27-8430-A3AF-C89E687CB224";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.7329360453517;
	setAttr ".ow" 7.0024735217212379;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0.34101213100446925 -0.23878526082775992 -1.4407693877175325 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "E075BEC5-49D8-3B8D-65F5-88AD6B98923E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.024857742993614956 0.4021791829892471 1000.1952532342027 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "BAC8E445-476E-D87D-5966-5BBAC243589B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1952532342027;
	setAttr ".ow" 1.0670609834546509;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0 -6.1629758220391547e-33 1.1102230246251565e-16 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "DD021256-4C4F-1F48-A6E7-8A877495387A";
	setAttr ".t" -type "double3" 1000.2292192488309 0.35465643245926998 -0.22770699203911271 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "FC2F8CCC-486E-907E-5D92-96A835992776";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.2292192488309;
	setAttr ".ow" 4.2359225211260441;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 -0.23491326886321401 1.6653345369377348e-16 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "back1";
	rename -uid "5943E7D9-46DF-784D-D4AD-9EB6211C611A";
	setAttr ".t" -type "double3" -0.056266927847285562 0.45553043463427062 -1000.1000064371819 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode camera -n "backShape1" -p "back1";
	rename -uid "C401FF40-4343-4EF4-D23B-2290A0D48696";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 999.75070903127857;
	setAttr ".ow" 0.49896488505665326;
	setAttr ".imn" -type "string" "back1";
	setAttr ".den" -type "string" "back1_depth";
	setAttr ".man" -type "string" "back1_mask";
	setAttr ".tp" -type "double3" 0.0074574037152359829 0.48600805246753881 -0.349297405903361 ;
	setAttr ".hc" -type "string" "viewSet -b %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "7D2F1FE0-4171-B181-0018-E5A2197AC796";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "80F84574-4539-6BF0-9CE1-CEBAEB9624B9";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "E4D3C8EE-4EAF-3FD1-BF3B-44B657206D57";
createNode displayLayerManager -n "layerManager";
	rename -uid "48F5A493-465E-1FB5-915F-FE997052ED49";
	setAttr ".cdl" 11;
	setAttr -s 11 ".dli[1:10]"  1 2 11 4 5 6 7 8 
		9 10;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "FA8DBEF5-4CFB-00F0-7548-46BB9F2722EA";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F3AAC6BF-4814-5FA2-6AF8-8EBDFCB2B12C";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "161122D9-422B-7819-F08D-3F877CBEDE54";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "1211426F-467B-DD99-DD57-47AF03A78C93";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"all\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1282\n            -height 344\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n"
		+ "            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n"
		+ "            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n"
		+ "\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n"
		+ "                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n"
		+ "            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1282\\n    -height 344\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"all\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1282\\n    -height 344\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "0832931A-44EA-905B-277B-77AF8F84CA86";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 24 -ast 0 -aet 24 ";
	setAttr ".st" 6;
createNode aiStandardSurface -n "aiStandardSurface1";
	rename -uid "4BE4C55B-4475-9798-8E66-D1A8FB70114E";
	setAttr ".base_color" -type "float3" 0.0060000001 0.0060000001 0.0060000001 ;
createNode shadingEngine -n "aiStandardSurface1SG";
	rename -uid "7C60186D-4155-5E5B-8DB2-2189241D446B";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "99A78441-49F5-947A-49CE-6C930732CC34";
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "632CBDD4-4754-0DCA-CFFD-4BB4DA3F6ED5";
	setAttr ".version" -type "string" "3.1.2";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "8B00F361-48B8-0071-F4EC-38BB8CAFE73D";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "0609C1A1-45CB-E55D-1B3B-41A72EB67227";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "5F384D3D-4B91-1EED-0D76-EE968DFE7F85";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode aiStandardSurface -n "aiStandardSurface2";
	rename -uid "F810B9FA-4A89-6203-97BC-FC80DA2A3C88";
	setAttr ".base_color" -type "float3" 0.91329998 0.0108 0 ;
createNode shadingEngine -n "aiStandardSurface2SG";
	rename -uid "56CD36EC-48D6-B73C-D45F-71B906EC46C2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "A94D8CEB-4362-1D33-EB09-99BFCE090D72";
createNode displayLayer -n "mesh";
	rename -uid "FFDF00D5-436C-46F9-BEC1-749B59BD2AB0";
	setAttr ".dt" 2;
	setAttr ".c" 15;
	setAttr ".do" 1;
createNode displayLayer -n "ctrllayer";
	rename -uid "CDAC49BF-4BD3-0E3F-B037-4987810994B7";
	setAttr ".c" 29;
	setAttr ".do" 2;
createNode polyExtrudeEdge -n "polyExtrudeEdge77";
	rename -uid "4057A7F7-4431-C77C-55A0-7687FF4B9502";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[96:103]";
	setAttr ".ix" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -2.8330772466548515 0.78051001456659364 6.326182673000738 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -2.8330772466548515 0.78051001456659364 6.326182673000738 1;
	setAttr ".pvt" -type "float3" -2.8330772 0.74201363 6.4427419 ;
	setAttr ".rs" 48944;
	setAttr ".lt" -type "double3" -1.2663481374630692e-16 0.035300693834484184 -7.6588041464376033e-16 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.3290893901147411 0.27102443147984168 6.2871882396615355 ;
	setAttr ".cbx" -type "double3" -2.3370648636673446 1.2130026738667765 6.5982964926052308 ;
createNode polyExtrudeFace -n "polyExtrudeFace87";
	rename -uid "BF52CBC9-4239-2B99-ADF5-F2AA7339DC56";
	setAttr ".ics" -type "componentList" 2 "f[31]" "f[39]";
	setAttr ".ix" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -1.237370651658251 0.78059761600866207 6.326182673000738 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -1.237370651658251 0.78059761600866207 6.326182673000738 1;
	setAttr ".pvt" -type "float3" -1.6419077 1.0097425 6.2752514 ;
	setAttr ".rs" 46726;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.7027399970770074 0.93565412048320462 6.1197758549170373 ;
	setAttr ".cbx" -type "double3" -1.5810753870035659 1.0912825516822895 6.4307272433252622 ;
createNode polyExtrudeFace -n "polyExtrudeFace95";
	rename -uid "6936CB3D-4C95-7043-9A12-89A17D66AC55";
	setAttr ".ics" -type "componentList" 2 "f[22]" "f[25]";
	setAttr ".ix" -type "matrix" 0.46349038772053064 0 0 0 0 0.46349038772053064 0 0
		 0 0 0.46349038772053064 0 -7.9509430838345159 1.0263017414812494 5.3607038008016374 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.46349038772053064 0 0 0 0 0.46349038772053064 0 0
		 0 0 0.46349038772053064 0 -7.9509430838345159 1.0263017414812494 5.3607038008016374 1;
	setAttr ".pvt" -type "float3" -7.950943 0.85730112 5.2243176 ;
	setAttr ".rs" 46044;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.0281914771835741 0.79455654762098415 5.0879309671467983 ;
	setAttr ".cbx" -type "double3" -7.8736946766723674 0.92004567274274174 5.3607038008016374 ;
createNode polyExtrudeFace -n "polyExtrudeFace106";
	rename -uid "0B4873D4-4D5B-A660-885F-94BC73EF92B2";
	setAttr ".ics" -type "componentList" 4 "f[6:7]" "f[18:19]" "f[28:29]" "f[38:39]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pvt" -type "float3" -7.4275131 0.33314458 2.5058122 ;
	setAttr ".rs" 52264;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.1254854202270508 0.33314454555511475 1.8212032318115234 ;
	setAttr ".cbx" -type "double3" -6.7295408248901367 0.33314460515975952 3.1904211044311523 ;
createNode aiStandardSurface -n "eggboitryrigtry2:aiStandardSurface1";
	rename -uid "E11CBFFC-4706-059D-E31C-9AA1F315F195";
	setAttr ".base_color" -type "float3" 0.0060000001 0.0060000001 0.0060000001 ;
createNode shadingEngine -n "eggboitryrigtry2:aiStandardSurface1SG";
	rename -uid "F9990BEF-47F8-874F-7C86-FEAC46CC5068";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "eggboitryrigtry2:materialInfo1";
	rename -uid "B96E64D5-4DD8-8F69-BCA7-9EA5DC0D89FA";
createNode aiStandardSurface -n "eggboitryrigtry2:aiStandardSurface2";
	rename -uid "5DAD6C68-4244-DDD3-FC69-BCB662886F9B";
	setAttr ".base_color" -type "float3" 0.91329998 0.0108 0 ;
createNode shadingEngine -n "eggboitryrigtry2:aiStandardSurface2SG";
	rename -uid "606FF215-412D-D74F-16F7-0591E4AB4FD3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "eggboitryrigtry2:materialInfo2";
	rename -uid "CBB7638F-4CD6-334B-D6A8-20BD8F523F79";
createNode polyExtrudeEdge -n "eggboitryrigtry2:polyExtrudeEdge77";
	rename -uid "441C9FC8-4594-0B7C-CBC3-E5A0CFF5222D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[96:103]";
	setAttr ".ix" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -2.8330772466548515 0.78051001456659364 6.326182673000738 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -2.8330772466548515 0.78051001456659364 6.326182673000738 1;
	setAttr ".pvt" -type "float3" -2.8330772 0.74201363 6.4427419 ;
	setAttr ".rs" 48944;
	setAttr ".lt" -type "double3" -1.2663481374630692e-16 0.035300693834484184 -7.6588041464376033e-16 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.3290893901147411 0.27102443147984168 6.2871882396615355 ;
	setAttr ".cbx" -type "double3" -2.3370648636673446 1.2130026738667765 6.5982964926052308 ;
createNode polyExtrudeFace -n "eggboitryrigtry2:polyExtrudeFace87";
	rename -uid "65FBE8E7-4804-D937-AE81-6C9A8D72C442";
	setAttr ".ics" -type "componentList" 2 "f[31]" "f[39]";
	setAttr ".ix" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -1.237370651658251 0.78059761600866207 6.326182673000738 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.50232582198726106 0 0 0 0 0.10680711128865752 -0.32339086513199722 0
		 0 0.47698429605282699 0.15753479854992844 0 -1.237370651658251 0.78059761600866207 6.326182673000738 1;
	setAttr ".pvt" -type "float3" -1.6419077 1.0097425 6.2752514 ;
	setAttr ".rs" 46726;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.7027399970770074 0.93565412048320462 6.1197758549170373 ;
	setAttr ".cbx" -type "double3" -1.5810753870035659 1.0912825516822895 6.4307272433252622 ;
createNode polyExtrudeFace -n "eggboitryrigtry2:polyExtrudeFace95";
	rename -uid "F06EA84E-4418-7768-A8B3-3A8485452E77";
	setAttr ".ics" -type "componentList" 2 "f[22]" "f[25]";
	setAttr ".ix" -type "matrix" 0.46349038772053064 0 0 0 0 0.46349038772053064 0 0
		 0 0 0.46349038772053064 0 -7.9509430838345159 1.0263017414812494 5.3607038008016374 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 0.46349038772053064 0 0 0 0 0.46349038772053064 0 0
		 0 0 0.46349038772053064 0 -7.9509430838345159 1.0263017414812494 5.3607038008016374 1;
	setAttr ".pvt" -type "float3" -7.950943 0.85730112 5.2243176 ;
	setAttr ".rs" 46044;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.0281914771835741 0.79455654762098415 5.0879309671467983 ;
	setAttr ".cbx" -type "double3" -7.8736946766723674 0.92004567274274174 5.3607038008016374 ;
createNode polyExtrudeFace -n "eggboitryrigtry2:polyExtrudeFace106";
	rename -uid "BF8F962D-41DA-4726-9D38-A7827360C4D7";
	setAttr ".ics" -type "componentList" 4 "f[6:7]" "f[18:19]" "f[28:29]" "f[38:39]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".mp" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pvt" -type "float3" -7.4275131 0.33314458 2.5058122 ;
	setAttr ".rs" 52264;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -8.1254854202270508 0.33314454555511475 1.8212032318115234 ;
	setAttr ".cbx" -type "double3" -6.7295408248901367 0.33314460515975952 3.1904211044311523 ;
createNode displayLayer -n "jointsandotherthingsyoudontneedtosee";
	rename -uid "BC0E8BED-4ACF-10DA-B519-FC94968299A7";
	setAttr ".v" no;
	setAttr ".c" 28;
	setAttr ".do" 3;
createNode gameFbxExporter -n "gameExporterPreset1";
	rename -uid "97526B19-42D6-127B-7964-1BAE692D6C0A";
	setAttr ".pn" -type "string" "Model Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".ebm" yes;
	setAttr ".inc" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "gameExporterPreset2";
	rename -uid "E3822C72-499A-4BF4-01C8-AF8E1F9E5B06";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 2;
	setAttr ".spt" 2;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode gameFbxExporter -n "gameExporterPreset3";
	rename -uid "678F82FB-49AF-4F18-21D1-0C97DAEDB51A";
	setAttr ".pn" -type "string" "TE Anim Default";
	setAttr ".ils" yes;
	setAttr ".eti" 3;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201800";
createNode reference -n "eggrig_finalRN";
	rename -uid "1C02C429-40D8-513E-E387-149D14A0C216";
	setAttr -s 54 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"eggrig_finalRN"
		"eggrig_finalRN" 0
		"eggrig_finalRN" 55
		2 "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl" "visibility" 
		" -av 1"
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.translateX" 
		"eggrig_finalRN.placeHolderList[1]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.translateY" 
		"eggrig_finalRN.placeHolderList[2]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.translateZ" 
		"eggrig_finalRN.placeHolderList[3]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.rotateX" 
		"eggrig_finalRN.placeHolderList[4]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.rotateY" 
		"eggrig_finalRN.placeHolderList[5]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.rotateZ" 
		"eggrig_finalRN.placeHolderList[6]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.scaleX" 
		"eggrig_finalRN.placeHolderList[7]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.scaleY" 
		"eggrig_finalRN.placeHolderList[8]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.scaleZ" 
		"eggrig_finalRN.placeHolderList[9]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_1.visibility" 
		"eggrig_finalRN.placeHolderList[10]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.translateX" 
		"eggrig_finalRN.placeHolderList[11]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.translateY" 
		"eggrig_finalRN.placeHolderList[12]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.translateZ" 
		"eggrig_finalRN.placeHolderList[13]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.rotateX" 
		"eggrig_finalRN.placeHolderList[14]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.rotateY" 
		"eggrig_finalRN.placeHolderList[15]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.rotateZ" 
		"eggrig_finalRN.placeHolderList[16]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.scaleX" 
		"eggrig_finalRN.placeHolderList[17]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.scaleY" 
		"eggrig_finalRN.placeHolderList[18]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.scaleZ" 
		"eggrig_finalRN.placeHolderList[19]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_2.visibility" 
		"eggrig_finalRN.placeHolderList[20]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.translateX" 
		"eggrig_finalRN.placeHolderList[21]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.translateY" 
		"eggrig_finalRN.placeHolderList[22]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.translateZ" 
		"eggrig_finalRN.placeHolderList[23]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.rotateX" 
		"eggrig_finalRN.placeHolderList[24]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.rotateY" 
		"eggrig_finalRN.placeHolderList[25]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.rotateZ" 
		"eggrig_finalRN.placeHolderList[26]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.scaleX" 
		"eggrig_finalRN.placeHolderList[27]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.scaleY" 
		"eggrig_finalRN.placeHolderList[28]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.scaleZ" 
		"eggrig_finalRN.placeHolderList[29]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_3.visibility" 
		"eggrig_finalRN.placeHolderList[30]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.translateX" 
		"eggrig_finalRN.placeHolderList[31]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.translateY" 
		"eggrig_finalRN.placeHolderList[32]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.translateZ" 
		"eggrig_finalRN.placeHolderList[33]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.rotateX" 
		"eggrig_finalRN.placeHolderList[34]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.rotateY" 
		"eggrig_finalRN.placeHolderList[35]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.rotateZ" 
		"eggrig_finalRN.placeHolderList[36]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.scaleX" 
		"eggrig_finalRN.placeHolderList[37]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.scaleY" 
		"eggrig_finalRN.placeHolderList[38]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.scaleZ" 
		"eggrig_finalRN.placeHolderList[39]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:foot_ctrl_4.visibility" 
		"eggrig_finalRN.placeHolderList[40]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.translateX" 
		"eggrig_finalRN.placeHolderList[41]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.translateY" 
		"eggrig_finalRN.placeHolderList[42]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.translateZ" 
		"eggrig_finalRN.placeHolderList[43]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.visibility" 
		"eggrig_finalRN.placeHolderList[44]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.scaleX" 
		"eggrig_finalRN.placeHolderList[45]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.scaleY" 
		"eggrig_finalRN.placeHolderList[46]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl.scaleZ" 
		"eggrig_finalRN.placeHolderList[47]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.rotateX" 
		"eggrig_finalRN.placeHolderList[48]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.rotateY" 
		"eggrig_finalRN.placeHolderList[49]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.rotateZ" 
		"eggrig_finalRN.placeHolderList[50]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.visibility" 
		"eggrig_finalRN.placeHolderList[51]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.scaleX" 
		"eggrig_finalRN.placeHolderList[52]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.scaleY" 
		"eggrig_finalRN.placeHolderList[53]" ""
		5 4 "eggrig_finalRN" "|eggrig_final:ctrls|eggrig_final:Main_ctrl|eggrig_final:body_ctrl|eggrig_final:rotatectrl.scaleZ" 
		"eggrig_finalRN.placeHolderList[54]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode animCurveTU -n "body_ctrl_visibility1";
	rename -uid "86254112-4705-5069-2A6E-C1A3AFE29909";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "body_ctrl_translateX";
	rename -uid "1CA9925F-4C02-D789-5106-C99230C379CF";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 24 0;
createNode animCurveTL -n "body_ctrl_translateY";
	rename -uid "9D42C69D-49FA-850B-1C4A-F383E277BD30";
	setAttr ".tan" 2;
	setAttr -s 7 ".ktv[0:6]"  0 0 2 -0.035 6 0.035305020352848604 12 0
		 14 -0.035 18 0.035 24 0;
createNode animCurveTL -n "body_ctrl_translateZ";
	rename -uid "EEB8D7A9-4999-A72B-D018-EEADCC64C948";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 24 0;
createNode animCurveTU -n "body_ctrl_scaleX";
	rename -uid "3F960156-4842-C008-0352-2B864BEDAE46";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
createNode animCurveTU -n "body_ctrl_scaleY";
	rename -uid "055A5FC2-426F-5754-A0A4-5D87A6A74BB1";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
createNode animCurveTU -n "body_ctrl_scaleZ";
	rename -uid "3B1A21A8-45A3-F588-F2E5-69879C064623";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
createNode animCurveTA -n "rotatectrl_rotateX";
	rename -uid "7E750835-4BBF-7335-80C5-2CB84F973AAB";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 24 0;
createNode animCurveTA -n "rotatectrl_rotateY";
	rename -uid "0C98C1AF-44DB-9B18-F877-D5B9CFABC5A1";
	setAttr ".tan" 2;
	setAttr -s 5 ".ktv[0:4]"  0 10 1 10.1 12 -10 13 -10.1 24 10;
createNode animCurveTA -n "rotatectrl_rotateZ";
	rename -uid "0C29AC83-4E1E-E39A-A2B8-5AA521208DD5";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 5 12 -5 24 5;
createNode animCurveTU -n "rotatectrl_visibility";
	rename -uid "31DE2997-4D68-ACD1-C57D-DD9DE321175E";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rotatectrl_scaleX";
	rename -uid "4AB4DFAE-4A82-0B97-C5F5-7AAE01360F3D";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
createNode animCurveTU -n "rotatectrl_scaleY";
	rename -uid "6F60E748-44B5-E297-921B-2C8A14CBF117";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
createNode animCurveTU -n "rotatectrl_scaleZ";
	rename -uid "2C248846-4FC3-D331-C407-C1B1388D115A";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 24 1;
createNode animCurveTL -n "foot_ctrl_1_translateX";
	rename -uid "23D7FD51-482F-E7D3-6026-828E5694E9D3";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTL -n "foot_ctrl_1_translateY";
	rename -uid "41365F16-4DE3-F48D-6E5F-809507E3435C";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  0 0 12 0 15 0.09 18 0.1 21 0.09 24 0;
createNode animCurveTL -n "foot_ctrl_1_translateZ";
	rename -uid "C40B0BD6-44E1-5466-2BE4-D5823E345A8C";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 -0.182 12 0.182 24 -0.182;
createNode animCurveTA -n "foot_ctrl_1_rotateX";
	rename -uid "BE4412CB-4B6F-EF61-3F95-819AFCD6C355";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTA -n "foot_ctrl_1_rotateY";
	rename -uid "D5EB16B4-4723-E08B-C725-93882C73A41D";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTA -n "foot_ctrl_1_rotateZ";
	rename -uid "4A4FAEF4-436B-1A0B-3560-11BB400B74EF";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTU -n "foot_ctrl_1_scaleX";
	rename -uid "8924ABA9-4AFC-FA4E-6B19-ADA7415320AC";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
createNode animCurveTU -n "foot_ctrl_1_scaleY";
	rename -uid "3FBD637E-4A4C-915B-AEA1-F599B906841D";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
createNode animCurveTU -n "foot_ctrl_1_scaleZ";
	rename -uid "6D23C1C6-42CF-4D54-D9DB-9DAB523A6E43";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
createNode animCurveTU -n "foot_ctrl_1_visibility";
	rename -uid "1EE7449B-4B40-5ED9-E948-D29E6BD2C892";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "foot_ctrl_2_translateX";
	rename -uid "AF86F9D8-46FD-5D75-6651-FFBF3F13015D";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "foot_ctrl_2_translateY";
	rename -uid "07A5507C-4840-F300-5062-DA9348D89057";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0.09 6 0.1 9 0.09 12 0 24 0;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  0.125;
	setAttr -s 6 ".kiy[5]"  -0.09;
	setAttr -s 6 ".kox[5]"  0.5;
	setAttr -s 6 ".koy[5]"  0.044999999999999991;
createNode animCurveTL -n "foot_ctrl_2_translateZ";
	rename -uid "05E9482A-4260-FD5C-1CD2-81A2E9221F55";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0.18164617234576053 12 -0.182 24 0.182;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  0.125;
	setAttr -s 3 ".kiy[2]"  -0.134;
	setAttr -s 3 ".kox[2]"  0.5;
	setAttr -s 3 ".koy[2]"  -0.60300000000000009;
createNode animCurveTA -n "foot_ctrl_2_rotateX";
	rename -uid "856D35DF-451C-356B-E795-799508A3AA38";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "foot_ctrl_2_rotateY";
	rename -uid "68F3A1FC-4F39-5C33-2687-D1A19CD97942";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "foot_ctrl_2_rotateZ";
	rename -uid "CA6330B2-4FA4-E230-1A23-8287046CE3B0";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "foot_ctrl_2_scaleX";
	rename -uid "A4AFC2F8-414B-1DA1-9B38-29909B0704D6";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "foot_ctrl_2_scaleY";
	rename -uid "75095813-4595-0740-EC21-ED978A03BBF2";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "foot_ctrl_2_scaleZ";
	rename -uid "95A38531-4216-A663-F0D1-79AE86054BC8";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "foot_ctrl_2_visibility";
	rename -uid "7880ADC4-4FDE-2E00-6672-8E94A52B851A";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "foot_ctrl_3_translateX";
	rename -uid "B9338A3E-487B-5B55-B32E-A69B3F56E20F";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTL -n "foot_ctrl_3_translateY";
	rename -uid "246C8A9E-46A7-774B-276A-09A0BD33B84C";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  0 0 12 0 15 0.09 18 0.1 21 0.09 24 0;
createNode animCurveTL -n "foot_ctrl_3_translateZ";
	rename -uid "B8C0B4E3-4855-5C73-DF66-B8A59B845FBF";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 -0.182 12 0.182 24 -0.182;
createNode animCurveTA -n "foot_ctrl_3_rotateX";
	rename -uid "24EAA95B-4F9D-58E5-6AA3-A0BAACE0A39E";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTA -n "foot_ctrl_3_rotateY";
	rename -uid "1DE4B136-48AC-D981-A27F-3EB7781FAC37";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTA -n "foot_ctrl_3_rotateZ";
	rename -uid "49D2AB94-4538-BA3D-71D0-D1A6A7BA5C9A";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 0 12 0;
createNode animCurveTU -n "foot_ctrl_3_scaleX";
	rename -uid "547E5AAB-4393-E75D-4FB0-7CB33AA90396";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
createNode animCurveTU -n "foot_ctrl_3_scaleY";
	rename -uid "80203BB5-42F8-F6DB-EB61-A6B39BEEAAD3";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
createNode animCurveTU -n "foot_ctrl_3_scaleZ";
	rename -uid "6CEB2851-46F3-C783-9F9A-C79DF6F96D56";
	setAttr ".tan" 2;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
createNode animCurveTU -n "foot_ctrl_3_visibility";
	rename -uid "3CAADAED-4A7D-FF2B-12D1-A19121DA7159";
	setAttr ".tan" 9;
	setAttr -s 2 ".ktv[0:1]"  0 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "foot_ctrl_4_translateX";
	rename -uid "0FCC7817-48F9-DFBB-35A6-E3B14DE2186C";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "foot_ctrl_4_translateY";
	rename -uid "DF23DE4E-470E-9DEC-BEDC-2AB6A9885A4B";
	setAttr ".tan" 2;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0.09 6 0.1 9 0.09 12 0 24 0;
createNode animCurveTL -n "foot_ctrl_4_translateZ";
	rename -uid "40C02923-44B0-BC78-028F-01AD5641862F";
	setAttr ".tan" 2;
	setAttr -s 3 ".ktv[0:2]"  0 0.18164617234576053 12 -0.182 24 0.182;
createNode animCurveTA -n "foot_ctrl_4_rotateX";
	rename -uid "18156AB3-4321-7AC5-4EF7-B686CBE5CEC1";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "foot_ctrl_4_rotateY";
	rename -uid "CBD9211F-4D40-467C-6D52-6EA6FDC92D68";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "foot_ctrl_4_rotateZ";
	rename -uid "9C3AFB63-4A1E-154E-FB5E-E984DC001D8B";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "foot_ctrl_4_scaleX";
	rename -uid "2716E444-4113-0B55-6002-82A520A353C5";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "foot_ctrl_4_scaleY";
	rename -uid "1D98405B-4194-1C1B-EB6F-C4B7B87E8569";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "foot_ctrl_4_scaleZ";
	rename -uid "8CFDAE07-45C3-E344-66F2-D4AC759D53BB";
	setAttr ".tan" 2;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "foot_ctrl_4_visibility";
	rename -uid "3667F16B-40B1-E984-608E-FE9A26E57233";
	setAttr ".tan" 9;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  5;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "3E70D33C-4D7B-8CA5-8BCC-04A255B933AD";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -355.95236680810473 -269.04760835662768 ;
	setAttr ".tgi[0].vh" -type "double2" 357.14284295127567 267.85713221345674 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".o" 24;
	setAttr ".unw" 24;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 11 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 13 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 48 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
select -ne :initialParticleSE;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off ".eeaa";
	setAttr -k off ".engm";
	setAttr -k off ".mes";
	setAttr -k off ".emb";
	setAttr -av -k off ".mbbf";
	setAttr -k off ".mbs";
	setAttr -k off ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off ".twa";
	setAttr -k off ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
select -ne :ikSystem;
	setAttr -s 2 ".sol";
connectAttr "foot_ctrl_1_translateX.o" "eggrig_finalRN.phl[1]";
connectAttr "foot_ctrl_1_translateY.o" "eggrig_finalRN.phl[2]";
connectAttr "foot_ctrl_1_translateZ.o" "eggrig_finalRN.phl[3]";
connectAttr "foot_ctrl_1_rotateX.o" "eggrig_finalRN.phl[4]";
connectAttr "foot_ctrl_1_rotateY.o" "eggrig_finalRN.phl[5]";
connectAttr "foot_ctrl_1_rotateZ.o" "eggrig_finalRN.phl[6]";
connectAttr "foot_ctrl_1_scaleX.o" "eggrig_finalRN.phl[7]";
connectAttr "foot_ctrl_1_scaleY.o" "eggrig_finalRN.phl[8]";
connectAttr "foot_ctrl_1_scaleZ.o" "eggrig_finalRN.phl[9]";
connectAttr "foot_ctrl_1_visibility.o" "eggrig_finalRN.phl[10]";
connectAttr "foot_ctrl_2_translateX.o" "eggrig_finalRN.phl[11]";
connectAttr "foot_ctrl_2_translateY.o" "eggrig_finalRN.phl[12]";
connectAttr "foot_ctrl_2_translateZ.o" "eggrig_finalRN.phl[13]";
connectAttr "foot_ctrl_2_rotateX.o" "eggrig_finalRN.phl[14]";
connectAttr "foot_ctrl_2_rotateY.o" "eggrig_finalRN.phl[15]";
connectAttr "foot_ctrl_2_rotateZ.o" "eggrig_finalRN.phl[16]";
connectAttr "foot_ctrl_2_scaleX.o" "eggrig_finalRN.phl[17]";
connectAttr "foot_ctrl_2_scaleY.o" "eggrig_finalRN.phl[18]";
connectAttr "foot_ctrl_2_scaleZ.o" "eggrig_finalRN.phl[19]";
connectAttr "foot_ctrl_2_visibility.o" "eggrig_finalRN.phl[20]";
connectAttr "foot_ctrl_3_translateX.o" "eggrig_finalRN.phl[21]";
connectAttr "foot_ctrl_3_translateY.o" "eggrig_finalRN.phl[22]";
connectAttr "foot_ctrl_3_translateZ.o" "eggrig_finalRN.phl[23]";
connectAttr "foot_ctrl_3_rotateX.o" "eggrig_finalRN.phl[24]";
connectAttr "foot_ctrl_3_rotateY.o" "eggrig_finalRN.phl[25]";
connectAttr "foot_ctrl_3_rotateZ.o" "eggrig_finalRN.phl[26]";
connectAttr "foot_ctrl_3_scaleX.o" "eggrig_finalRN.phl[27]";
connectAttr "foot_ctrl_3_scaleY.o" "eggrig_finalRN.phl[28]";
connectAttr "foot_ctrl_3_scaleZ.o" "eggrig_finalRN.phl[29]";
connectAttr "foot_ctrl_3_visibility.o" "eggrig_finalRN.phl[30]";
connectAttr "foot_ctrl_4_translateX.o" "eggrig_finalRN.phl[31]";
connectAttr "foot_ctrl_4_translateY.o" "eggrig_finalRN.phl[32]";
connectAttr "foot_ctrl_4_translateZ.o" "eggrig_finalRN.phl[33]";
connectAttr "foot_ctrl_4_rotateX.o" "eggrig_finalRN.phl[34]";
connectAttr "foot_ctrl_4_rotateY.o" "eggrig_finalRN.phl[35]";
connectAttr "foot_ctrl_4_rotateZ.o" "eggrig_finalRN.phl[36]";
connectAttr "foot_ctrl_4_scaleX.o" "eggrig_finalRN.phl[37]";
connectAttr "foot_ctrl_4_scaleY.o" "eggrig_finalRN.phl[38]";
connectAttr "foot_ctrl_4_scaleZ.o" "eggrig_finalRN.phl[39]";
connectAttr "foot_ctrl_4_visibility.o" "eggrig_finalRN.phl[40]";
connectAttr "body_ctrl_translateX.o" "eggrig_finalRN.phl[41]";
connectAttr "body_ctrl_translateY.o" "eggrig_finalRN.phl[42]";
connectAttr "body_ctrl_translateZ.o" "eggrig_finalRN.phl[43]";
connectAttr "body_ctrl_visibility1.o" "eggrig_finalRN.phl[44]";
connectAttr "body_ctrl_scaleX.o" "eggrig_finalRN.phl[45]";
connectAttr "body_ctrl_scaleY.o" "eggrig_finalRN.phl[46]";
connectAttr "body_ctrl_scaleZ.o" "eggrig_finalRN.phl[47]";
connectAttr "rotatectrl_rotateX.o" "eggrig_finalRN.phl[48]";
connectAttr "rotatectrl_rotateY.o" "eggrig_finalRN.phl[49]";
connectAttr "rotatectrl_rotateZ.o" "eggrig_finalRN.phl[50]";
connectAttr "rotatectrl_visibility.o" "eggrig_finalRN.phl[51]";
connectAttr "rotatectrl_scaleX.o" "eggrig_finalRN.phl[52]";
connectAttr "rotatectrl_scaleY.o" "eggrig_finalRN.phl[53]";
connectAttr "rotatectrl_scaleZ.o" "eggrig_finalRN.phl[54]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "eggboitryrigtry2:aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "eggboitryrigtry2:aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "eggboitryrigtry2:aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "eggboitryrigtry2:aiStandardSurface2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "aiStandardSurface1.out" "aiStandardSurface1SG.ss";
connectAttr "aiStandardSurface1SG.msg" "materialInfo1.sg";
connectAttr "aiStandardSurface1.msg" "materialInfo1.m";
connectAttr "aiStandardSurface1.msg" "materialInfo1.t" -na;
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "aiStandardSurface2.out" "aiStandardSurface2SG.ss";
connectAttr "aiStandardSurface2SG.msg" "materialInfo2.sg";
connectAttr "aiStandardSurface2.msg" "materialInfo2.m";
connectAttr "aiStandardSurface2.msg" "materialInfo2.t" -na;
connectAttr "layerManager.dli[1]" "mesh.id";
connectAttr "layerManager.dli[2]" "ctrllayer.id";
connectAttr "eggboitryrigtry2:aiStandardSurface1.out" "eggboitryrigtry2:aiStandardSurface1SG.ss"
		;
connectAttr "eggboitryrigtry2:aiStandardSurface1SG.msg" "eggboitryrigtry2:materialInfo1.sg"
		;
connectAttr "eggboitryrigtry2:aiStandardSurface1.msg" "eggboitryrigtry2:materialInfo1.m"
		;
connectAttr "eggboitryrigtry2:aiStandardSurface1.msg" "eggboitryrigtry2:materialInfo1.t"
		 -na;
connectAttr "eggboitryrigtry2:aiStandardSurface2.out" "eggboitryrigtry2:aiStandardSurface2SG.ss"
		;
connectAttr "eggboitryrigtry2:aiStandardSurface2SG.msg" "eggboitryrigtry2:materialInfo2.sg"
		;
connectAttr "eggboitryrigtry2:aiStandardSurface2.msg" "eggboitryrigtry2:materialInfo2.m"
		;
connectAttr "eggboitryrigtry2:aiStandardSurface2.msg" "eggboitryrigtry2:materialInfo2.t"
		 -na;
connectAttr "layerManager.dli[3]" "jointsandotherthingsyoudontneedtosee.id";
connectAttr "aiStandardSurface1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface2SG.pa" ":renderPartition.st" -na;
connectAttr "eggboitryrigtry2:aiStandardSurface1SG.pa" ":renderPartition.st" -na
		;
connectAttr "eggboitryrigtry2:aiStandardSurface2SG.pa" ":renderPartition.st" -na
		;
connectAttr "aiStandardSurface1.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface2.msg" ":defaultShaderList1.s" -na;
connectAttr "eggboitryrigtry2:aiStandardSurface1.msg" ":defaultShaderList1.s" -na
		;
connectAttr "eggboitryrigtry2:aiStandardSurface2.msg" ":defaultShaderList1.s" -na
		;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of egg_walk_final.ma
