//Maya ASCII 2018 scene
//Name: Pistol1.ma
//Last modified: Thu, Nov 14, 2019 10:26:53 AM
//Codeset: 1252
requires maya "2018";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "2448456C-40D6-BD12-5BFC-519190CCA633";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.61929347060099049 -0.27735469416173031 1.5594575867326272 ;
	setAttr ".r" -type "double3" 19.46164726965813 346.20000000054381 1.2281597202572433e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "C70C7D52-49D3-D745-3EEE-E89FC7B37FC3";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1.7409810019750633;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.29107369744200828 0.25455482372874089 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "59B2C283-4B0D-D2C0-D416-3A99AB03E54F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BEE27E32-4C0C-6989-A276-4292C938DD67";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "4EFAB1E7-4A7E-44FE-A661-68AC04E2F473";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.027256409778102814 0.14712344179243697 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "0FB03794-4F8D-78AD-98F2-D9910115C492";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 1.0510801957303855;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "5605DAAB-4398-FD8B-476E-C8B6B6E871B3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "1BADDD57-4B62-2A32-C7A0-3A864BFCEFCF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "imagePlane1";
	rename -uid "83523AAC-49B4-2B21-D334-7E94D78E46E0";
	setAttr ".t" -type "double3" 0.76341312274856898 -0.46976794771285335 5 ;
	setAttr ".s" -type "double3" 0.1057111318524535 0.1057111318524535 0.1057111318524535 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "E5831E2C-4266-1607-AEDD-4694F915647E";
	setAttr -k off ".v";
	setAttr ".fc" 101;
	setAttr ".imn" -type "string" "D:/Google Drive/.Hagenberg/Semester 3/Pro3/3D/Scan 2019-11-13 11.26.06.jpg";
	setAttr ".cov" -type "short2" 3269 2217 ;
	setAttr ".ag" 0.21428571401962213;
	setAttr ".dlc" no;
	setAttr ".w" 32.69;
	setAttr ".h" 22.169999999999998;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "imagePlane2";
	rename -uid "5F685CE4-47EC-8794-BA69-02A3A4F5A558";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 2.4854806411445622 0.85093780629314786 ;
	setAttr ".s" -type "double3" 0.21928043203380848 0.21928043203380848 0.21928043203380848 ;
createNode imagePlane -n "imagePlaneShape2" -p "imagePlane2";
	rename -uid "284C3FED-4F7C-0DE4-329E-7ABC7A5ED092";
	setAttr -k off ".v";
	setAttr ".fc" 101;
	setAttr ".imn" -type "string" "D:/Google Drive/.Hagenberg/Semester 3/Pro3/3D/Scan 2019-11-13 11.24.55.jpg";
	setAttr ".cov" -type "short2" 3579 2066 ;
	setAttr ".dlc" no;
	setAttr ".w" 35.79;
	setAttr ".h" 20.66;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCube1";
	rename -uid "D9A52FE6-4AAF-6881-75A1-8893ED36BE6F";
	setAttr ".t" -type "double3" -0.29028602206130155 0.39099320838087864 0 ;
	setAttr ".s" -type "double3" 1 1 0.6818378433036606 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "BA1346B7-4ABC-63BB-16C4-E3A5019B4DC6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.71250000596046448 0.138888880610466 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 90 ".pt";
	setAttr ".pt[42]" -type "float3" 0.00029318 0 0 ;
	setAttr ".pt[43]" -type "float3" 0.0002931949 0 0 ;
	setAttr ".pt[84]" -type "float3" 0.00029318 0 0 ;
	setAttr ".pt[85]" -type "float3" 0.0002931949 0 0 ;
	setAttr ".pt[89]" -type "float3" 0 -4.6566129e-10 5.5879354e-09 ;
	setAttr ".pt[90]" -type "float3" 0 -4.6566129e-10 5.5879354e-09 ;
	setAttr ".pt[91]" -type "float3" 0 -4.6566129e-10 5.5879354e-09 ;
	setAttr ".pt[92]" -type "float3" 0 -4.6566129e-10 3.7252903e-09 ;
	setAttr ".pt[95]" -type "float3" 0 0 -0.0087677827 ;
	setAttr ".pt[96]" -type "float3" 0 0 -0.0087677827 ;
	setAttr ".pt[97]" -type "float3" 0 0 -0.0087677827 ;
	setAttr ".pt[98]" -type "float3" 0 0 -0.0087677827 ;
	setAttr ".pt[99]" -type "float3" 0 0 -0.0087677827 ;
	setAttr ".pt[113]" -type "float3" 0 -4.6566129e-10 5.5879354e-09 ;
	setAttr ".pt[114]" -type "float3" 0 -4.6566129e-10 5.5879354e-09 ;
	setAttr ".pt[115]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".pt[116]" -type "float3" 0 4.6566129e-10 0 ;
	setAttr ".pt[119]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[120]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[121]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[122]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[123]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[134]" -type "float3" 0 0 -1.8626451e-09 ;
	setAttr ".pt[135]" -type "float3" 0 -9.3132257e-10 3.7252903e-09 ;
	setAttr ".pt[137]" -type "float3" 0 -9.3132257e-10 5.5879354e-09 ;
	setAttr ".pt[138]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".pt[139]" -type "float3" 0 4.6566129e-10 0 ;
	setAttr ".pt[140]" -type "float3" 0 -4.6566129e-10 -3.7252903e-09 ;
	setAttr ".pt[141]" -type "float3" 0 0 1.8626451e-09 ;
	setAttr ".pt[182]" -type "float3" 0 0 1.2107193e-08 ;
	setAttr ".pt[183]" -type "float3" 0 0 1.3038516e-08 ;
	setAttr ".pt[184]" -type "float3" 0 0 -1.3969839e-08 ;
	setAttr ".pt[185]" -type "float3" 0 0 -1.3969839e-08 ;
	setAttr ".pt[186]" -type "float3" 0 0 1.3969839e-08 ;
	setAttr ".pt[187]" -type "float3" 0 2.3283064e-10 -1.3969839e-08 ;
	setAttr ".pt[188]" -type "float3" 0 0 1.3969839e-08 ;
	setAttr ".pt[189]" -type "float3" 0 0 -1.3969839e-08 ;
	setAttr ".pt[199]" -type "float3" 0 -4.6566129e-10 5.5879354e-09 ;
	setAttr ".pt[219]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[220]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[228]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[229]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[230]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[231]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[232]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[233]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[234]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[235]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[236]" -type "float3" 0 0.030177271 0.0087677827 ;
	setAttr ".pt[237]" -type "float3" 0 0.030177271 -0.0087677818 ;
	setAttr ".pt[238]" -type "float3" 0 0.046854146 0.0087677827 ;
	setAttr ".pt[239]" -type "float3" 0 0.046854146 -0.0087677818 ;
	setAttr ".pt[241]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[242]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[243]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[244]" -type "float3" 0 0 -0.0087677827 ;
	setAttr ".pt[254]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[255]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[256]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[257]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[258]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[259]" -type "float3" 0 0.020250537 -0.0087677818 ;
	setAttr ".pt[260]" -type "float3" 0 0.020250537 -0.0087677818 ;
	setAttr ".pt[261]" -type "float3" 0 0.020250537 0.0087677827 ;
	setAttr ".pt[262]" -type "float3" 0 0.020250537 0.0087677827 ;
	setAttr ".pt[263]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[264]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[265]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[266]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[267]" -type "float3" 0 2.3283064e-10 0.0087677827 ;
	setAttr ".pt[268]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[269]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[270]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[271]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[272]" -type "float3" 0 0 0.0087677827 ;
	setAttr ".pt[273]" -type "float3" 0 0.030177271 0.0087677827 ;
	setAttr ".pt[274]" -type "float3" 0 0.046854146 0.0087677827 ;
	setAttr ".pt[275]" -type "float3" 0 0.046854146 -0.0087677818 ;
	setAttr ".pt[276]" -type "float3" 0 0.030177271 -0.0087677818 ;
	setAttr ".pt[277]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[278]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[279]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[280]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[281]" -type "float3" 0 0 -0.0087677818 ;
	setAttr ".pt[282]" -type "float3" 0.019619551 0.012000322 0 ;
	setAttr ".pt[283]" -type "float3" 0.019626822 0.01200477 -1.8626451e-09 ;
	setAttr ".pt[284]" -type "float3" 0.019626811 0.01200477 1.8626451e-09 ;
	setAttr ".pt[285]" -type "float3" 0.019619551 0.012000322 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube2";
	rename -uid "C650DA81-4337-207D-2DE2-AB94391FDC51";
	setAttr ".t" -type "double3" -0.13206331202365507 -0.21349599934207641 0 ;
	setAttr ".r" -type "double3" 0 0 90 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "AE999A7D-472C-4F68-A192-57B26AE9C10C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.52500000037252903 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt";
	setAttr ".pt[32]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[35]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[37]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[39]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[41]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[43]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[45]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[47]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[49]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[51]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[53]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[57]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[59]" -type "float3" -0.054057375 0 0 ;
	setAttr ".pt[74]" -type "float3" -0.089750864 0.02662294 0 ;
	setAttr ".pt[75]" -type "float3" -0.089750864 0.015973771 0 ;
	setAttr ".pt[76]" -type "float3" -0.089750864 0.011222298 0 ;
	setAttr ".pt[77]" -type "float3" -0.089750864 -0.011222302 0 ;
	setAttr ".pt[78]" -type "float3" -0.089750864 -0.015973778 0 ;
	setAttr ".pt[79]" -type "float3" -0.089750864 -0.02662294 0 ;
	setAttr ".pt[80]" -type "float3" -0.089750864 -0.02662294 0 ;
	setAttr ".pt[81]" -type "float3" -0.089750864 -0.02662294 0 ;
	setAttr ".pt[82]" -type "float3" -0.089750864 -0.015973778 0 ;
	setAttr ".pt[83]" -type "float3" -0.089750864 -0.011222302 0 ;
	setAttr ".pt[84]" -type "float3" -0.089750864 0.011222298 0 ;
	setAttr ".pt[85]" -type "float3" -0.089750864 0.015973771 0 ;
	setAttr ".pt[86]" -type "float3" -0.089750864 0.02662294 0 ;
	setAttr ".pt[87]" -type "float3" -0.089750864 0.02662294 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube3";
	rename -uid "2CA4790C-4FB0-28EF-4C3B-CAAE8A5784B0";
	setAttr ".t" -type "double3" 0.11788683836011016 -0.12623840826265553 0 ;
	setAttr ".r" -type "double3" 0 0 90 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "A57B0445-4226-D680-6683-AD83CC2748B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.58643531799316406 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 102 ".uvst[0].uvsp[0:101]" -type "float2" 0.5 0 0.5625 0 0.625
		 0 0.5 0.050000001 0.5625 0.050000001 0.625 0.050000001 0.5 0.1 0.5625 0.1 0.625 0.1
		 0.5 0.15000001 0.5625 0.15000001 0.625 0.15000001 0.5 0.2 0.5625 0.2 0.625 0.2 0.5
		 0.25 0.5625 0.25 0.625 0.25 0.5 0.375 0.5625 0.375 0.625 0.375 0.5 0.5 0.5625 0.5
		 0.625 0.5 0.5 0.55000001 0.5625 0.55000001 0.625 0.55000001 0.5 0.60000002 0.5625
		 0.60000002 0.625 0.60000002 0.5 0.65000004 0.5625 0.65000004 0.625 0.65000004 0.5
		 0.70000005 0.5625 0.70000005 0.625 0.70000005 0.5 0.75000006 0.5625 0.75000006 0.625
		 0.75000006 0.5 0.87500006 0.5625 0.87500006 0.625 0.87500006 0.5 1 0.5625 1 0.625
		 1 0.875 0 0.75 0 0.875 0.050000001 0.75 0.050000001 0.875 0.1 0.75 0.1 0.875 0.15000001
		 0.75 0.15000001 0.875 0.2 0.75 0.2 0.875 0.25 0.75 0.25 0.58643532 0 0.58643532 1
		 0.58643532 0.87500006 0.58643532 0.75000012 0.58643532 0.70000005 0.58643532 0.6500001
		 0.58643532 0.60000002 0.58643532 0.55000001 0.58643532 0.5 0.58643532 0.375 0.58643532
		 0.25 0.58643532 0.20000002 0.58643532 0.15000001 0.58643532 0.10000001 0.58643532
		 0.050000004 0.5 0.050000001 0.5 0 0.5 0.1 0.5 0.15000001 0.5 0.2 0.5 0.25 0.5 0.375
		 0.5 0.5 0.5 0.55000001 0.5 0.60000002 0.5 0.65000004 0.5 0.70000005 0.5 0.75000006
		 0.5 0.87500006 0.5 1 0.58643532 0.87500006 0.58643532 1 0.58643532 0.75000012 0.58643532
		 0.70000005 0.58643532 0.6500001 0.58643532 0.60000002 0.58643532 0.55000001 0.58643532
		 0.5 0.58643532 0.375 0.58643532 0.25 0.58643532 0.20000002 0.58643532 0.15000001
		 0.58643532 0.10000001 0.58643532 0.050000004 0.58643532 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt";
	setAttr ".pt[32]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[33]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[34]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[35]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[36]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[37]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[38]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[39]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[40]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[41]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[42]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[43]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[44]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[45]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".pt[74]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[75]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[76]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[77]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[78]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[79]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[80]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[81]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[82]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[83]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[84]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[85]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[86]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".pt[87]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr -s 88 ".vt[0:87]"  -7.2347936e-19 -0.079997271 0.017923452 0.14583333 -0.079997271 0.017923452
		 -4.3408764e-19 -0.047998361 0.017923452 0.14583333 -0.047998361 0.017923452 3.7902822e-18 -0.033721022 0.017923452
		 0.12235551 -0.033720981 0.017923448 -3.7902822e-18 0.033721022 0.017923452 0.12235551 0.033720981 0.017923448
		 4.3408774e-19 0.047998369 0.017923452 0.14583333 0.047998369 0.017923452 7.2347936e-19 0.079997271 0.017923452
		 0.14583333 0.079997271 0.017923452 7.2347936e-19 0.079997271 0 0.14583333 0.079997271 0
		 7.2347936e-19 0.079997271 -0.017923452 0.14583333 0.079997271 -0.017923452 4.3408774e-19 0.047998369 -0.017923452
		 0.14583333 0.047998369 -0.017923452 -3.7902822e-18 0.033721022 -0.017923452 0.12235551 0.033720981 -0.017923448
		 3.7902822e-18 -0.033721022 -0.017923452 0.12235551 -0.033720981 -0.017923448 -4.3408764e-19 -0.047998361 -0.017923452
		 0.14583333 -0.047998361 -0.017923452 -7.2347936e-19 -0.079997271 -0.017923452 0.14583333 -0.079997271 -0.017923452
		 -7.2347936e-19 -0.079997271 0 0.14583333 -0.079997271 0 0.14583333 -0.047998361 0
		 0.12235551 -0.033720981 0 0.12235551 0.033720981 0 0.14583333 0.047998369 0 0.10084119 -0.079997271 0.017923452
		 0.10084119 -0.079997271 0 0.10084119 -0.079997271 -0.017923452 0.10084119 -0.047998361 -0.017923452
		 0.10084119 -0.033721007 -0.017923452 0.10084119 0.033721022 -0.017923452 0.10084119 0.047998369 -0.017923452
		 0.10084119 0.079997271 -0.017923452 0.10084119 0.079997271 0 0.10084119 0.079997271 0.017923452
		 0.10084119 0.047998369 0.017923452 0.10084119 0.033721022 0.017923452 0.10084119 -0.033721007 0.017923452
		 0.10084119 -0.047998361 0.017923452 -4.6566129e-10 -0.087821685 0.024528611 0.072916664 -0.087821685 0.024528611
		 0.072916664 -0.052693009 0.024528611 -4.6566129e-10 -0.052693009 0.024528611 0.072916664 -0.037019182 0.024528611
		 -4.6566129e-10 -0.037019182 0.024528611 0.072916664 0.037019182 0.024528611 -4.6566129e-10 0.037019182 0.024528611
		 0.072916664 0.052693017 0.024528611 -4.6566129e-10 0.052693017 0.024528611 0.072916664 0.087821685 0.024528611
		 -4.6566129e-10 0.087821685 0.024528611 0.072916664 0.087821685 0 -4.6566129e-10 0.087821685 0
		 0.072916664 0.087821685 -0.024528611 -4.6566129e-10 0.087821685 -0.024528611 0.072916664 0.052693017 -0.024528611
		 -4.6566129e-10 0.052693017 -0.024528611 0.072916664 0.037019182 -0.024528611 -4.6566129e-10 0.037019182 -0.024528611
		 0.072916664 -0.037019182 -0.024528611 -4.6566129e-10 -0.037019182 -0.024528611 0.072916664 -0.052693009 -0.024528611
		 -4.6566129e-10 -0.052693009 -0.024528611 0.072916664 -0.087821685 -0.024528611 -4.6566129e-10 -0.087821685 -0.024528611
		 0.072916664 -0.087821685 0 -4.6566129e-10 -0.087821685 0 0.10084119 -0.087821685 0
		 0.10084119 -0.087821685 0.024528611 0.10084119 -0.087821685 -0.024528611 0.10084119 -0.052693009 -0.024528611
		 0.10084119 -0.037019182 -0.024528611 0.10084119 0.037019182 -0.024528611 0.10084119 0.052693017 -0.024528611
		 0.10084119 0.087821685 -0.024528611 0.10084119 0.087821685 0 0.10084119 0.087821685 0.024528611
		 0.10084119 0.052693017 0.024528611 0.10084119 0.037019182 0.024528611 0.10084119 -0.037019182 0.024528611
		 0.10084119 -0.052693009 0.024528611;
	setAttr -s 167 ".ed";
	setAttr ".ed[0:165]"  0 2 0 1 3 0 2 4 0 3 5 0 4 6 0 5 7 0 6 8 0 7 9 0 8 10 0
		 9 11 0 10 12 0 11 13 0 12 14 0 13 15 0 14 16 0 15 17 0 16 18 0 17 19 0 18 20 0 19 21 0
		 20 22 0 21 23 0 22 24 0 23 25 0 24 26 0 25 27 0 26 0 0 27 1 0 23 28 0 28 3 0 21 29 0
		 29 5 0 19 30 0 30 7 0 17 31 0 31 9 0 27 28 0 28 29 0 29 30 0 30 31 0 31 13 0 32 1 0
		 33 27 0 34 25 0 35 23 0 36 21 0 37 19 0 38 17 0 39 15 0 40 13 0 41 11 0 42 9 0 43 7 0
		 44 5 0 45 3 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0
		 41 42 0 42 43 0 43 44 0 44 45 0 45 32 0 0 46 0 46 47 0 47 48 0 2 49 0 49 48 0 46 49 0
		 48 50 0 4 51 0 51 50 0 49 51 0 50 52 0 6 53 0 53 52 0 51 53 0 52 54 0 8 55 0 55 54 0
		 53 55 0 54 56 0 10 57 0 57 56 0 55 57 0 56 58 0 12 59 0 59 58 0 57 59 0 58 60 0 14 61 0
		 61 60 0 59 61 0 60 62 0 16 63 0 63 62 0 61 63 0 62 64 0 18 65 0 65 64 0 63 65 0 64 66 0
		 20 67 0 67 66 0 65 67 0 66 68 0 22 69 0 69 68 0 67 69 0 68 70 0 24 71 0 71 70 0 69 71 0
		 70 72 0 26 73 0 73 72 0 71 73 0 72 47 0 73 46 0 33 74 0 72 74 0 32 75 0 75 74 0 47 75 0
		 34 76 0 70 76 0 74 76 0 35 77 0 68 77 0 76 77 0 36 78 0 66 78 0 77 78 0 37 79 0 64 79 0
		 78 79 0 38 80 0 62 80 0 79 80 0 39 81 0 60 81 0 80 81 0 40 82 0 58 82 0 81 82 0 41 83 0
		 56 83 0 82 83 0 42 84 0 54 84 0 83 84 0 43 85 0 52 85 0 84 85 0 44 86 0 50 86 0 85 86 0
		 45 87 0 48 87 0 86 87 0;
	setAttr ".ed[166]" 87 75 0;
	setAttr -s 80 -ch 320 ".fc[0:79]" -type "polyFaces" 
		f 4 70 71 -74 -75
		mu 0 4 73 1 4 72
		f 4 68 41 1 -55
		mu 0 4 71 57 2 5
		f 4 73 75 -78 -79
		mu 0 4 72 4 7 74
		f 4 67 54 3 -54
		mu 0 4 70 71 5 8
		f 4 77 79 -82 -83
		mu 0 4 74 7 10 75
		f 4 66 53 5 -53
		mu 0 4 69 70 8 11
		f 4 81 83 -86 -87
		mu 0 4 75 10 13 76
		f 4 65 52 7 -52
		mu 0 4 68 69 11 14
		f 4 85 87 -90 -91
		mu 0 4 76 13 16 77
		f 4 64 51 9 -51
		mu 0 4 67 68 14 17
		f 4 89 91 -94 -95
		mu 0 4 77 16 19 78
		f 4 63 50 11 -50
		mu 0 4 66 67 17 20
		f 4 93 95 -98 -99
		mu 0 4 78 19 22 79
		f 4 62 49 13 -49
		mu 0 4 65 66 20 23
		f 4 97 99 -102 -103
		mu 0 4 79 22 25 80
		f 4 61 48 15 -48
		mu 0 4 64 65 23 26
		f 4 101 103 -106 -107
		mu 0 4 80 25 28 81
		f 4 60 47 17 -47
		mu 0 4 63 64 26 29
		f 4 105 107 -110 -111
		mu 0 4 81 28 31 82
		f 4 59 46 19 -46
		mu 0 4 62 63 29 32
		f 4 109 111 -114 -115
		mu 0 4 82 31 34 83
		f 4 58 45 21 -45
		mu 0 4 61 62 32 35
		f 4 113 115 -118 -119
		mu 0 4 83 34 37 84
		f 4 57 44 23 -44
		mu 0 4 60 61 35 38
		f 4 117 119 -122 -123
		mu 0 4 84 37 40 85
		f 4 56 43 25 -43
		mu 0 4 59 60 38 41
		f 4 121 123 -71 -125
		mu 0 4 85 40 43 86
		f 4 55 42 27 -42
		mu 0 4 58 59 41 44
		f 4 -26 -24 28 -37
		mu 0 4 46 45 47 48
		f 4 -28 36 29 -2
		mu 0 4 2 46 48 5
		f 4 -29 -22 30 -38
		mu 0 4 48 47 49 50
		f 4 -30 37 31 -4
		mu 0 4 5 48 50 8
		f 4 -31 -20 32 -39
		mu 0 4 50 49 51 52
		f 4 -32 38 33 -6
		mu 0 4 8 50 52 11
		f 4 -33 -18 34 -40
		mu 0 4 52 51 53 54
		f 4 -34 39 35 -8
		mu 0 4 11 52 54 14
		f 4 -35 -16 -14 -41
		mu 0 4 54 53 55 56
		f 4 -36 40 -12 -10
		mu 0 4 14 54 56 17
		f 4 126 -129 -130 -124
		mu 0 4 40 87 88 43
		f 4 131 -133 -127 -120
		mu 0 4 37 89 87 40
		f 4 134 -136 -132 -116
		mu 0 4 34 90 89 37
		f 4 137 -139 -135 -112
		mu 0 4 31 91 90 34
		f 4 140 -142 -138 -108
		mu 0 4 28 92 91 31
		f 4 143 -145 -141 -104
		mu 0 4 25 93 92 28
		f 4 146 -148 -144 -100
		mu 0 4 22 94 93 25
		f 4 149 -151 -147 -96
		mu 0 4 19 95 94 22
		f 4 152 -154 -150 -92
		mu 0 4 16 96 95 19
		f 4 155 -157 -153 -88
		mu 0 4 13 97 96 16
		f 4 158 -160 -156 -84
		mu 0 4 10 98 97 13
		f 4 161 -163 -159 -80
		mu 0 4 7 99 98 10
		f 4 164 -166 -162 -76
		mu 0 4 4 100 99 7
		f 4 129 -167 -165 -72
		mu 0 4 1 101 100 4
		f 4 -1 69 74 -73
		mu 0 4 3 0 73 72
		f 4 -3 72 78 -77
		mu 0 4 6 3 72 74
		f 4 -5 76 82 -81
		mu 0 4 9 6 74 75
		f 4 -7 80 86 -85
		mu 0 4 12 9 75 76
		f 4 -9 84 90 -89
		mu 0 4 15 12 76 77
		f 4 -11 88 94 -93
		mu 0 4 18 15 77 78
		f 4 -13 92 98 -97
		mu 0 4 21 18 78 79
		f 4 -15 96 102 -101
		mu 0 4 24 21 79 80
		f 4 -17 100 106 -105
		mu 0 4 27 24 80 81
		f 4 -19 104 110 -109
		mu 0 4 30 27 81 82
		f 4 -21 108 114 -113
		mu 0 4 33 30 82 83
		f 4 -23 112 118 -117
		mu 0 4 36 33 83 84
		f 4 -25 116 122 -121
		mu 0 4 39 36 84 85
		f 4 -27 120 124 -70
		mu 0 4 42 39 85 86
		f 4 -56 127 128 -126
		mu 0 4 59 58 88 87
		f 4 -57 125 132 -131
		mu 0 4 60 59 87 89
		f 4 -58 130 135 -134
		mu 0 4 61 60 89 90
		f 4 -59 133 138 -137
		mu 0 4 62 61 90 91
		f 4 -60 136 141 -140
		mu 0 4 63 62 91 92
		f 4 -61 139 144 -143
		mu 0 4 64 63 92 93
		f 4 -62 142 147 -146
		mu 0 4 65 64 93 94
		f 4 -63 145 150 -149
		mu 0 4 66 65 94 95
		f 4 -64 148 153 -152
		mu 0 4 67 66 95 96
		f 4 -65 151 156 -155
		mu 0 4 68 67 96 97
		f 4 -66 154 159 -158
		mu 0 4 69 68 97 98
		f 4 -67 157 162 -161
		mu 0 4 70 69 98 99
		f 4 -68 160 165 -164
		mu 0 4 71 70 99 100
		f 4 -69 163 166 -128
		mu 0 4 57 71 100 101;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube4";
	rename -uid "73B06980-4AE1-5FB5-77A9-5C932CCEF7D4";
	setAttr ".t" -type "double3" 0.37317503179682476 -0.048688302735697853 0 ;
	setAttr ".r" -type "double3" 0 0 90 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "822F140D-4B03-4783-BCAB-778D873B9B5A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 87 ".uvst[0].uvsp[0:86]" -type "float2" 0.5625 0 0.625 0
		 0.5625 0.050000001 0.625 0.050000001 0.5625 0.1 0.625 0.1 0.5625 0.15000001 0.625
		 0.15000001 0.5625 0.2 0.625 0.2 0.5625 0.25 0.625 0.25 0.5625 0.375 0.625 0.375 0.5625
		 0.5 0.625 0.5 0.5625 0.55000001 0.625 0.55000001 0.5625 0.60000002 0.625 0.60000002
		 0.5625 0.65000004 0.625 0.65000004 0.5625 0.70000005 0.625 0.70000005 0.5625 0.75000006
		 0.625 0.75000006 0.5625 0.87500006 0.625 0.87500006 0.5625 1 0.625 1 0.875 0 0.75
		 0 0.875 0.050000001 0.75 0.050000001 0.875 0.1 0.75 0.1 0.875 0.15000001 0.75 0.15000001
		 0.875 0.2 0.75 0.2 0.875 0.25 0.75 0.25 0.58643532 0 0.58643532 1 0.58643532 0.87500006
		 0.58643532 0.75000012 0.58643532 0.70000005 0.58643532 0.6500001 0.58643532 0.60000002
		 0.58643532 0.55000001 0.58643532 0.5 0.58643532 0.375 0.58643532 0.25 0.58643532
		 0.20000002 0.58643532 0.15000001 0.58643532 0.10000001 0.58643532 0.050000004 0.5
		 0.050000001 0.5 0 0.5 0.1 0.5 0.15000001 0.5 0.2 0.5 0.25 0.5 0.375 0.5 0.5 0.5 0.55000001
		 0.5 0.60000002 0.5 0.65000004 0.5 0.70000005 0.5 0.75000006 0.5 0.87500006 0.5 1
		 0.58643532 0.87500006 0.58643532 1 0.58643532 0.75000012 0.58643532 0.70000005 0.58643532
		 0.6500001 0.58643532 0.60000002 0.58643532 0.55000001 0.58643532 0.5 0.58643532 0.375
		 0.58643532 0.25 0.58643532 0.20000002 0.58643532 0.15000001 0.58643532 0.10000001
		 0.58643532 0.050000004 0.58643532 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[32]" -type "float3" -0.065751225 -1.3877788e-17 0 ;
	setAttr ".pt[35]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[37]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[39]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[41]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[43]" -type "float3" -0.065751225 -1.3877788e-17 0 ;
	setAttr ".pt[45]" -type "float3" -0.065751225 -1.3877788e-17 0 ;
	setAttr ".pt[47]" -type "float3" -0.065751225 -1.3877788e-17 0 ;
	setAttr ".pt[49]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[51]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[53]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[55]" -type "float3" -0.065751225 -6.9388939e-18 0 ;
	setAttr ".pt[57]" -type "float3" -0.065751225 -1.3877788e-17 0 ;
	setAttr ".pt[59]" -type "float3" -0.065751225 -1.3877788e-17 0 ;
	setAttr -s 74 ".vt[0:73]"  0.14583333 -0.079997271 0.017923452 0.14583333 -0.047998361 0.017923452
		 0.12235551 -0.033720981 0.017923448 0.12235551 0.033720981 0.017923448 0.14583333 0.047998369 0.017923452
		 0.14583333 0.079997271 0.017923452 0.14583333 0.079997271 0 0.14583333 0.079997271 -0.017923452
		 0.14583333 0.047998369 -0.017923452 0.12235551 0.033720981 -0.017923448 0.12235551 -0.033720981 -0.017923448
		 0.14583333 -0.047998361 -0.017923452 0.14583333 -0.079997271 -0.017923452 0.14583333 -0.079997271 0
		 0.14583333 -0.047998361 0 0.12235551 -0.033720981 0 0.12235551 0.033720981 0 0.14583333 0.047998369 0
		 0.10665482 -0.079997271 0.017923452 0.10665482 -0.079997271 0 0.10665482 -0.079997271 -0.017923452
		 0.10665482 -0.047998361 -0.017923452 0.10665482 -0.033721007 -0.017923452 0.10665482 0.033721022 -0.017923452
		 0.10665482 0.047998369 -0.017923452 0.10665482 0.079997271 -0.017923452 0.10665482 0.079997271 0
		 0.10665482 0.079997271 0.017923452 0.10665482 0.047998369 0.017923452 0.10665482 0.033721022 0.017923452
		 0.10665482 -0.033721007 0.017923452 0.10665482 -0.047998361 0.017923452 -4.6566129e-10 -0.087821685 0.024528611
		 0.072916664 -0.087821685 0.024528611 0.072916664 -0.052693009 0.024528611 -4.6566129e-10 -0.052693009 0.024528611
		 0.072916664 -0.037019182 0.024528611 -4.6566129e-10 -0.037019182 0.024528611 0.072916664 0.037019182 0.024528611
		 -4.6566129e-10 0.037019182 0.024528611 0.072916664 0.052693017 0.024528611 -4.6566129e-10 0.052693017 0.024528611
		 0.072916664 0.087821685 0.024528611 -4.6566129e-10 0.087821685 0.024528611 0.072916664 0.087821685 0
		 -4.6566129e-10 0.087821685 0 0.072916664 0.087821685 -0.024528611 -4.6566129e-10 0.087821685 -0.024528611
		 0.072916664 0.052693017 -0.024528611 -4.6566129e-10 0.052693017 -0.024528611 0.072916664 0.037019182 -0.024528611
		 -4.6566129e-10 0.037019182 -0.024528611 0.072916664 -0.037019182 -0.024528611 -4.6566129e-10 -0.037019182 -0.024528611
		 0.072916664 -0.052693009 -0.024528611 -4.6566129e-10 -0.052693009 -0.024528611 0.072916664 -0.087821685 -0.024528611
		 -4.6566129e-10 -0.087821685 -0.024528611 0.072916664 -0.087821685 0 -4.6566129e-10 -0.087821685 0
		 0.10084119 -0.087821685 0 0.10084119 -0.087821685 0.024528611 0.10084119 -0.087821685 -0.024528611
		 0.10084119 -0.052693009 -0.024528611 0.10084119 -0.037019182 -0.024528611 0.10084119 0.037019182 -0.024528611
		 0.10084119 0.052693017 -0.024528611 0.10084119 0.087821685 -0.024528611 0.10084119 0.087821685 0
		 0.10084119 0.087821685 0.024528611 0.10084119 0.052693017 0.024528611 0.10084119 0.037019182 0.024528611
		 0.10084119 -0.037019182 0.024528611 0.10084119 -0.052693009 0.024528611;
	setAttr -s 139 ".ed[0:138]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 0 0 11 14 0 14 1 0 10 15 0 15 2 0 9 16 0
		 16 3 0 8 17 0 17 4 0 13 14 0 14 15 0 15 16 0 16 17 0 17 6 0 18 0 0 19 13 0 20 12 0
		 21 11 0 22 10 0 23 9 0 24 8 0 25 7 0 26 6 0 27 5 0 28 4 0 29 3 0 30 2 0 31 1 0 18 19 0
		 19 20 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0
		 30 31 0 31 18 0 32 33 0 33 34 0 35 34 0 32 35 0 34 36 0 37 36 0 35 37 0 36 38 0 39 38 0
		 37 39 0 38 40 0 41 40 0 39 41 0 40 42 0 43 42 0 41 43 0 42 44 0 45 44 0 43 45 0 44 46 0
		 47 46 0 45 47 0 46 48 0 49 48 0 47 49 0 48 50 0 51 50 0 49 51 0 50 52 0 53 52 0 51 53 0
		 52 54 0 55 54 0 53 55 0 54 56 0 57 56 0 55 57 0 56 58 0 59 58 0 57 59 0 58 33 0 59 32 0
		 19 60 0 58 60 0 18 61 0 61 60 0 33 61 0 20 62 0 56 62 0 60 62 0 21 63 0 54 63 0 62 63 0
		 22 64 0 52 64 0 63 64 0 23 65 0 50 65 0 64 65 0 24 66 0 48 66 0 65 66 0 25 67 0 46 67 0
		 66 67 0 26 68 0 44 68 0 67 68 0 27 69 0 42 69 0 68 69 0 28 70 0 40 70 0 69 70 0 29 71 0
		 38 71 0 70 71 0 30 72 0 36 72 0 71 72 0 31 73 0 34 73 0 72 73 0 73 61 0;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 55 56 -58 -59
		mu 0 4 58 0 2 57
		f 4 54 27 0 -41
		mu 0 4 56 42 1 3
		f 4 57 59 -61 -62
		mu 0 4 57 2 4 59
		f 4 53 40 1 -40
		mu 0 4 55 56 3 5
		f 4 60 62 -64 -65
		mu 0 4 59 4 6 60
		f 4 52 39 2 -39
		mu 0 4 54 55 5 7
		f 4 63 65 -67 -68
		mu 0 4 60 6 8 61
		f 4 51 38 3 -38
		mu 0 4 53 54 7 9
		f 4 66 68 -70 -71
		mu 0 4 61 8 10 62
		f 4 50 37 4 -37
		mu 0 4 52 53 9 11
		f 4 69 71 -73 -74
		mu 0 4 62 10 12 63
		f 4 49 36 5 -36
		mu 0 4 51 52 11 13
		f 4 72 74 -76 -77
		mu 0 4 63 12 14 64
		f 4 48 35 6 -35
		mu 0 4 50 51 13 15
		f 4 75 77 -79 -80
		mu 0 4 64 14 16 65
		f 4 47 34 7 -34
		mu 0 4 49 50 15 17
		f 4 78 80 -82 -83
		mu 0 4 65 16 18 66
		f 4 46 33 8 -33
		mu 0 4 48 49 17 19
		f 4 81 83 -85 -86
		mu 0 4 66 18 20 67
		f 4 45 32 9 -32
		mu 0 4 47 48 19 21
		f 4 84 86 -88 -89
		mu 0 4 67 20 22 68
		f 4 44 31 10 -31
		mu 0 4 46 47 21 23
		f 4 87 89 -91 -92
		mu 0 4 68 22 24 69
		f 4 43 30 11 -30
		mu 0 4 45 46 23 25
		f 4 90 92 -94 -95
		mu 0 4 69 24 26 70
		f 4 42 29 12 -29
		mu 0 4 44 45 25 27
		f 4 93 95 -56 -97
		mu 0 4 70 26 28 71
		f 4 41 28 13 -28
		mu 0 4 43 44 27 29
		f 4 -13 -12 14 -23
		mu 0 4 31 30 32 33
		f 4 -14 22 15 -1
		mu 0 4 1 31 33 3
		f 4 -15 -11 16 -24
		mu 0 4 33 32 34 35
		f 4 -16 23 17 -2
		mu 0 4 3 33 35 5
		f 4 -17 -10 18 -25
		mu 0 4 35 34 36 37
		f 4 -18 24 19 -3
		mu 0 4 5 35 37 7
		f 4 -19 -9 20 -26
		mu 0 4 37 36 38 39
		f 4 -20 25 21 -4
		mu 0 4 7 37 39 9
		f 4 -21 -8 -7 -27
		mu 0 4 39 38 40 41
		f 4 -22 26 -6 -5
		mu 0 4 9 39 41 11
		f 4 98 -101 -102 -96
		mu 0 4 26 72 73 28
		f 4 103 -105 -99 -93
		mu 0 4 24 74 72 26
		f 4 106 -108 -104 -90
		mu 0 4 22 75 74 24
		f 4 109 -111 -107 -87
		mu 0 4 20 76 75 22
		f 4 112 -114 -110 -84
		mu 0 4 18 77 76 20
		f 4 115 -117 -113 -81
		mu 0 4 16 78 77 18
		f 4 118 -120 -116 -78
		mu 0 4 14 79 78 16
		f 4 121 -123 -119 -75
		mu 0 4 12 80 79 14
		f 4 124 -126 -122 -72
		mu 0 4 10 81 80 12
		f 4 127 -129 -125 -69
		mu 0 4 8 82 81 10
		f 4 130 -132 -128 -66
		mu 0 4 6 83 82 8
		f 4 133 -135 -131 -63
		mu 0 4 4 84 83 6
		f 4 136 -138 -134 -60
		mu 0 4 2 85 84 4
		f 4 101 -139 -137 -57
		mu 0 4 0 86 85 2
		f 4 -42 99 100 -98
		mu 0 4 44 43 73 72
		f 4 -43 97 104 -103
		mu 0 4 45 44 72 74
		f 4 -44 102 107 -106
		mu 0 4 46 45 74 75
		f 4 -45 105 110 -109
		mu 0 4 47 46 75 76
		f 4 -46 108 113 -112
		mu 0 4 48 47 76 77
		f 4 -47 111 116 -115
		mu 0 4 49 48 77 78
		f 4 -48 114 119 -118
		mu 0 4 50 49 78 79
		f 4 -49 117 122 -121
		mu 0 4 51 50 79 80
		f 4 -50 120 125 -124
		mu 0 4 52 51 80 81
		f 4 -51 123 128 -127
		mu 0 4 53 52 81 82
		f 4 -52 126 131 -130
		mu 0 4 54 53 82 83
		f 4 -53 129 134 -133
		mu 0 4 55 54 83 84
		f 4 -54 132 137 -136
		mu 0 4 56 55 84 85
		f 4 -55 135 138 -100
		mu 0 4 42 56 85 86;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "96F1EE6A-4EAF-E12C-2A92-48874CCF663B";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "4274C5BD-4C7A-DB40-7AC6-ECA426EFF60A";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "0089ED67-4E3D-B93F-63ED-CF917CB53C11";
createNode displayLayerManager -n "layerManager";
	rename -uid "8624E57F-4701-EB74-BCC9-DE833A2BAFF5";
createNode displayLayer -n "defaultLayer";
	rename -uid "16DD6424-4BBD-ABB9-7994-BA9F455844B0";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "D117F4CB-477A-ED67-7623-668AEB85213E";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1B537129-4E74-8CB3-6DCC-6187B2C7578D";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "2D8756F7-4EDA-FF6B-31D9-3EA174D210A4";
	setAttr ".w" 0.6;
	setAttr ".h" 0.175;
	setAttr ".d" 0.15;
	setAttr ".sw" 5;
	setAttr ".sh" 3;
	setAttr ".cuv" 4;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "E4F5B908-4606-6195-067F-3BB23F62320C";
	setAttr ".dc" -type "componentList" 2 "vtx[18]" "vtx[24]";
createNode polyTweak -n "polyTweak1";
	rename -uid "B3844563-4058-9957-C905-F58E40788485";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[18]" -type "float3" 0.070088491 0.0036414117 0 ;
	setAttr ".tk[24]" -type "float3" 0.070088491 0.0036414117 0 ;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "E810FFF7-42B0-95A6-6D1E-05AA92AC0F3F";
	setAttr ".dc" -type "componentList" 2 "vtx[18]" "vtx[24]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "4AF253FC-4604-A1D3-3C9D-21A6AAE2AB6A";
	setAttr ".dc" -type "componentList" 1 "e[58]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "75E7506C-4CBB-E7BE-F8AF-D3986C3CEF92";
	setAttr ".dc" -type "componentList" 8 "vtx[0:4]" "vtx[6:10]" "vtx[12:15]" "vtx[18:21]" "vtx[24:26]" "vtx[30:33]" "vtx[36:39]" "vtx[42:46]";
createNode polyTweak -n "polyTweak2";
	rename -uid "133223AF-44F7-306F-5985-7FB8583B56E3";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk";
	setAttr ".tk[0]" -type "float3" -0.024821827 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.049451623 0 0 ;
	setAttr ".tk[2]" -type "float3" -0.030007055 0 0 ;
	setAttr ".tk[6]" -type "float3" 0.0051852195 -0.022290954 0 ;
	setAttr ".tk[7]" -type "float3" -0.019444555 -0.022290954 0 ;
	setAttr ".tk[8]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[9]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[10]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[11]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[12]" -type "float3" 0.0051852195 0 0 ;
	setAttr ".tk[13]" -type "float3" -0.019444555 0 0 ;
	setAttr ".tk[18]" -type "float3" -0.019444555 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.019444555 0 0 ;
	setAttr ".tk[28]" -type "float3" 0.0051852195 0 0 ;
	setAttr ".tk[29]" -type "float3" -0.019444555 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.0051852195 -0.022290954 0 ;
	setAttr ".tk[35]" -type "float3" -0.019444555 -0.022290954 0 ;
	setAttr ".tk[36]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[37]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[38]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[39]" -type "float3" 0 -0.022290954 0 ;
	setAttr ".tk[40]" -type "float3" -0.024821827 0 0 ;
	setAttr ".tk[41]" -type "float3" -0.049451623 0 0 ;
	setAttr ".tk[42]" -type "float3" -0.030007055 0 0 ;
createNode deleteComponent -n "deleteComponent5";
	rename -uid "A0DF4D20-4D29-8F38-E100-A3B0A91B117F";
	setAttr ".dc" -type "componentList" 4 "f[0:1]" "f[29:30]" "f[34:35]" "f[42]";
createNode polySplitRing -n "polySplitRing1";
	rename -uid "8C8CF7DE-4D8E-8B94-203D-AE93BB9CCA56";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[3]" "e[8]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.55432283878326416;
	setAttr ".re" 3;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "BFBF56D7-4E4E-5754-6634-CEAE850B2396";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[2]" "e[7]" "e[12]" "e[16]" "e[20]" "e[25]" "e[30]" "e[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.70585989952087402;
	setAttr ".dr" no;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "484C53A1-4316-ED51-2D1F-F2A66564C2D5";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[2]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[8]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[14]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[30]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[36]" -type "float3" -0.082932055 0 0 ;
	setAttr ".tk[40]" -type "float3" -0.082932055 0 0 ;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "2E769F21-4754-39F5-4D4C-6D9653E5E706";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[2]" "e[7]" "e[12]" "e[16]" "e[20]" "e[25]" "e[30]" "e[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.23264016211032867;
	setAttr ".re" 2;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "2974D027-44A4-B7FC-E43D-3CBC1FEFD5A1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[98:99]" "e[101]" "e[103]" "e[105]" "e[107]" "e[109]" "e[111]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.72455090284347534;
	setAttr ".dr" no;
	setAttr ".re" 98;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "8A0FE389-47D8-D024-BC73-01A3FE1CD069";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[0]" "e[5]" "e[10]" "e[14]" "e[18]" "e[23]" "e[28]" "e[31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.20288187265396118;
	setAttr ".re" 0;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "08ED2881-41DB-BA5E-A76A-B3A5D7DEE566";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[52]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[53]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[60]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[61]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[66]" -type "float3" 0 0.046073347 0 ;
	setAttr ".tk[67]" -type "float3" 0 0.046073347 0 ;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "2C73986E-4801-0CBB-B005-11A9FF5566ED";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[130:131]" "e[133]" "e[135]" "e[137]" "e[139]" "e[141]" "e[143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.73677116632461548;
	setAttr ".dr" no;
	setAttr ".re" 130;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "19EE1F1D-443B-0B6E-BF20-959D312A1459";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[21]" "e[26]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.55775928497314453;
	setAttr ".dr" no;
	setAttr ".re" 21;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "72F586E3-472E-EB80-4842-3FA8342C5ADA";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[53]" -type "float3" 0 1.8626451e-09 0 ;
	setAttr ".tk[58]" -type "float3" 0 1.8626451e-09 0 ;
	setAttr ".tk[61]" -type "float3" 0 1.8626451e-09 0 ;
	setAttr ".tk[66]" -type "float3" 0 1.8626451e-09 0 ;
	setAttr ".tk[68]" -type "float3" -0.013562952 0 0 ;
	setAttr ".tk[69]" -type "float3" -0.0083398083 0 0 ;
	setAttr ".tk[70]" -type "float3" -0.0083398083 0 0 ;
	setAttr ".tk[71]" -type "float3" -0.0083398083 0 0 ;
	setAttr ".tk[72]" -type "float3" -0.0083398083 0 0 ;
	setAttr ".tk[73]" -type "float3" -0.0083398083 0 0 ;
	setAttr ".tk[74]" -type "float3" -0.0083398083 0 0 ;
	setAttr ".tk[75]" -type "float3" -0.013562952 0 0 ;
	setAttr ".tk[76]" -type "float3" 0.022632696 0 0 ;
	setAttr ".tk[77]" -type "float3" -0.012805506 0 0 ;
	setAttr ".tk[78]" -type "float3" -0.012805506 0 0 ;
	setAttr ".tk[79]" -type "float3" -0.012805506 0 0 ;
	setAttr ".tk[80]" -type "float3" -0.012805506 0 0 ;
	setAttr ".tk[81]" -type "float3" -0.012805506 0 0 ;
	setAttr ".tk[82]" -type "float3" -0.012805506 0 0 ;
	setAttr ".tk[83]" -type "float3" 0.022632696 0 0 ;
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "8D6C5836-40CE-33F3-20E5-4092157CC210";
	setAttr ".ics" -type "componentList" 2 "e[3]" "e[26]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 42;
	setAttr ".sv2" 32;
	setAttr ".d" 1;
createNode polyTweak -n "polyTweak6";
	rename -uid "27B3F45C-4D34-E5C9-B7E7-8B90C67681B3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[42]" -type "float3" -0.001812479 0 0 ;
	setAttr ".tk[43]" -type "float3" -0.001812479 0 0 ;
	setAttr ".tk[84]" -type "float3" -0.0021402226 0 0 ;
	setAttr ".tk[85]" -type "float3" -0.0021402226 0 0 ;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "0065F212-4E61-A697-9011-729232B1A0D7";
	setAttr ".ics" -type "componentList" 2 "e[79]" "e[163]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 5;
	setAttr ".sv2" 85;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge3";
	rename -uid "EDDE89C6-4477-E7D0-BFEA-05866E508D81";
	setAttr ".ics" -type "componentList" 2 "e[34]" "e[67]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 0;
	setAttr ".sv2" 34;
	setAttr ".d" 1;
createNode polyTweak -n "polyTweak7";
	rename -uid "A907FDC9-4D70-481F-8E98-3B925EC6C73A";
	setAttr ".uopa" yes;
	setAttr -s 77 ".tk";
	setAttr ".tk[4]" -type "float3" 0 0 -0.030610854 ;
	setAttr ".tk[10]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[16]" -type "float3" 0 0 -0.030610858 ;
	setAttr ".tk[17]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[18]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[19]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[20]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.030610858 ;
	setAttr ".tk[22]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[23]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[24]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[25]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[26]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[32]" -type "float3" 0 0 0.030610854 ;
	setAttr ".tk[42]" -type "float3" 2.220446e-16 0 0 ;
	setAttr ".tk[43]" -type "float3" 2.220446e-16 0 0 ;
	setAttr ".tk[45]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[46]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[55]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[56]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[63]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[64]" -type "float3" 0 0 0.030610858 ;
	setAttr ".tk[71]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[72]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[79]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[80]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[84]" -type "float3" 2.220446e-16 0 0 ;
	setAttr ".tk[85]" -type "float3" 2.220446e-16 0 0 ;
	setAttr ".tk[86]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[87]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[88]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[89]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[90]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[91]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[92]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[93]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[94]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[95]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[96]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[97]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[98]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[99]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[100]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[101]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[102]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[103]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[104]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[105]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[106]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[107]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[108]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[109]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[110]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[111]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[112]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[113]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[114]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[115]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[116]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[117]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[118]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[119]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[120]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[121]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[122]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[123]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[124]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[125]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[126]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[127]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[128]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[129]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[130]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[131]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[132]" -type "float3" 0 0 0.03061085 ;
	setAttr ".tk[133]" -type "float3" 0 0 -0.03061085 ;
	setAttr ".tk[134]" -type "float3" 0 0 0.03061085 ;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	rename -uid "6E612482-433F-4DAA-4F71-FAA1C9E51C88";
	setAttr ".ics" -type "componentList" 2 "e[4]" "e[27]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 6;
	setAttr ".sv2" 33;
	setAttr ".d" 1;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "108925DE-44CA-80A6-1464-298255B3E272";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 13 "e[50:54]" "e[71:78]" "e[86]" "e[94]" "e[106]" "e[113]" "e[122]" "e[129]" "e[138]" "e[145]" "e[154]" "e[161]" "e[165:167]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".wt" 0.70327049493789673;
	setAttr ".dr" no;
	setAttr ".re" 50;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "F9FFDB99-47D2-373E-524A-EDB329045E68";
	setAttr ".uopa" yes;
	setAttr -s 56 ".tk";
	setAttr ".tk[4]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[10]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".tk[26]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[32]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[86]" -type "float3" 0 0 -0.009233553 ;
	setAttr ".tk[87]" -type "float3" 0 0 -0.0092335576 ;
	setAttr ".tk[88]" -type "float3" 0 0 -0.0092335576 ;
	setAttr ".tk[89]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[90]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[91]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[92]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[93]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[94]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[95]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[96]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[97]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[98]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[99]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[100]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[101]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[102]" -type "float3" 0 0 -0.015601048 ;
	setAttr ".tk[103]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[104]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[105]" -type "float3" 0 0 -0.009233552 ;
	setAttr ".tk[106]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[107]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[108]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[109]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[110]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[111]" -type "float3" 0 0 -0.0092335539 ;
	setAttr ".tk[112]" -type "float3" 0 0 0.009233553 ;
	setAttr ".tk[113]" -type "float3" 0 0 0.0092335558 ;
	setAttr ".tk[114]" -type "float3" 0 0 0.0092335558 ;
	setAttr ".tk[115]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[116]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[117]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[118]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[119]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[120]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[121]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[122]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[123]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[124]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[125]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[126]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[127]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[128]" -type "float3" 0 0 0.015601048 ;
	setAttr ".tk[129]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[130]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[131]" -type "float3" 0 0 0.0092335548 ;
	setAttr ".tk[132]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[133]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[134]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[135]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[136]" -type "float3" 0 0 0.0092335539 ;
	setAttr ".tk[137]" -type "float3" 0 0 0.0092335539 ;
createNode deleteComponent -n "deleteComponent6";
	rename -uid "E1784EEC-420B-FE80-1E82-ADB278B9D9B7";
	setAttr ".dc" -type "componentList" 1 "f[113:115]";
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "9193E223-4F9A-63C4-E7BF-CDB70A87E347";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[176]" "e[178:180]" "e[225]" "e[227:228]" "e[230]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.44216672 0.3215144 2.540044e-09 ;
	setAttr ".rs" 41901;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.53404741250891807 0.30349320242041417 -0.027683328885898754 ;
	setAttr ".cbx" -type "double3" -0.35028602444548734 0.3395355946742899 0.027683333965986559 ;
createNode polyBridgeEdge -n "polyBridgeEdge5";
	rename -uid "8FE66D6D-4ED7-8F00-CD24-E191FD6DACBD";
	setAttr ".ics" -type "componentList" 2 "e[272]" "e[282]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 138;
	setAttr ".sv2" 144;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polyTweak -n "polyTweak9";
	rename -uid "E442BE09-4B30-CC79-803F-DEB5172A33FA";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[89]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[90]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[91]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[92]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[115]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".tk[116]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".tk[117]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".tk[118]" -type "float3" 0 0 9.3132257e-10 ;
	setAttr ".tk[138]" -type "float3" 0.033105705 -0.044087365 2.7939677e-09 ;
	setAttr ".tk[139]" -type "float3" 0.0036262944 -0.049577534 2.7939677e-09 ;
	setAttr ".tk[140]" -type "float3" -0.088002667 -0.067679651 2.7939677e-09 ;
	setAttr ".tk[141]" -type "float3" -0.034095705 -0.040480934 2.7939677e-09 ;
	setAttr ".tk[142]" -type "float3" -0.034095705 -0.040480934 -2.7939677e-09 ;
	setAttr ".tk[143]" -type "float3" 0.033105705 -0.04408735 -2.7939677e-09 ;
	setAttr ".tk[144]" -type "float3" 0.0036262944 -0.049577519 -2.7939677e-09 ;
	setAttr ".tk[145]" -type "float3" -0.088002667 -0.067679651 -2.7939677e-09 ;
createNode polyBridgeEdge -n "polyBridgeEdge6";
	rename -uid "A48DC03E-4372-05AC-0969-4DA2E071E4FB";
	setAttr ".ics" -type "componentList" 2 "e[274]" "e[284]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 139;
	setAttr ".sv2" 145;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge7";
	rename -uid "FAD3FA32-4006-14AE-8320-1385A1FED61A";
	setAttr ".ics" -type "componentList" 2 "e[278]" "e[285]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 140;
	setAttr ".sv2" 142;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "5A5BDAC9-4646-61D8-2C16-9BA0125CD95C";
	setAttr ".ics" -type "componentList" 1 "f[141:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.45766526 0.27923027 2.540044e-09 ;
	setAttr ".rs" 44572;
	setAttr ".lt" -type "double3" -1.7347234759768071e-18 4.4177202911523671e-18 0.041851364848184774 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.50094174110312584 0.26301226088637181 -0.027683326345854849 ;
	setAttr ".cbx" -type "double3" -0.4143887933434976 0.29544824430579014 0.027683331425942658 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "E45AA87F-4E52-2322-AD04-D2B2994A13F1";
	setAttr ".ics" -type "componentList" 1 "f[141:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.47218901 0.2399798 2.540044e-09 ;
	setAttr ".rs" 33404;
	setAttr ".lt" -type "double3" -0.051021278928277056 -2.4041728554407305e-17 0.10921825711564795 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.51546536647696617 0.22376185724371739 -0.027683326345854849 ;
	setAttr ".cbx" -type "double3" -0.42891265713591703 0.25619775125616856 0.027683331425942658 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "5D707F99-4B6B-41A6-D1A2-2E9F51705EC3";
	setAttr ".ics" -type "componentList" 1 "f[141:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.54100549 0.16870303 2.540044e-09 ;
	setAttr ".rs" 60266;
	setAttr ".lt" -type "double3" 5.4613375716507074e-17 6.361792343537369e-26 0.039864205874860385 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.60121427738089683 0.16870287129515477 -0.027683326345854849 ;
	setAttr ".cbx" -type "double3" -0.4807967271747125 0.16870318421953984 0.027683331425942658 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "103EB941-47F4-F09C-E9EF-FF9B2F423145";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[138:161]" -type "float3"  -0.0029168902 0 0 -0.0029168902
		 0 0 0 0 0 0 0 0 0 0 0 -0.0029168902 0 0 -0.0029168902 0 0 0 0 0 0.0058337911 0 0
		 0.0058337911 0 0 0.0058337911 0 0 0.0058337911 0 0 -0.014584472 0 0 -0.014584472
		 0 0 -0.014584472 0 0 -0.014584472 0 0 0 -0.0027660523 0 0 0.002344213 0 0 0.002344213
		 0 0 -0.0027660523 0 0.033870827 0.020443087 0 0.033870827 0.020443087 0 0.033870827
		 0.029663481 0 0.033870827 0.029663481 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "2C6D63A1-4890-1E60-CB79-5F8C09E34310";
	setAttr ".ics" -type "componentList" 1 "f[141:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.53904712 0.13630958 2.540044e-09 ;
	setAttr ".rs" 34512;
	setAttr ".lt" -type "double3" 0.076720928004215178 -9.681226589713882e-18 0.069905902824188348 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.59016885125060203 0.11593535611265965 -0.027683326345854849 ;
	setAttr ".cbx" -type "double3" -0.48792541288752683 0.15668380329245396 0.027683331425942658 ;
createNode polyTweak -n "polyTweak11";
	rename -uid "7B344EB6-4C60-D20F-FD68-22B95CFD8E4F";
	setAttr ".uopa" yes;
	setAttr -s 34 ".tk";
	setAttr ".tk[92]" -type "float3" -0.00077619404 0.00061992556 1.1175871e-07 ;
	setAttr ".tk[118]" -type "float3" -0.00077627599 0.0006198436 1.9669533e-06 ;
	setAttr ".tk[138]" -type "float3" -0.0012843013 -0.0059751272 0 ;
	setAttr ".tk[139]" -type "float3" -0.00081160665 -0.0037757382 0 ;
	setAttr ".tk[140]" -type "float3" 0.00066712499 0.0031039864 0 ;
	setAttr ".tk[141]" -type "float3" 0.0041004121 0.0045132488 -1.7397106e-06 ;
	setAttr ".tk[142]" -type "float3" 0.0041003749 0.0045132264 -8.1993639e-06 ;
	setAttr ".tk[143]" -type "float3" -0.0012843013 -0.0059751272 0 ;
	setAttr ".tk[144]" -type "float3" -0.00081160665 -0.0037757382 0 ;
	setAttr ".tk[145]" -type "float3" 0.00066712499 0.0031039864 0 ;
	setAttr ".tk[146]" -type "float3" 0.0014919937 0.0024501979 0 ;
	setAttr ".tk[147]" -type "float3" -6.7234039e-05 -0.0001104027 0 ;
	setAttr ".tk[148]" -type "float3" -6.7234039e-05 -0.0001104027 0 ;
	setAttr ".tk[149]" -type "float3" 0.0014919937 0.0024501979 0 ;
	setAttr ".tk[150]" -type "float3" 0.0005633831 0.00092513859 0 ;
	setAttr ".tk[151]" -type "float3" 0.0005633831 0.00092513859 0 ;
	setAttr ".tk[152]" -type "float3" -0.0034294128 -0.0021139383 9.9092722e-07 ;
	setAttr ".tk[153]" -type "float3" -0.0034291744 -0.0021136254 4.4479966e-06 ;
	setAttr ".tk[154]" -type "float3" -0.0028722659 -0.0012833402 0 ;
	setAttr ".tk[155]" -type "float3" -0.003389335 -0.0013617809 0 ;
	setAttr ".tk[156]" -type "float3" -0.003389335 -0.0013617809 0 ;
	setAttr ".tk[157]" -type "float3" -0.0028722659 -0.0012833402 0 ;
	setAttr ".tk[158]" -type "float3" -0.0063271374 -0.0018079105 0 ;
	setAttr ".tk[159]" -type "float3" -0.0063271374 -0.0018079105 0 ;
	setAttr ".tk[160]" -type "float3" -0.008887792 -0.00059575133 1.3113022e-06 ;
	setAttr ".tk[161]" -type "float3" -0.0088878665 -0.00059584074 5.479902e-06 ;
	setAttr ".tk[162]" -type "float3" 0.011045548 -0.012903641 0 ;
	setAttr ".tk[163]" -type "float3" 0.010024233 -0.0075923605 0 ;
	setAttr ".tk[164]" -type "float3" 0.010024233 -0.0075923605 0 ;
	setAttr ".tk[165]" -type "float3" 0.011045548 -0.012903641 0 ;
	setAttr ".tk[166]" -type "float3" 0.0027851255 0.022023806 0 ;
	setAttr ".tk[167]" -type "float3" 0.0027851255 0.022023806 0 ;
	setAttr ".tk[168]" -type "float3" -0.0071285451 0.027845137 0 ;
	setAttr ".tk[169]" -type "float3" -0.0071285451 0.027845137 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "4FCDFC6E-48A6-1D9B-453F-A8B9B712336A";
	setAttr ".ics" -type "componentList" 1 "f[141:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.40916285 0.07513836 2.540044e-09 ;
	setAttr ".rs" 56360;
	setAttr ".lt" -type "double3" -1.3444106938820255e-17 -1.5227364433599286e-18 0.039953775345082522 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.41284445934910902 0.027842105122745708 -0.027683326345854849 ;
	setAttr ".cbx" -type "double3" -0.40548121445794233 0.12243461677664585 0.027683331425942658 ;
createNode polyTweak -n "polyTweak12";
	rename -uid "B7FD9942-405B-73E9-2EFB-9BAC5AB0ECAB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[170:177]" -type "float3"  0.080196716 -0.051498767 0
		 0.067890272 -0.043397136 0 0.067890272 -0.043397136 0 0.080196716 -0.051498767 0
		 -0.0014505521 -0.0066462145 0 -0.0014505447 -0.0066462145 0 -0.014842158 0.001921316
		 0 -0.014842158 0.001921316 0;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "A8505BFD-4010-AEAB-F9CC-7B85165A1D17";
	setAttr ".ics" -type "componentList" 1 "f[141:143]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.37296626 0.082947925 2.540044e-09 ;
	setAttr ".rs" 58959;
	setAttr ".lt" -type "double3" -0.072234084123436154 1.9037218604417537e-17 0.17373137623902252 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.3939616184772981 0.040407509299457745 -0.027683326345854849 ;
	setAttr ".cbx" -type "double3" -0.35197087266464361 0.12548834154242344 0.027683331425942658 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "75816C11-4F87-69CF-4119-BDBC0A1B775C";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[178:185]" -type "float3"  0.021040287 0.015666014 0
		 0.014042181 0.014317271 0 0.014042181 0.014317271 0 0.021040287 0.015666014 0 -0.020695645
		 0.0076224939 0 -0.020695645 0.0076224939 0 -0.028313721 0.0061542755 0 -0.028313721
		 0.0061542755 0;
createNode polyTweak -n "polyTweak14";
	rename -uid "6551A063-4CF7-4C51-A76D-4ABA73434D5D";
	setAttr ".uopa" yes;
	setAttr -s 48 ".tk";
	setAttr ".tk[0]" -type "float3" -0.00039298087 0.00030957907 3.5986304e-06 ;
	setAttr ".tk[6]" -type "float3" 0.00018902868 -0.00016079843 -3.5986304e-06 ;
	setAttr ".tk[34]" -type "float3" 0.00018902496 -0.00016079098 3.5986304e-06 ;
	setAttr ".tk[38]" -type "float3" -0.00038698316 0.00030519813 -3.5986304e-06 ;
	setAttr ".tk[91]" -type "float3" -0.00018903986 0.00016079471 -3.5576522e-06 ;
	setAttr ".tk[92]" -type "float3" 0.00039160997 -0.00030855089 3.5576522e-06 ;
	setAttr ".tk[117]" -type "float3" -0.00018904731 0.00016079471 3.5576522e-06 ;
	setAttr ".tk[118]" -type "float3" 0.00038833916 -0.00030620396 -3.5576522e-06 ;
	setAttr ".tk[154]" -type "float3" 0 -0.00026240567 0 ;
	setAttr ".tk[155]" -type "float3" 0 -0.00018388408 0 ;
	setAttr ".tk[156]" -type "float3" 0 -0.00018388408 0 ;
	setAttr ".tk[157]" -type "float3" 0 -0.00026240567 0 ;
	setAttr ".tk[158]" -type "float3" 0 0.00026240147 0 ;
	setAttr ".tk[159]" -type "float3" 0 0.00026240147 0 ;
	setAttr ".tk[160]" -type "float3" -0.0040205889 -0.00094966748 0 ;
	setAttr ".tk[161]" -type "float3" -0.0040205889 -0.00094958086 0 ;
	setAttr ".tk[162]" -type "float3" 0.0050791358 -0.0073315948 0 ;
	setAttr ".tk[163]" -type "float3" 0.0036384112 -0.0062296242 0 ;
	setAttr ".tk[164]" -type "float3" 0.0036383297 -0.0062294984 0 ;
	setAttr ".tk[165]" -type "float3" 0.0050790887 -0.007331443 0 ;
	setAttr ".tk[166]" -type "float3" -0.0023167764 0.0010344352 0 ;
	setAttr ".tk[167]" -type "float3" -0.0023167962 0.0010345868 0 ;
	setAttr ".tk[168]" -type "float3" -0.0038489627 0.0021684768 0 ;
	setAttr ".tk[169]" -type "float3" -0.003849043 0.0021686025 0 ;
	setAttr ".tk[170]" -type "float3" 0.00064781436 0.0027742321 0 ;
	setAttr ".tk[171]" -type "float3" 0.00064781436 -5.7364407e-05 0 ;
	setAttr ".tk[172]" -type "float3" 0.00064781436 -5.7364407e-05 0 ;
	setAttr ".tk[173]" -type "float3" 0.00064781436 0.0027742321 0 ;
	setAttr ".tk[174]" -type "float3" -0.00064781454 -0.0076347552 0 ;
	setAttr ".tk[175]" -type "float3" -0.00064781454 -0.0076347552 0 ;
	setAttr ".tk[176]" -type "float3" -0.00064781454 -0.010717152 0 ;
	setAttr ".tk[177]" -type "float3" -0.00064781454 -0.010717152 0 ;
	setAttr ".tk[178]" -type "float3" -0.00064781454 0.0065996763 0 ;
	setAttr ".tk[179]" -type "float3" -0.00064781454 0.0040528448 0 ;
	setAttr ".tk[180]" -type "float3" -0.00064781454 0.0040528448 0 ;
	setAttr ".tk[181]" -type "float3" -0.00064781454 0.0065996763 0 ;
	setAttr ".tk[182]" -type "float3" -0.00064781454 -0.0085893646 0 ;
	setAttr ".tk[183]" -type "float3" -0.00064781454 -0.0085893646 0 ;
	setAttr ".tk[184]" -type "float3" -0.00064781454 -0.01136183 0 ;
	setAttr ".tk[185]" -type "float3" -0.00064781454 -0.01136183 0 ;
	setAttr ".tk[186]" -type "float3" 0.00038292276 -0.0011542144 0 ;
	setAttr ".tk[187]" -type "float3" 0.0063366033 -0.00063444272 0 ;
	setAttr ".tk[188]" -type "float3" 0.0063366033 -0.00063444272 0 ;
	setAttr ".tk[189]" -type "float3" 0.00038292276 -0.0011542144 0 ;
	setAttr ".tk[190]" -type "float3" 0.035891172 0.0019457494 0 ;
	setAttr ".tk[191]" -type "float3" 0.035891172 0.0019457494 0 ;
	setAttr ".tk[192]" -type "float3" 0.042372838 0.002511593 0 ;
	setAttr ".tk[193]" -type "float3" 0.042372838 0.002511593 0 ;
createNode polySplit -n "polySplit1";
	rename -uid "2AD35200-4448-527B-31AE-B88F0A0ECD4F";
	setAttr -s 10 ".e[0:9]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.511316 0.5
		 0.5;
	setAttr -s 10 ".d[0:9]"  -2147483420 -2147483644 -2147483639 -2147483635 -2147483379 -2147483429 
		-2147483631 -2147483626 -2147483621 -2147483470;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "deleteComponent7";
	rename -uid "FC1D5CFB-4DE7-EDCD-625C-8EBE3EFC7ADD";
	setAttr ".dc" -type "componentList" 1 "e[283]";
createNode deleteComponent -n "deleteComponent8";
	rename -uid "31F1D29F-48F7-62F8-DB37-35BAB77FBA2F";
	setAttr ".dc" -type "componentList" 1 "e[273]";
createNode polySplit -n "polySplit2";
	rename -uid "667E13D3-4F29-131B-8CA7-28A6D40BD3AD";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483371 -2147483470;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "DE85EBB4-46A0-5C2C-6627-5E922B7EBB1B";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483366 -2147483266;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "27A06480-4D35-4861-D35C-C58555AF4228";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -622.61902287839052 -395.23807953274502 ;
	setAttr ".tgi[0].vh" -type "double2" 592.85711929911758 413.09522168030884 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -157.14285278320313;
	setAttr ".tgi[0].ni[0].y" 375.71429443359375;
	setAttr ".tgi[0].ni[0].nvs" 3042;
	setAttr ".tgi[0].ni[1].x" -534.28570556640625;
	setAttr ".tgi[0].ni[1].y" 371.42855834960938;
	setAttr ".tgi[0].ni[1].nvs" 3042;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "50ABC7B6-46A6-753D-6E2D-6F80B9E130BC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak15";
	rename -uid "9C10343D-4FB4-29D7-6A66-4594BBE50716";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[140]" -type "float3" -0.0026380334 -0.0066443346 3.1292439e-07 ;
	setAttr ".tk[145]" -type "float3" -0.0026380762 -0.0066442029 3.6507845e-07 ;
	setAttr ".tk[150]" -type "float3" -0.0010573776 -0.00047596404 7.4505806e-09 ;
	setAttr ".tk[151]" -type "float3" -0.0010573859 -0.00047595007 2.2351742e-08 ;
	setAttr ".tk[158]" -type "float3" 0.00030405365 0.011272093 -3.6507845e-07 ;
	setAttr ".tk[159]" -type "float3" 0.00030410674 0.011271983 -4.2840838e-07 ;
	setAttr ".tk[194]" -type "float3" 0.036836773 1.8626451e-09 0 ;
	setAttr ".tk[195]" -type "float3" 0.036836769 0 0 ;
	setAttr ".tk[196]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[197]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[198]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[199]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[200]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[201]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[202]" -type "float3" 0.036836777 0 0 ;
	setAttr ".tk[203]" -type "float3" 0.036836773 0 0 ;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "6EE38D5B-463A-11A7-C2EE-0FA83B201077";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:402]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".a" 0;
createNode polySplit -n "polySplit4";
	rename -uid "D9EE043C-4E8B-DD67-1353-8C8265FBD3F1";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483486 -2147483593;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "41E9BE45-438F-939E-09FE-6D86D99F9190";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483640 -2147483603;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "79AAD338-4F41-C729-0DF8-FEB297831E05";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".a" 0;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "4A460361-4BBD-F7EB-92BE-A99EE6D83597";
	setAttr ".ics" -type "componentList" 3 "f[33:35]" "f[98:100]" "f[121:123]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.0097139897 0.39099321 0 ;
	setAttr ".rs" 41831;
	setAttr ".lt" -type "double3" -4.2243727859835831e-18 1.4971449540084421e-18 0.012225116295170271 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.0097139898596274032 0.30349320242041417 -0.051137840279809665 ;
	setAttr ".cbx" -type "double3" 0.0097139898596274032 0.47849321434134312 0.051137840279809665 ;
createNode polySplit -n "polySplit6";
	rename -uid "9458B748-48FB-5425-923D-169CA8F76203";
	setAttr -s 13 ".e[0:12]"  0.28534201 0.28534201 0.28534201 0.28534201
		 0.28534201 0.28534201 0.28534201 0.28534201 0.28534201 0.71465802 0.71465802 0.28534201
		 0.28534201;
	setAttr -s 13 ".d[0:12]"  -2147483568 -2147483555 -2147483557 -2147483409 -2147483454 -2147483559 
		-2147483561 -2147483563 -2147483565 -2147483451 -2147483406 -2147483567 -2147483568;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "4D162A7A-4C77-42D5-3853-8E93569A1547";
	setAttr ".ics" -type "componentList" 2 "f[116:119]" "f[218]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.13161513 0.32652989 -1.270022e-09 ;
	setAttr ".rs" 54753;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.23028601967711576 0.30349320242041417 -0.027683336506030463 ;
	setAttr ".cbx" -type "double3" -0.032944249743415199 0.34956654975050755 0.027683333965986559 ;
createNode polyTweak -n "polyTweak16";
	rename -uid "F9D40935-4829-FD92-996C-0D94162A52FB";
	setAttr ".uopa" yes;
	setAttr -s 44 ".tk";
	setAttr ".tk[95]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[96]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[97]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[98]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[99]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[100]" -type "float3" 0 0 -1.1175871e-08 ;
	setAttr ".tk[121]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[122]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[123]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[124]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[125]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[126]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[182]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[183]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[184]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[185]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[186]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[187]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[188]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[189]" -type "float3" -0.0061263279 0 0 ;
	setAttr ".tk[216]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[217]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[218]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[219]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[220]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[221]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[222]" -type "float3" 0 7.4505806e-08 0 ;
	setAttr ".tk[223]" -type "float3" 0 7.4505806e-08 0 ;
	setAttr ".tk[224]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[225]" -type "float3" 0 8.9406967e-08 0 ;
	setAttr ".tk[226]" -type "float3" 0 2.6077032e-08 0 ;
	setAttr ".tk[227]" -type "float3" 0 2.6077032e-08 0 ;
	setAttr ".tk[228]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[229]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[230]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[231]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[232]" -type "float3" 0 -0.17613329 0 ;
	setAttr ".tk[233]" -type "float3" 0 -0.17613329 0 ;
	setAttr ".tk[234]" -type "float3" 0 -0.17613329 0 ;
	setAttr ".tk[235]" -type "float3" 0 -0.17613329 0 ;
	setAttr ".tk[236]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[237]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[238]" -type "float3" 0 -0.13006034 0 ;
	setAttr ".tk[239]" -type "float3" 0 -0.13006034 0 ;
createNode polySplit -n "polySplit7";
	rename -uid "5690A523-4A79-2569-CE9B-C1957B19B1E6";
	setAttr -s 13 ".e[0:12]"  0.53790301 0.53790301 0.53790301 0.53790301
		 0.53790301 0.53790301 0.53790301 0.53790301 0.53790301 0.53790301 0.53790301 0.53790301
		 0.53790301;
	setAttr -s 13 ".d[0:12]"  -2147483199 -2147483198 -2147483191 -2147483184 -2147483179 -2147483175 
		-2147483176 -2147483181 -2147483186 -2147483189 -2147483196 -2147483194 -2147483199;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak17";
	rename -uid "39F011AC-4450-2752-6F72-5CB4E42DB2E9";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[232]" -type "float3" 3.7252903e-09 0 0 ;
	setAttr ".tk[233]" -type "float3" 3.7252903e-09 0 0 ;
	setAttr ".tk[240]" -type "float3" 0 -0.002094205 0 ;
	setAttr ".tk[241]" -type "float3" 0 -0.002094205 0 ;
	setAttr ".tk[242]" -type "float3" 3.7252903e-09 -0.023384571 0 ;
	setAttr ".tk[243]" -type "float3" 0 -0.023384571 0 ;
	setAttr ".tk[244]" -type "float3" 0 -0.002094205 0 ;
	setAttr ".tk[245]" -type "float3" 0 -0.0020942534 0 ;
	setAttr ".tk[246]" -type "float3" 0 -0.0020942534 0 ;
	setAttr ".tk[247]" -type "float3" 0 -0.002094205 0 ;
	setAttr ".tk[248]" -type "float3" 0 -0.023384571 0 ;
	setAttr ".tk[249]" -type "float3" 3.7252903e-09 -0.023384571 0 ;
	setAttr ".tk[250]" -type "float3" 0 -0.002094205 0 ;
	setAttr ".tk[251]" -type "float3" 0 -0.002094205 0 ;
createNode polySplit -n "polySplit8";
	rename -uid "865A9894-41D0-0BC6-8290-63B1FA36A50A";
	setAttr -s 17 ".e[0:16]"  0.213313 0.213313 0.78668702 0.213313 0.213313
		 0.213313 0.213313 0.213313 0.213313 0.213313 0.213313 0.78668702 0.78668702 0.213313
		 0.213313 0.213313 0.213313;
	setAttr -s 17 ".d[0:16]"  -2147483552 -2147483417 -2147483151 -2147483185 -2147483183 -2147483157 
		-2147483461 -2147483539 -2147483541 -2147483543 -2147483545 -2147483451 -2147483406 -2147483547 -2147483549 -2147483551 -2147483552;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak18";
	rename -uid "F14A1DF6-425A-0506-66A7-E2A87D5D9765";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[232]" -type "float3" 0 0.032702845 0 ;
	setAttr ".tk[233]" -type "float3" 0 0.032702845 0 ;
	setAttr ".tk[234]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[235]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[236]" -type "float3" 0 0.080865182 0 ;
	setAttr ".tk[237]" -type "float3" 0 0.080865182 0 ;
	setAttr ".tk[238]" -type "float3" 0 0.099892244 0 ;
	setAttr ".tk[239]" -type "float3" 0 0.099892244 0 ;
	setAttr ".tk[242]" -type "float3" 0 0.032702845 0 ;
	setAttr ".tk[243]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[244]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[245]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[246]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[247]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[248]" -type "float3" 0 0.049351584 0 ;
	setAttr ".tk[249]" -type "float3" 0 0.032702845 0 ;
	setAttr ".tk[254]" -type "float3" 0 0.049351569 0 ;
	setAttr ".tk[255]" -type "float3" 0 0.049351569 0 ;
	setAttr ".tk[256]" -type "float3" 0 0.049351569 0 ;
	setAttr ".tk[257]" -type "float3" 0 0.049351569 0 ;
createNode deleteComponent -n "deleteComponent9";
	rename -uid "997A39FD-4475-16A1-0D60-2295E79E83F6";
	setAttr ".dc" -type "componentList" 3 "e[489:500]" "e[503]" "e[506]";
createNode deleteComponent -n "deleteComponent10";
	rename -uid "FC37ADA1-4473-2513-C31B-AA994D9E2A73";
	setAttr ".dc" -type "componentList" 18 "vtx[1:3]" "vtx[7:9]" "vtx[35:37]" "vtx[39:41]" "vtx[48:52]" "vtx[59:60]" "vtx[67]" "vtx[69]" "vtx[74]" "vtx[76:77]" "vtx[82:83]" "vtx[94:100]" "vtx[118:124]" "vtx[182:189]" "vtx[200:203]" "vtx[208:211]" "vtx[217:222]" "vtx[228:259]";
createNode polyTweak -n "polyTweak19";
	rename -uid "C716D58D-483B-3B64-C44F-4DAE6F660189";
	setAttr ".uopa" yes;
	setAttr -s 62 ".tk";
	setAttr ".tk[42]" -type "float3" 0.015885569 0 0 ;
	setAttr ".tk[43]" -type "float3" 0.015885569 0 0 ;
	setAttr ".tk[84]" -type "float3" 0.015885569 0 0 ;
	setAttr ".tk[85]" -type "float3" 0.015885569 0 0 ;
	setAttr ".tk[89]" -type "float3" 0.015885569 0 0 ;
	setAttr ".tk[92]" -type "float3" 0.00030440837 -0.00025086105 0 ;
	setAttr ".tk[95]" -type "float3" 0 0 -0.014094537 ;
	setAttr ".tk[96]" -type "float3" 0 0 -0.014094537 ;
	setAttr ".tk[97]" -type "float3" 0 0 -0.014094537 ;
	setAttr ".tk[98]" -type "float3" 0 0 -0.014094537 ;
	setAttr ".tk[99]" -type "float3" 0 0 -0.014094537 ;
	setAttr ".tk[113]" -type "float3" 0.015885569 0 0 ;
	setAttr ".tk[116]" -type "float3" 0.00030697882 -0.00025298446 0 ;
	setAttr ".tk[119]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[120]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[121]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[122]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[123]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[137]" -type "float3" 0.00013374537 -0.00011021644 0 ;
	setAttr ".tk[138]" -type "float3" 0.00013323873 -0.00010979176 0 ;
	setAttr ".tk[148]" -type "float3" -0.00056253374 0.00046357512 0 ;
	setAttr ".tk[149]" -type "float3" -0.00056308508 0.00046397746 0 ;
	setAttr ".tk[156]" -type "float3" 0.00055320561 -0.00045591593 0 ;
	setAttr ".tk[157]" -type "float3" 0.00055272877 -0.00045548379 0 ;
	setAttr ".tk[170]" -type "float3" 0.010521569 -0.0037327693 0 ;
	setAttr ".tk[171]" -type "float3" 0.010521569 -0.0037327693 0 ;
	setAttr ".tk[172]" -type "float3" 0.01226481 -0.0037327693 0 ;
	setAttr ".tk[173]" -type "float3" 0.01226481 -0.0037327693 0 ;
	setAttr ".tk[174]" -type "float3" -4.6566129e-10 9.3132257e-10 0 ;
	setAttr ".tk[175]" -type "float3" -4.6566129e-10 9.3132257e-10 0 ;
	setAttr ".tk[176]" -type "float3" -4.6566129e-10 9.3132257e-10 0 ;
	setAttr ".tk[177]" -type "float3" -4.6566129e-10 9.3132257e-10 0 ;
	setAttr ".tk[178]" -type "float3" 0.012264811 -0.0037327698 0 ;
	setAttr ".tk[179]" -type "float3" 0.012264811 -0.0037327698 0 ;
	setAttr ".tk[180]" -type "float3" 0.012264811 -0.0037327698 0 ;
	setAttr ".tk[181]" -type "float3" 0.012264811 -0.0037327698 0 ;
	setAttr ".tk[182]" -type "float3" 0.00053634821 2.7939677e-09 0 ;
	setAttr ".tk[183]" -type "float3" 0.00053634821 2.7939677e-09 0 ;
	setAttr ".tk[184]" -type "float3" 0.00053634821 2.7939677e-09 0 ;
	setAttr ".tk[185]" -type "float3" 0.00053634821 2.7939677e-09 0 ;
	setAttr ".tk[186]" -type "float3" 0.00053634821 9.3132257e-10 0 ;
	setAttr ".tk[187]" -type "float3" 0.00053634821 9.3132257e-10 0 ;
	setAttr ".tk[188]" -type "float3" 0.00053634821 9.3132257e-10 0 ;
	setAttr ".tk[189]" -type "float3" 0.00053634821 9.3132257e-10 0 ;
	setAttr ".tk[219]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[220]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[228]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[229]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[230]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[231]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[232]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[233]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[234]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[235]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[236]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[237]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[238]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[239]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[241]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[242]" -type "float3" 0 0 0.014094537 ;
	setAttr ".tk[243]" -type "float3" 0 0 -0.014094532 ;
	setAttr ".tk[244]" -type "float3" 0 0 -0.014094537 ;
createNode deleteComponent -n "deleteComponent11";
	rename -uid "E75F12AB-446C-E66A-2E3B-528DC3FF1C4F";
	setAttr ".dc" -type "componentList" 1 "f[229]";
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	rename -uid "372754FF-4C20-BBA4-E40B-03858F85AF65";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[183]" "e[452]" "e[465]" "e[476]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.23028602 0.23846303 0 ;
	setAttr ".rs" 49217;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.23028601967711576 0.17343285748595066 -0.037293524031025788 ;
	setAttr ".cbx" -type "double3" -0.23028601967711576 0.30349320242041417 0.037293524031025788 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "05F2434D-44F1-9E00-511B-89AADA263536";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1319\n            -height 726\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n"
		+ "                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1319\\n    -height 726\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1319\\n    -height 726\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "FE21E378-41BA-72AF-6FD4-7AA27DFA4FC7";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyTweak -n "polyTweak20";
	rename -uid "C9F5505A-43E2-F424-2F7B-7E9B7CE6973E";
	setAttr ".uopa" yes;
	setAttr -s 36 ".tk";
	setAttr ".tk[182]" -type "float3" 0.0030707419 0 0 ;
	setAttr ".tk[183]" -type "float3" 0.0030708015 0 0 ;
	setAttr ".tk[184]" -type "float3" 0.0030708015 0 0 ;
	setAttr ".tk[185]" -type "float3" 0.0030707419 0 0 ;
	setAttr ".tk[186]" -type "float3" 0.0030710995 0 0 ;
	setAttr ".tk[187]" -type "float3" 0.0030710995 0 0 ;
	setAttr ".tk[188]" -type "float3" 0.003071174 0 0 ;
	setAttr ".tk[189]" -type "float3" 0.003071174 0 0 ;
	setAttr ".tk[229]" -type "float3" 0 9.3132257e-10 0 ;
	setAttr ".tk[230]" -type "float3" -9.3132257e-10 -9.3132257e-10 0 ;
	setAttr ".tk[232]" -type "float3" -6.9849193e-10 -1.1641532e-10 0 ;
	setAttr ".tk[233]" -type "float3" -6.9849193e-10 -1.1641532e-10 0 ;
	setAttr ".tk[234]" -type "float3" -0.00010450743 0.00039104783 0 ;
	setAttr ".tk[235]" -type "float3" -0.00010450743 0.00039104783 0 ;
	setAttr ".tk[236]" -type "float3" -0.0002354118 -0.031122137 0 ;
	setAttr ".tk[237]" -type "float3" -0.00023539644 -0.031122137 0 ;
	setAttr ".tk[238]" -type "float3" 0.00033989176 -0.050149195 0 ;
	setAttr ".tk[239]" -type "float3" 0.00033992156 -0.050149195 0 ;
	setAttr ".tk[240]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[241]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[242]" -type "float3" -9.3132257e-10 0.00039082803 0 ;
	setAttr ".tk[243]" -type "float3" -9.3132257e-10 0.00039082803 0 ;
	setAttr ".tk[244]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[245]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[246]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[247]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[248]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[249]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[250]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[251]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[252]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[253]" -type "float3" -1.8626451e-09 0 0 ;
	setAttr ".tk[254]" -type "float3" 0 -0.033829659 0.014094546 ;
	setAttr ".tk[255]" -type "float3" 0 -0.033829659 -0.014094541 ;
	setAttr ".tk[256]" -type "float3" 0 0.0074843764 0.014094542 ;
	setAttr ".tk[257]" -type "float3" 0 0.0074843764 -0.014094541 ;
createNode polySplit -n "polySplit9";
	rename -uid "5EA526DC-456A-52F9-206A-6F9DACF47991";
	setAttr -s 16 ".e[0:15]"  0.37189201 0.37189201 0.37189201 0.37189201
		 0.62810802 0.37189201 0.37189201 0.37189201 0.37189201 0.37189201 0.37189201 0.37189201
		 0.37189201 0.37189201 0.37189201 0.37189201;
	setAttr -s 16 ".d[0:15]"  -2147483137 -2147483183 -2147483182 -2147483181 -2147483154 -2147483180 
		-2147483179 -2147483178 -2147483177 -2147483176 -2147483175 -2147483156 -2147483174 -2147483173 -2147483172 -2147483136;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit10";
	rename -uid "DF313A45-44A8-3BD4-1C0F-34B4A6FA4757";
	setAttr -s 16 ".e[0:15]"  0.80238801 0.80238801 0.80238801 0.80238801
		 0.80238801 0.80238801 0.80238801 0.80238801 0.80238801 0.80238801 0.80238801 0.197612
		 0.80238801 0.80238801 0.80238801 0.80238801;
	setAttr -s 16 ".d[0:15]"  -2147483120 -2147483121 -2147483122 -2147483123 -2147483124 -2147483125 
		-2147483126 -2147483127 -2147483128 -2147483129 -2147483130 -2147483154 -2147483132 -2147483133 -2147483134 -2147483135;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "2DD5D3D4-415A-F008-4B6B-45BD446AE4A8";
	setAttr ".ics" -type "componentList" 4 "vtx[182:189]" "vtx[254:258]" "vtx[273:274]" "vtx[289]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak21";
	rename -uid "17C52CDE-4922-8A2E-299B-75B2ABFF8BFA";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[258]" -type "float3" 0 0.019276604 0 ;
	setAttr ".tk[273]" -type "float3" 0 0.019276604 0 ;
	setAttr ".tk[274]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[275]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[276]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[277]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[278]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[279]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[280]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[281]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[282]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[283]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[284]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[285]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[286]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[287]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[288]" -type "float3" 0 0.0017194491 0 ;
	setAttr ".tk[289]" -type "float3" 0 0.0017194491 0 ;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "A4C6DCC5-45BD-9CFD-48A8-2E917A2F0157";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.6818378433036606 0 -0.29028602206130155 0.39099320838087864 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak22";
	rename -uid "681BFC67-4F48-EF5D-A0E5-EA858C60787E";
	setAttr ".uopa" yes;
	setAttr -s 26 ".tk";
	setAttr ".tk[95]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[116]" -type "float3" 7.4505806e-09 -7.4505806e-09 0 ;
	setAttr ".tk[119]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[138]" -type "float3" -7.4505806e-09 0 0 ;
	setAttr ".tk[156]" -type "float3" 1.4901161e-08 0 0 ;
	setAttr ".tk[157]" -type "float3" 1.4901161e-08 -1.4901161e-08 0 ;
	setAttr ".tk[182]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[183]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[184]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[185]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[186]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[187]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[188]" -type "float3" -1.9557774e-08 1.268927e-08 0 ;
	setAttr ".tk[189]" -type "float3" -1.9557774e-08 1.268927e-08 0 ;
	setAttr ".tk[228]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[229]" -type "float3" 0.00011129677 -0.00011219084 0 ;
	setAttr ".tk[230]" -type "float3" 0.00011129677 -0.00011219084 0 ;
	setAttr ".tk[231]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[232]" -type "float3" -6.7874789e-05 6.8441033e-05 0 ;
	setAttr ".tk[233]" -type "float3" -6.7874789e-05 6.8441033e-05 0 ;
	setAttr ".tk[242]" -type "float3" 2.4482608e-05 -2.4631619e-05 0 ;
	setAttr ".tk[243]" -type "float3" 2.4482608e-05 -2.4631619e-05 0 ;
	setAttr ".tk[254]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[267]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[268]" -type "float3" -1.9557774e-08 0 0 ;
	setAttr ".tk[281]" -type "float3" -1.9557774e-08 0 0 ;
createNode deleteComponent -n "deleteComponent12";
	rename -uid "2AD9A814-4CC8-068C-577B-219D66FF3D64";
	setAttr ".dc" -type "componentList" 1 "e[387:388]";
createNode polySplit -n "polySplit11";
	rename -uid "7C0C4627-426C-034D-DE31-44A7FA226F2D";
	setAttr -s 6 ".e[0:5]"  1 0.5 0.5 0.5 0.5 0;
	setAttr -s 6 ".d[0:5]"  -2147483640 -2147483604 -2147483437 -2147483480 -2147483593 -2147483488;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCube -n "polyCube2";
	rename -uid "FC251AB6-4362-8323-9CE0-C183053371CC";
	setAttr ".w" 0.25;
	setAttr ".h" 0.15;
	setAttr ".d" 0.1;
	setAttr ".sw" 4;
	setAttr ".sh" 5;
	setAttr ".sd" 2;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak23";
	rename -uid "B5EB3E1B-4732-A533-2645-9EB639B1C79B";
	setAttr ".uopa" yes;
	setAttr -s 78 ".tk[0:77]" -type "float3"  0 -0.0082555255 -0.021655222
		 0 -0.0082555255 -0.021655222 1.8330949e-18 -0.0082555255 -0.021655222 0 -0.0082555255
		 -0.021655222 0 -0.0082555255 -0.021655222 0 -0.0049533155 -0.021655222 0 -0.0049533155
		 -0.021655222 1.099857e-18 -0.0049533155 -0.021655222 0 -0.0049533155 -0.021655222
		 0 -0.0049533155 -0.021655222 0 -0.0016511051 -0.021655222 0 -0.0016511051 -0.021655222
		 3.6661898e-19 -0.0016511051 -0.021655222 0 -0.0016511051 -0.021655222 3.7252903e-09
		 -0.001651095 -0.021655226 0 0.0016511051 -0.021655222 0 0.0016511051 -0.021655222
		 -3.6661898e-19 0.0016511051 -0.021655222 0 0.0016511051 -0.021655222 3.7252903e-09
		 0.001651095 -0.021655226 0 0.0049533155 -0.021655222 0 0.0049533155 -0.021655222
		 -1.099857e-18 0.0049533155 -0.021655222 0 0.0049533155 -0.021655222 0 0.0049533155
		 -0.021655222 0 0.0082555255 -0.021655222 0 0.0082555255 -0.021655222 -1.8330949e-18
		 0.0082555255 -0.021655222 0 0.0082555255 -0.021655222 0 0.0082555255 -0.021655222
		 0 0.0082555255 0 0 0.0082555255 0 -1.8330949e-18 0.0082555255 0 0 0.0082555255 0
		 0 0.0082555255 0 0 0.0082555255 0.021655222 0 0.0082555255 0.021655222 -1.8330949e-18
		 0.0082555255 0.021655222 0 0.0082555255 0.021655222 0 0.0082555255 0.021655222 0
		 0.0049533155 0.021655222 0 0.0049533155 0.021655222 -1.099857e-18 0.0049533155 0.021655222
		 0 0.0049533155 0.021655222 0 0.0049533155 0.021655222 0 0.0016511051 0.021655222
		 0 0.0016511051 0.021655222 -3.6661898e-19 0.0016511051 0.021655222 0 0.0016511051
		 0.021655222 7.4505806e-09 0.001651095 0.021655226 0 -0.0016511051 0.021655222 0 -0.0016511051
		 0.021655222 3.6661898e-19 -0.0016511051 0.021655222 0 -0.0016511051 0.021655222 7.4505806e-09
		 -0.001651095 0.021655226 0 -0.0049533155 0.021655222 0 -0.0049533155 0.021655222
		 1.099857e-18 -0.0049533155 0.021655222 0 -0.0049533155 0.021655222 0 -0.0049533155
		 0.021655222 0 -0.0082555255 0.021655222 0 -0.0082555255 0.021655222 1.8330949e-18
		 -0.0082555255 0.021655222 0 -0.0082555255 0.021655222 0 -0.0082555255 0.021655222
		 0 -0.0082555255 0 0 -0.0082555255 0 1.8330949e-18 -0.0082555255 0 0 -0.0082555255
		 0 0 -0.0082555255 0 0 -0.0049533155 0 7.4505806e-09 -0.001651095 0 7.4505806e-09
		 0.001651095 0 0 0.0049533155 0 0 -0.0049533155 0 0 -0.0016511051 0 0 0.0016511051
		 0 0 0.0049533155 0;
createNode deleteComponent -n "deleteComponent13";
	rename -uid "46962B0C-463C-F115-CC6C-7DB05AB63C48";
	setAttr ".dc" -type "componentList" 15 "f[0:1]" "f[4:5]" "f[8:9]" "f[12:13]" "f[16:17]" "f[20:21]" "f[24:25]" "f[28:29]" "f[32:33]" "f[36:37]" "f[40:41]" "f[44:45]" "f[48:49]" "f[52:53]" "f[66:75]";
createNode polyTweak -n "polyTweak24";
	rename -uid "DEA9A136-4922-1E44-AD7E-E8BF4B212892";
	setAttr ".uopa" yes;
	setAttr -s 46 ".tk[0:45]" -type "float3"  -2.7755574e-18 0 0 0.010416667
		 0 0 0.020833334 0 0 -1.6653348e-18 0 0 0.010416667 0 0 0.020833334 0 0 -5.5511155e-19
		 0 0 0.010416667 0 0 0.020833334 0 0 5.5511155e-19 0 0 0.010416667 0 0 0.020833334
		 0 0 1.6653348e-18 0 0 0.010416667 0 0 0.020833334 0 0 2.7755574e-18 0 0 0.010416667
		 0 0 0.020833334 0 0 2.7755574e-18 0 0 0.010416667 0 0 0.020833334 0 0 2.7755574e-18
		 0 0 0.010416667 0 0 0.020833334 0 0 1.6653348e-18 0 0 0.010416667 0 0 0.020833334
		 0 0 5.5511155e-19 0 0 0.010416667 0 0 0.020833334 0 0 -5.5511155e-19 0 0 0.010416667
		 0 0 0.020833334 0 0 -1.6653348e-18 0 0 0.010416667 0 0 0.020833334 0 0 -2.7755574e-18
		 0 0 0.010416667 0 0 0.020833334 0 0 -2.7755574e-18 0 0 0.010416667 0 0 0.020833334
		 0 0 0.020833334 0 0 0.020833334 0 0 0.020833334 0 0 0.020833334 0 0;
createNode polySplit -n "polySplit12";
	rename -uid "CA535A3F-4459-FF0B-6C82-B089D328A8BD";
	setAttr -s 15 ".e[0:14]"  0.382965 0.382965 0.382965 0.382965 0.382965
		 0.382965 0.382965 0.382965 0.382965 0.382965 0.382965 0.382965 0.382965 0.382965
		 0.382965;
	setAttr -s 15 ".d[0:14]"  -2147483647 -2147483621 -2147483623 -2147483625 -2147483627 -2147483629 
		-2147483631 -2147483633 -2147483635 -2147483637 -2147483639 -2147483641 -2147483643 -2147483645 -2147483647;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "995E6839-40EF-733B-C975-1B9C40BF401F";
	setAttr ".ics" -type "componentList" 15 "f[0]" "f[2]" "f[4]" "f[6]" "f[8]" "f[10]" "f[12]" "f[14]" "f[16]" "f[18]" "f[20]" "f[22]" "f[24]" "f[26]" "f[38:51]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 -0.13206331202365507 -0.19570436254585782 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.13206331 -0.14528376 0 ;
	setAttr ".rs" 63115;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.21531884142734159 -0.19570436254585785 -0.017923451960086823 ;
	setAttr ".cbx" -type "double3" -0.048807782619968515 -0.0948631681546072 0.017923451960086823 ;
createNode polyTweak -n "polyTweak25";
	rename -uid "39103337-4DCC-F2E4-8851-B4AB88651E0E";
	setAttr ".uopa" yes;
	setAttr -s 60 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[1]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[2]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[3]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[4]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[5]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[6]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[7]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[8]" -type "float3" 0 0 -0.010421327 ;
	setAttr ".tk[9]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[10]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[11]" -type "float3" 0 0 -0.010421327 ;
	setAttr ".tk[12]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[13]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[14]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[15]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[16]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[17]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[21]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[22]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[23]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[24]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[25]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[26]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[27]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[28]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[29]" -type "float3" 0 0 0.010421327 ;
	setAttr ".tk[30]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[31]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[32]" -type "float3" 0 0 0.010421327 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[37]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[38]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[46]" -type "float3" 0 0 -0.010421328 ;
	setAttr ".tk[48]" -type "float3" 0 0 0.010421328 ;
	setAttr ".tk[49]" -type "float3" 0 0 0.010421328 ;
	setAttr ".tk[50]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[51]" -type "float3" 0 0 0.010421326 ;
	setAttr ".tk[52]" -type "float3" 0 0 0.010421328 ;
	setAttr ".tk[53]" -type "float3" 0 0 0.010421328 ;
	setAttr ".tk[55]" -type "float3" 0 0 -0.010421328 ;
	setAttr ".tk[56]" -type "float3" 0 0 -0.010421328 ;
	setAttr ".tk[57]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[58]" -type "float3" 0 0 -0.010421326 ;
	setAttr ".tk[59]" -type "float3" 0 0 -0.010421328 ;
	setAttr ".tk[90]" -type "float3" 0 0 1.3969839e-09 ;
	setAttr ".tk[91]" -type "float3" 0 0 1.3969839e-09 ;
	setAttr ".tk[92]" -type "float3" 0 0 1.3969839e-09 ;
	setAttr ".tk[94]" -type "float3" 0 0 1.3969839e-09 ;
	setAttr ".tk[97]" -type "float3" 0 0 1.3969839e-09 ;
	setAttr ".tk[99]" -type "float3" 0 0 1.3969839e-09 ;
	setAttr ".tk[103]" -type "float3" 0 0 -1.3969839e-09 ;
	setAttr ".tk[105]" -type "float3" 0 0 -1.3969839e-09 ;
	setAttr ".tk[106]" -type "float3" 0 0 -1.3969839e-09 ;
	setAttr ".tk[108]" -type "float3" 0 0 -1.3969839e-09 ;
	setAttr ".tk[111]" -type "float3" 0 0 -1.3969839e-09 ;
	setAttr ".tk[113]" -type "float3" 0 0 -1.3969839e-09 ;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "3CC24673-441D-C112-E128-D8AED71F2157";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 -0.13206331202365507 -0.12623840826265553 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak26";
	rename -uid "9EDC7197-4826-3E03-848D-9DA2462DEF54";
	setAttr ".uopa" yes;
	setAttr -s 88 ".tk[0:87]" -type "float3"  -7.2347936e-19 0.0032582614
		 0 0 0.0032582614 0 -4.3408764e-19 0.0019549569 0 6.519258e-09 0.0019549569 0 3.7902822e-18
		 -0.017069913 0 -0.023477824 -0.017069887 9.3132257e-10 -3.7902822e-18 0.017069913
		 0 -0.023477824 0.017069887 9.3132257e-10 4.3408774e-19 -0.0019549574 0 6.519258e-09
		 -0.0019549574 0 7.2347936e-19 -0.0032582614 0 0 -0.0032582614 0 7.2347936e-19 -0.0032582614
		 0 0 -0.0032582614 0 7.2347936e-19 -0.0032582614 0 0 -0.0032582614 0 4.3408774e-19
		 -0.0019549574 0 6.519258e-09 -0.0019549574 0 -3.7902822e-18 0.017069913 0 -0.023477824
		 0.017069887 -9.3132257e-10 3.7902822e-18 -0.017069913 0 -0.023477824 -0.017069887
		 -9.3132257e-10 -4.3408764e-19 0.0019549569 0 6.519258e-09 0.0019549569 0 -7.2347936e-19
		 0.0032582614 0 0 0.0032582614 0 -7.2347936e-19 0.0032582614 0 0 0.0032582614 0 6.519258e-09
		 0.0019549569 0 -0.023477824 -0.017069887 0 -0.023477824 0.017069887 0 6.519258e-09
		 -0.0019549574 0 0 0.0032582614 0 0 0.0032582614 0 0 0.0032582614 0 0 0.0019549569
		 0 0 -0.017069906 -4.6566129e-10 0 0.017069913 -4.6566129e-10 0 -0.0019549574 0 0
		 -0.0032582614 0 0 -0.0032582614 0 0 -0.0032582614 0 0 -0.0019549574 0 0 0.017069913
		 4.6566129e-10 0 -0.017069906 4.6566129e-10 0 0.0019549569 0 -4.6566129e-10 -0.0045661526
		 0.00660516 -2.3283064e-10 -0.0045661526 0.00660516 -2.3283064e-10 -0.0027396919 0.00660516
		 -4.6566129e-10 -0.0027396919 0.00660516 -2.3283064e-10 -0.020368073 0.00660516 -4.6566129e-10
		 -0.020368073 0.00660516 -2.3283064e-10 0.020368073 0.00660516 -4.6566129e-10 0.020368073
		 0.00660516 -2.3283064e-10 0.0027396921 0.00660516 -4.6566129e-10 0.0027396921 0.00660516
		 -2.3283064e-10 0.0045661526 0.00660516 -4.6566129e-10 0.0045661526 0.00660516 -2.3283064e-10
		 0.0045661526 0 -4.6566129e-10 0.0045661526 0 -2.3283064e-10 0.0045661526 -0.00660516
		 -4.6566129e-10 0.0045661526 -0.00660516 -2.3283064e-10 0.0027396921 -0.00660516 -4.6566129e-10
		 0.0027396921 -0.00660516 -2.3283064e-10 0.020368073 -0.00660516 -4.6566129e-10 0.020368073
		 -0.00660516 -2.3283064e-10 -0.020368073 -0.00660516 -4.6566129e-10 -0.020368073 -0.00660516
		 -2.3283064e-10 -0.0027396919 -0.00660516 -4.6566129e-10 -0.0027396919 -0.00660516
		 -2.3283064e-10 -0.0045661526 -0.00660516 -4.6566129e-10 -0.0045661526 -0.00660516
		 -2.3283064e-10 -0.0045661526 0 -4.6566129e-10 -0.0045661526 0 4.6566129e-10 -0.0045661526
		 0 4.6566129e-10 -0.0045661526 0.00660516 4.6566129e-10 -0.0045661526 -0.00660516
		 4.6566129e-10 -0.0027396919 -0.00660516 4.6566129e-10 -0.020368081 -0.00660516 4.6566129e-10
		 0.020368073 -0.00660516 4.6566129e-10 0.0027396921 -0.00660516 4.6566129e-10 0.0045661526
		 -0.00660516 4.6566129e-10 0.0045661526 0 4.6566129e-10 0.0045661526 0.00660516 4.6566129e-10
		 0.0027396921 0.00660516 4.6566129e-10 0.020368073 0.00660516 4.6566129e-10 -0.020368081
		 0.00660516 4.6566129e-10 -0.0027396919 0.00660516;
createNode polyTweak -n "polyTweak27";
	rename -uid "313920B6-449D-2B37-5277-07A35D4261B1";
	setAttr ".uopa" yes;
	setAttr -s 28 ".tk";
	setAttr ".tk[32]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[33]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[35]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[36]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[37]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[38]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[39]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[40]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[41]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[42]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[43]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[44]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[45]" -type "float3" 0.0058136191 0 0 ;
	setAttr ".tk[74]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[75]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[76]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[77]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[78]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[79]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[80]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[81]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[82]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[83]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[84]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[85]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[86]" -type "float3" 2.7939677e-09 0 0 ;
	setAttr ".tk[87]" -type "float3" 2.7939677e-09 0 0 ;
createNode deleteComponent -n "deleteComponent14";
	rename -uid "ED56D2D6-499B-7F60-BE2F-5F9C0B3343C8";
	setAttr ".dc" -type "componentList" 1 "f[52:65]";
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	rename -uid "05E45E7D-4CE7-4026-DEDD-F4A7C7CB9213";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 14 "e[58]" "e[61]" "e[64]" "e[67]" "e[70]" "e[73]" "e[76]" "e[79]" "e[82]" "e[85]" "e[88]" "e[91]" "e[94]" "e[96]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 -0.13206331202365507 0.16306695392229414 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.13206331 0.10013363 0 ;
	setAttr ".rs" 36173;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.21988499680139173 0.10013362815477704 -0.024528611451387405 ;
	setAttr ".cbx" -type "double3" -0.044241627245918416 0.10013362815477707 0.024528611451387405 ;
createNode polyTweak -n "polyTweak28";
	rename -uid "3C24DE45-4E15-DD42-646D-C79BD8791957";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[32]" -type "float3" -0.062933326 -1.3877788e-17 0 ;
	setAttr ".tk[35]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[37]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[39]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[41]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[43]" -type "float3" -0.062933326 -1.3877788e-17 0 ;
	setAttr ".tk[45]" -type "float3" -0.062933326 -1.3877788e-17 0 ;
	setAttr ".tk[47]" -type "float3" -0.062933326 -1.3877788e-17 0 ;
	setAttr ".tk[49]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[51]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[53]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[55]" -type "float3" -0.062933326 -6.9388939e-18 0 ;
	setAttr ".tk[57]" -type "float3" -0.062933326 -1.3877788e-17 0 ;
	setAttr ".tk[59]" -type "float3" -0.062933326 -1.3877788e-17 0 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":perspShape.msg" "imagePlaneShape1.ltc";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape2.ws";
connectAttr ":perspShape.msg" "imagePlaneShape2.ltc";
connectAttr "polySplit11.out" "pCubeShape1.i";
connectAttr "polyExtrudeEdge3.out" "pCubeShape2.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyTweak1.ip";
connectAttr "polyTweak1.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polyTweak2.ip";
connectAttr "polyTweak2.out" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyTweak3.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak3.ip";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polyTweak4.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak4.ip";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polyTweak5.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing6.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyBridgeEdge1.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge1.mp";
connectAttr "polySplitRing7.out" "polyTweak6.ip";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge2.mp";
connectAttr "polyTweak7.out" "polyBridgeEdge3.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge3.mp";
connectAttr "polyBridgeEdge2.out" "polyTweak7.ip";
connectAttr "polyBridgeEdge3.out" "polyBridgeEdge4.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge4.mp";
connectAttr "polyBridgeEdge4.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "polyExtrudeEdge1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge1.mp";
connectAttr "polyTweak9.out" "polyBridgeEdge5.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge5.mp";
connectAttr "polyExtrudeEdge1.out" "polyTweak9.ip";
connectAttr "polyBridgeEdge5.out" "polyBridgeEdge6.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge6.mp";
connectAttr "polyBridgeEdge6.out" "polyBridgeEdge7.ip";
connectAttr "pCubeShape1.wm" "polyBridgeEdge7.mp";
connectAttr "polyBridgeEdge7.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak10.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak13.ip";
connectAttr "polyExtrudeFace6.out" "polyTweak14.ip";
connectAttr "polyTweak14.out" "polySplit1.ip";
connectAttr "polySplit1.out" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "imagePlaneShape1.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "imagePlaneShape2.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "polyTweak15.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplit3.out" "polyTweak15.ip";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySoftEdge3.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace7.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak16.ip";
connectAttr "polyTweak16.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polyTweak17.ip";
connectAttr "polyTweak17.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polyTweak18.ip";
connectAttr "polyTweak18.out" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "polyTweak19.ip";
connectAttr "polyTweak19.out" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "polyExtrudeEdge2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge2.out" "polyTweak20.ip";
connectAttr "polyTweak20.out" "polySplit9.ip";
connectAttr "polySplit9.out" "polySplit10.ip";
connectAttr "polyTweak21.out" "polyMergeVert1.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert1.mp";
connectAttr "polySplit10.out" "polyTweak21.ip";
connectAttr "polyMergeVert1.out" "polySoftEdge4.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge4.out" "polyTweak22.ip";
connectAttr "polyTweak22.out" "deleteComponent12.ig";
connectAttr "deleteComponent12.og" "polySplit11.ip";
connectAttr "polyCube2.out" "polyTweak23.ip";
connectAttr "polyTweak23.out" "deleteComponent13.ig";
connectAttr "deleteComponent13.og" "polyTweak24.ip";
connectAttr "polyTweak24.out" "polySplit12.ip";
connectAttr "polyTweak25.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace9.mp";
connectAttr "polySplit12.out" "polyTweak25.ip";
connectAttr "polyTweak26.out" "polySoftEdge5.ip";
connectAttr "pCubeShape2.wm" "polySoftEdge5.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak26.ip";
connectAttr "polySoftEdge5.out" "polyTweak27.ip";
connectAttr "polyTweak27.out" "deleteComponent14.ig";
connectAttr "polyTweak28.out" "polyExtrudeEdge3.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeEdge3.mp";
connectAttr "deleteComponent14.og" "polyTweak28.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
// End of Pistol1.ma
